// Function to load script externally on demand
// -----------------------------------------------
var loadExternalScript = function(path) {
	var result = $.Deferred(),
		script = document.createElement("script");

	script.async = "async";
	script.type = "text/javascript";
	script.src = path;
	script.onload = script.onreadystatechange = function(_, isAbort) {
		if (!script.readyState || /loaded|complete/.test(script.readyState)) {
			if (isAbort)
				result.reject();
			else
				result.resolve();
		}
	};

	script.onerror = function() {
		result.reject();
	};

	$("head")[0].appendChild(script);
	return result.promise();
};

// Use loadScript to load the Razorpay checkout.js
// -----------------------------------------------
var callRazorPayScript = function(dataAmount, dataQty, dataBrand, dataDescription, dataThemeColor, dataImg) {

	var amount = dataAmount,
    qty = dataQty,
	brand = dataBrand,
	description = dataDescription,
	themeColor = dataThemeColor,
	img = dataImg;

	loadExternalScript('https://checkout.razorpay.com/v1/checkout.js').then(function() {
		var options = {
			key: 'rzp_test_3Cgnuh5smGbkK1',
			protocol: 'https',
			hostname: 'api.razorpay.com',
			amount: amount,
			name: brand,
			description: description,
			image: img,
			theme: {
				color: themeColor
			},
			handler: function(transaction) {
				console.log('Transaction Id: ', transaction.razorpay_payment_id);
				$.ajax({
			        url: "/api/capture/" + transaction.razorpay_payment_id + "?amount=" + amount
			    });
			}
		};

		window.rzpay = new Razorpay(options);
		rzpay.open();
	});

	console.log('Amount: ', amount);
	console.log('Quantity: ', qty);
	console.log('Brand: ', brand);
	console.log('Description: ', description);
	console.log('ThemeColor: ', themeColor);
	console.log('Image: ', img);
}


// Trigger addCoins CTA depending on the bundle selected
// -----------------------------------------------------

$(document).ready(function() {
    $.ajax({
        url: "/api/payment/" + getUrlParameter('orderId')
    }).then(function(data) {
    	$addCoinsBundle.attr('data-amount', data.amountInPaise);
    	$addCoinsBundle.attr('data-brand', data.amountInPaise);
    	$addCoinsBundle.attr('data-description', data.amountInPaise);
    	$addCoinsBundle.attr('data-amount', data.amountInPaise);
    	$addCoinsBundle.attr('data-amount', data.amountInPaise);
    	$addCoinsBundle.attr('data-amount', data.amountInPaise);
    	console.log(data.amountInPaise);
    });
    
    var $addCoinsBundle = $('.js-add-coin');

    $addCoinsBundle.on('click', function(e) {
      var dataItemId = $(this).data('itemid'),
          dataAmount = $(this).data('amount'),
          dataQty = $(this).data('qty'),
          dataProcessorid = $(this).data('processorid'),
          dataBrand = $(this).data('brand'),
          dataDescription = $(this).data('description'),
          dataThemeColor = $(this).data('themecolor'),
          dataImg = $(this).data('img');

      callRazorPayScript(dataAmount, dataQty, dataBrand, dataDescription, dataThemeColor, dataImg);

      e.preventDefault();
    });
});

// Read Query Parameters
var getUrlParameter = function getUrlParam(sParam) {
	var sPageURL = decodeURIComponent(window.location.search.substring(1)), sURLVariables = sPageURL
			.split('&'), sParameterName, i;

	for (i = 0; i < sURLVariables.length; i++) {
		sParameterName = sURLVariables[i].split('=');

		if (sParameterName[0] === sParam) {
			console.log(getUrlParameter(sParameterName[1] === undefined ? true : sParameterName[1]));
			return sParameterName[1] === undefined ? true : sParameterName[1];
		}
	}
}