package com.orileo.melloe.servicesimpl;

import com.orileo.melloe.model.payment.Tax;
import com.orileo.melloe.repositories.TaxesRepository;
import com.orileo.melloe.servicesinterface.TaxesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.Optional.ofNullable;


@Service
public class TaxesServiceImpl implements TaxesService{

    private final TaxesRepository taxesRepository;

    @Autowired
    public TaxesServiceImpl(TaxesRepository taxesRepository) {
        this.taxesRepository = taxesRepository;
    }

    @Override
    public List<Tax> listAllTaxes() {
        return newArrayList(ofNullable(taxesRepository.findAll())
                .orElse(Collections.emptyList()));
    }

    @Override
    public Optional<Tax> getTax(Integer id) {
        return ofNullable(taxesRepository.findOne(id));
    }

    @Transactional
    @Override
    public Tax save(@NotNull Tax tax) {
        return taxesRepository.save(tax);
    }

    @Transactional
    @Override
    public void deleteTax(Integer id) {
        taxesRepository.delete(id);
    }

    @Transactional
    @Override
    public boolean isTaxExist(Integer id) {
        return taxesRepository.exists(id);
    }
}
