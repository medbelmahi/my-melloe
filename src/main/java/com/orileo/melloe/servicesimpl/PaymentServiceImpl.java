package com.orileo.melloe.servicesimpl;

import com.orileo.melloe.model.cart.Cart;
import com.orileo.melloe.model.customer.Customer;
import com.orileo.melloe.model.customer.CustomerAddress;
import com.orileo.melloe.controllers.dto.PaymentGatewayData;
import com.orileo.melloe.model.payment.PaymentTransaction;
import com.orileo.melloe.repositories.PaymentRepository;
import com.orileo.melloe.security.IEnrichedUserDetails;
import com.orileo.melloe.servicesinterface.CustomerService;
import com.orileo.melloe.servicesinterface.PaymentService;
import com.razorpay.Payment;
import com.razorpay.RazorpayClient;
import com.razorpay.RazorpayException;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

/**
 * Created by Mohamed BELMAHI on 19/08/2017.
 */
@Service
@Slf4j
public class PaymentServiceImpl implements PaymentService {

    private CustomerService customerService;
    private PaymentRepository paymentRepository;
    private RazorpayClient razorpayClient;
    private String image;
    private String description;
    private String name;
    private String key;
    private String color;

    @Autowired
    public PaymentServiceImpl(CustomerService customerService, @Value("${melloe.razorpay.payment.info.logo}") String image,
                              @Value("${melloe.razorpay.payment.info.description}") String description,
                              @Value("${melloe.razorpay.payment.info.name}") String name,
                              @Value("${melloe.razorpay.payment.info.color}") String color,
                              @Value("${melloe.razorpay.apiKey}") String key,
                              @Value("${melloe.razorpay.secretKey}") String secretKey, PaymentRepository paymentRepository) throws RazorpayException {
        this.customerService = customerService;
        this.paymentRepository = paymentRepository;
        this.razorpayClient = new RazorpayClient(key, secretKey, true);
        this.image = image;
        this.description = description;
        this.name = name;
        this.color = color;
        this.key = key;
    }


    @Override
    public PaymentGatewayData initPaymentObject() {
    	BigDecimal totalAmount = null;
        PaymentGatewayData paymentGatewayData = new PaymentGatewayData();
        final Integer customerId = getCustomerId();
        Optional<Customer> customer = customerService.getCustomer(customerId);
        if (customer.isPresent()) {
            final Customer customerModel = customer.get();
            final Cart cart = customerModel.getCart();

            String addressString = " ";
            if (!CollectionUtils.isEmpty(customerModel.getAddress())) {
                final CustomerAddress deliveryAddress = customerModel.getAddress().get(0);
                addressString = deliveryAddress.getAddressLine()
                        + " " + deliveryAddress.getState()
                        + " " + deliveryAddress.getCity()
                        + " " + deliveryAddress.getPincode();
            }
            System.out.println("Initialised "+cart.getPromoCodeAmount());
            System.out.println("getTotalOrderPriceWithPromocode "+cart.getTotalOrderPriceWithPromocode());
            if(cart.getPromoCodeAmount() != null &&  cart.getPromoCodeAmount() > 0)
            {
            	totalAmount = BigDecimal.valueOf(cart.getTotalOrderPriceWithPromocode());
            }else
            {
                totalAmount = BigDecimal.valueOf(cart.getTotalPrice());
            }
            paymentGatewayData.withAmount(totalAmount.multiply(BigDecimal.valueOf(100)).doubleValue())
                                .withName(this.name)
                                .withDescription(this.description)
                                .withImage(this.image)
                                .withKey(this.key)
                                .withNotes(addressString)
                                .withProfile(customerModel.getFirstName()
                                        + " " + customerModel.getLastName(),
                                        customerModel.getUserId().getEmailId())
                                .withThem(this.color);
            System.out.println("totalAmount 2"+totalAmount);
            
        }
        return paymentGatewayData;
    }

    @Override
    public Payment getPaymentInfo(String paymentId) throws RazorpayException {
        final Payment payment = razorpayClient.Payments.fetch(paymentId);
        return payment;
    }

    @Override
    public void makePaymentCapture(String id, Integer amount) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("amount", amount);
        try {
            razorpayClient.Payments.capture(id, jsonObject);
        } catch (RazorpayException e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public void savePaymentTransaction(PaymentTransaction paymentTransaction) {
        paymentRepository.save(paymentTransaction);
    }

    @Override
    public List<PaymentTransaction> findAllByNotCaptured() {
        return paymentRepository.findAllByCapturedIsFalse();
    }

    @Scheduled(fixedDelayString = "${fixedDelay.in.milliseconds}")
    public void captureAllPaymentTransactions() {
        final List<PaymentTransaction> paymentTransactions = findAllByNotCaptured();
        if (!CollectionUtils.isEmpty(paymentTransactions)) {
            paymentTransactions.stream().forEach(paymentTransaction -> {
                makePaymentCapture(paymentTransaction.getId(), paymentTransaction.getAmount());
                paymentTransaction.setCaptured(true);
                savePaymentTransaction(paymentTransaction);
            });
        }
    }

    private Integer getCustomerId() {
        return getCurrentUserDetails().getCustomerId();
    }

    private IEnrichedUserDetails getCurrentUserDetails() {
        return (IEnrichedUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}