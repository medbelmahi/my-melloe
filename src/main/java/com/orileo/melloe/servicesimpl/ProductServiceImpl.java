package com.orileo.melloe.servicesimpl;


import com.google.common.collect.Lists;
import com.orileo.melloe.model.category.Category;
import com.orileo.melloe.model.filter.ProductFilter;
import com.orileo.melloe.model.product.Product;
import com.orileo.melloe.repositories.ProductRepository;
import com.orileo.melloe.servicesinterface.CategoryService;
import com.orileo.melloe.servicesinterface.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

import static java.util.Optional.ofNullable;

@Service
public class ProductServiceImpl implements ProductService {

	private final ProductRepository productRepository;
	private final CategoryService categoryService;

	@Autowired
	public ProductServiceImpl(ProductRepository productRepository, CategoryService categoryService) {
		this.productRepository = productRepository;
		this.categoryService = categoryService;
	}

	@Override
	public List<Product> listProducts(List<Integer> categoryID, List<Integer> subCategoryID, List<Integer> vendorID) {
		return productRepository.findProducts(categoryID, vendorID);
	}

	@Override
	public List<Product> getProductsByVendorId(Integer vendorID) {
		return productRepository.findProductsByVendorId(vendorID);
	}

	@Override
	public Optional<Product> getProduct(Integer id) {
    return ofNullable(productRepository.findOne(id));
	}

	@Override
	public Optional<Product> findProduct(Integer id, Integer vendorId) {
		return ofNullable(productRepository.findOne(id));
	}

    @Transactional
    @Override
    public Product saveProduct(@NotNull Product product) {
        return productRepository.save(product);
    }

    @Transactional
    @Override
    public void deleteProduct(Integer id) {
        productRepository.delete(id);
    }

    @Transactional
    @Override
    public boolean isProductExists(Integer id) {
        return productRepository.exists(id);
    }

	@Override
	public List<Product> listProducts(ProductFilter productFilter)
	{
		productFilter.setName(null);
		productFilter.setCustomer(null);
		return Lists.newArrayList(productRepository.findByQuery(
				productFilter.getMaxPrice(),
				productFilter.getMinPrice(),
				productFilter.getName(),
				productFilter.getIsDiscounted(),
				productFilter.getIsVegetarian()
		));
	}
	
	/**
	* <b>Note:</b> Method return products by passing multiple vendor ids
	* @author  Kapil Halewale
	* @version 0.2 Beta
	* @since   2017-08-02
	*/
	
	@Override
	public List<Product> vendorsProduct(List<Integer> vendorID)
	{
		if(vendorID != null && vendorID.size() > 0)
		{
			return productRepository.vendorsProduct(vendorID);
		}
		else
		{
			return null;
		}
	}

	@Override
	public List<Product> listProductsByVendorsAndCat(Integer categoryID, List<Integer> vendorID) {
		Optional<Category> category = categoryService.getCategory(categoryID);
		return productRepository.findProductsByCategoryContainsAndVendorIdIn(category.get(), vendorID);
	}
}
