package com.orileo.melloe.servicesimpl;

import com.google.maps.DistanceMatrixApi;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.PlacesApi;
import com.google.maps.model.*;
import com.orileo.melloe.model.customer.CustomerAddress;
import com.orileo.melloe.model.product.Product;
import com.orileo.melloe.model.vendor.Vendor;
import com.orileo.melloe.servicesinterface.GoogleMapService;
import com.orileo.melloe.servicesinterface.ProductService;
import com.orileo.melloe.servicesinterface.VendorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Mohamed BELMAHI on 01/09/2017.
 */
@Service
public class GoogleMapServiceImpl implements GoogleMapService {

    @Value("${melloe.google.api.key}")
    private String API_KEY;
    public static final int RADIUS = 3500;
    public static final String ADDRESS_TYPE_CITY = "ADMINISTRATIVE_AREA_LEVEL_1";
    public static final String ADDRESS_TYPE_STATE = "ADMINISTRATIVE_AREA_LEVEL_2";
    public static final String ADDRESS_TYPE_POSTAL_CODE = "POSTAL_CODE";

    @Autowired
    private VendorService vendorService;
    @Autowired
    private ProductService productService;

    @Override
    public boolean isUnderRadius(LatLng latLngOrigin) {
        List<Integer> vendorsIds = getVendorsUnderRadius(latLngOrigin);
        return !CollectionUtils.isEmpty(vendorsIds);
    }

    @Override
    public List<Product> getProductOfVendorsUnderRadius(double lat, double lng) {
        List<Integer> vendorsIds = getVendorsUnderRadius(new LatLng(lat, lng));
        final List<Product> products = productService.vendorsProduct(vendorsIds);
        return products;
    }

    private List<Integer> getVendorsUnderRadius(final LatLng latLngOrigin) {
        GeoApiContext context = new GeoApiContext.Builder()
                .apiKey(API_KEY)
                .build();

        final List<Vendor> vendors = vendorService.listAllVendors();
        final List<LatLng> latLngVendors = vendors.stream().map(vendor ->
                new LatLng("string".equals(vendor.getAddress().getLatitude()) ? 0.0 : Double.valueOf(vendor.getAddress().getLatitude()),
                "string".equals(vendor.getAddress().getLongitude()) ? 0.0 : Double.valueOf(vendor.getAddress().getLongitude()))).collect(Collectors.toList());

        final LatLng[] latLngDestinations = latLngVendors.toArray(new LatLng[latLngVendors.size()]);

        final DistanceMatrix distanceMatrix = DistanceMatrixApi.newRequest(context)
                .origins(latLngOrigin)
                .destinations(latLngDestinations)
                .awaitIgnoreError();

        List<Integer> vendorsIds = new ArrayList<>();
        for (int i = 0; i < distanceMatrix.rows[0].elements.length; i++) {
            final DistanceMatrixElement element = distanceMatrix.rows[0].elements[i];
            if (DistanceMatrixElementStatus.OK.equals(element.status)) {
                final Distance distance = element.distance;
                if (distance.inMeters <= RADIUS) {
                    vendorsIds.add(vendors.get(i).getId());
                }
            }
        }
        return vendorsIds;
    }

    @Override
    public List<Product> getProductOfCategoryAndVendorsUnderRadius(double lat, double lng, Integer catId) {
        List<Integer> vendorsIds = getVendorsUnderRadius(new LatLng(lat, lng));
        final List<Product> products = productService.listProductsByVendorsAndCat(catId,vendorsIds);
        return products;
    }

    @Override
    public CustomerAddress getNewAddressFrom(LatLng latLngOrigin) {
        CustomerAddress customerAddress = new CustomerAddress();

        GeoApiContext context = new GeoApiContext.Builder()
                .apiKey(API_KEY)
                .build();
        final GeocodingResult[] geoCodingResults = GeocodingApi.reverseGeocode(context, latLngOrigin).awaitIgnoreError();
        if (geoCodingResults.length > 0) {
            final AddressComponent[] addressComponents = geoCodingResults[0].addressComponents;
            if (addressComponents.length > 0) {
                final String city = getAddressComponent(ADDRESS_TYPE_CITY, addressComponents);
                final String state = getAddressComponent(ADDRESS_TYPE_STATE, addressComponents);;
                final String pinCode = getAddressComponent(ADDRESS_TYPE_POSTAL_CODE, addressComponents);;
                customerAddress.setCity(city);
                customerAddress.setState(state);
                customerAddress.setPincode(pinCode);
            }
        }

        return customerAddress;
    }

    @Override
    public boolean isUnderRadius(LatLng cartLatLong, LatLng vendorLatLng) {
        GeoApiContext context = new GeoApiContext.Builder()
                .apiKey(API_KEY)
                .build();

        final LatLng[] latLngDestinations = new LatLng[1];
        latLngDestinations[0] = vendorLatLng;

        final DistanceMatrix distanceMatrix = DistanceMatrixApi.newRequest(context)
                .origins(cartLatLong)
                .destinations(latLngDestinations)
                .awaitIgnoreError();

            final DistanceMatrixElement element = distanceMatrix.rows[0].elements[0];
            if (DistanceMatrixElementStatus.OK.equals(element.status)) {
                final Distance distance = element.distance;
                if (distance.inMeters <= RADIUS) {
                    return true;
                }
            }

        return false;
    }

    private String getAddressComponent(String type, AddressComponent[] addressComponents) {
        for (AddressComponent addressComponent : addressComponents) {
            if (type.equals(addressComponent.types[0].name())) {
                return addressComponent.longName;
            }
        }
        return "Not Available";
    }
}