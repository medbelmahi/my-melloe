package com.orileo.melloe.servicesimpl;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.Optional.ofNullable;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.orileo.melloe.model.customer.Customer;
import com.orileo.melloe.model.customer.CustomerContactForm;
import com.orileo.melloe.repositories.CustomerContactFormRepository;
import com.orileo.melloe.repositories.CustomerRepository;
import com.orileo.melloe.servicesinterface.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService {
	private final CustomerRepository customerRepository;
	private final CustomerContactFormRepository customerContactFormRepository;

	@Autowired
	public CustomerServiceImpl(CustomerRepository customerRepository, CustomerContactFormRepository customerContactFormRepository)
	{
		this.customerRepository = customerRepository;
		this.customerContactFormRepository= customerContactFormRepository;
	}

	@Override
	public List<Customer> listAllCustomers() {
		return newArrayList(ofNullable(customerRepository.findAll()).orElse(Collections.emptyList()));
	}

	@Override
	public Optional<Customer> getCustomer(Integer id) {
		return ofNullable(customerRepository.findOne(id));
	}

    @Transactional
    @Override
    public Customer saveCustomer(@NotNull Customer customer) {
        return customerRepository.save(customer);
    }

    @Transactional
    @Override
    public void deleteCustomer(Integer id) {
        customerRepository.delete(id);
    }

    @Transactional
    @Override
    public boolean isCustomerExists(Integer id) {
        return customerRepository.exists(id);
    }

    @Override
    public Optional<Customer> findCustomerByEmail(String username) {
        return ofNullable(customerRepository.findByEmail(username));
    }

	@Override
	public List<CustomerContactForm> listAllCustomerContactForm()
	{
		return newArrayList(ofNullable(customerContactFormRepository.findAll()).orElse(Collections.emptyList()));
	}

	@Override
	public CustomerContactForm saveCustomerContactForm(@NotNull CustomerContactForm customerContactForm)
	{
		customerContactForm.setContactedDate(new Date());
		return customerContactFormRepository.save(customerContactForm);
	}
}
