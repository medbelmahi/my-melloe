package com.orileo.melloe.servicesimpl;

import com.orileo.melloe.model.product.ProductDiscount;
import com.orileo.melloe.repositories.ProductDiscountRepository;
import com.orileo.melloe.servicesinterface.ProductDiscountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.Optional.ofNullable;

@Service
public class ProductDiscountServiceImpl implements ProductDiscountService{


	private final ProductDiscountRepository productDiscountRepository;

	@Autowired
	public ProductDiscountServiceImpl(ProductDiscountRepository productDiscountRepository) {
		this.productDiscountRepository = productDiscountRepository;
	}

	@Override
	public List<ProductDiscount> listAllProductDiscounts() {
		return newArrayList(ofNullable(productDiscountRepository.findAll()).orElse(Collections.emptyList()));
	}

	@Override
	public Optional<ProductDiscount> getProductDiscount(Integer id) {
		return ofNullable(productDiscountRepository.findOne(id));
	}

    @Transactional
    @Override
    public ProductDiscount saveProductDiscount(@NotNull ProductDiscount priceDiscount) {
        return productDiscountRepository.save(priceDiscount);
    }

    @Transactional
    @Override
    public void deleteProductDiscount(Integer id) {
        productDiscountRepository.delete(id);
    }

    @Transactional
    @Override
    public boolean isProductDiscountExists(Integer id) {
        return productDiscountRepository.exists(id);
    }

    @Override
    public Optional<ProductDiscount> findProductDiscount(Integer id, Integer vendorId) {
        return productDiscountRepository.findByIdAndDiscount(id, vendorId);
    }
}
