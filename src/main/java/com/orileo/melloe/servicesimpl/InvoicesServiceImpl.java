package com.orileo.melloe.servicesimpl;

import static java.util.Optional.ofNullable;

import java.util.List;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.orileo.melloe.model.order.Order;
import com.orileo.melloe.model.payment.Invoice;
import com.orileo.melloe.repositories.InvoicesRepository;
import com.orileo.melloe.servicesinterface.InvoicesService;


@Service

public class InvoicesServiceImpl implements InvoicesService{

    private final InvoicesRepository invoicesRepository;

    private final double deliveryCharges;
    private final double packingCharges;

    @Autowired
    public InvoicesServiceImpl(InvoicesRepository invoicesRepository,
                               @Value("${melloe.order.deliverycharges}") double deliveryCharges,
                               @Value("${melloe.order.packingcharges}") double packingCharges) {
        this.invoicesRepository = invoicesRepository;
        this.deliveryCharges = deliveryCharges;
        this.packingCharges = packingCharges;
    }

    @Override
    public List<Invoice> listAllInvoices() {
        return Lists.newArrayList(invoicesRepository.findAll());
    }

    @Override
    public Optional<Invoice> getInvoice(Integer id) {
        return ofNullable(invoicesRepository.findOne(id));
    }

    @Transactional
    @Override
    public Invoice saveInvoice(@NotNull Invoice invoice) {
        return invoicesRepository.save(invoice);
    }

    @Transactional
    @Override
    public void deleteInvoice(Integer id) {
        invoicesRepository.delete(id);
    }

    @Override
    public List<Invoice> listAllInvoicesForOrder(Integer orderID) {
        return invoicesRepository.listAllInvoicesForOrder(orderID);
    }

    @Override
    public List<Invoice> listAllInvoicesForCustomer(Integer customerID) {
        return invoicesRepository.listAllInvoicesForCustomer(customerID);
    }

    @Override
    public void generateInvoice(Order order) {
    	double serviceTax = (deliveryCharges *18)/100;
        double grandTotal = order.getTotalPrice() + serviceTax + deliveryCharges;
        Invoice invoice = new Invoice();
        invoice.setOrder(order);
        invoice.setDeliveryCharges(deliveryCharges);
        invoice.setPackingCharges(packingCharges);
        invoice.setTotalAmount(order.getTotalPrice());
        invoice.setGrandTotal(grandTotal);
        getId(order.getId()).ifPresent(invoice::setId);
        invoicesRepository.save(invoice);
    }

    private Optional<Integer> getId(Integer orderId) {
        return ofNullable(invoicesRepository.findByOrder(orderId)).map(Invoice::getId);
    }

    /*private Double calculateTotalAmount(List<OrderDetails> orderDetails) {
        return orderDetails.stream().map(OrderDetails::getTotalPrice).mapToDouble(Double::doubleValue).sum();
    }*/
}
