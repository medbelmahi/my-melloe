package com.orileo.melloe.servicesimpl;

import com.orileo.melloe.model.product.ProductReview;
import com.orileo.melloe.repositories.ProductReviewRepository;
import com.orileo.melloe.servicesinterface.ProductReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.Optional.ofNullable;


@Service
public class ProductReviewServiceImpl implements ProductReviewService{

	private final ProductReviewRepository productReviewRepository;

    @Autowired
	public ProductReviewServiceImpl(ProductReviewRepository productReviewRepository) {
		this.productReviewRepository = productReviewRepository;
	}

	@Override
	public List<ProductReview> listAllProductReviews(List<Integer> customers, List<Integer> products) {
		return newArrayList(ofNullable(productReviewRepository.findAll()).orElse(Collections.emptyList()));
	}

	@Override
	public Optional<ProductReview> getProductReview(Integer id) {
		return ofNullable(productReviewRepository.findOne(id));
	}

    @Override
    public Optional<ProductReview> getProductReview(Integer id, Integer customerID) {
        return productReviewRepository.findByIDAndCustomerID(id, customerID);
    }

    @Transactional
    @Override
    public ProductReview saveProductReview(@NotNull ProductReview productReview) {
        return productReviewRepository.save(productReview);
    }

    @Transactional
    @Override
    public void deleteProductReview(Integer id) {
        productReviewRepository.delete(id);
    }

    @Transactional
    @Override
    public boolean isProductReviewExists(Integer id) {
        return productReviewRepository.exists(id);
    }
}