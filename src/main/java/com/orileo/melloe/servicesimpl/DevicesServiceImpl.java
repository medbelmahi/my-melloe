package com.orileo.melloe.servicesimpl;

import com.orileo.melloe.model.notifications.Device;
import com.orileo.melloe.repositories.DevicesRepository;
import com.orileo.melloe.servicesinterface.DevicesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;




@Service
@Transactional
public class DevicesServiceImpl implements DevicesService{

	private DevicesRepository devicesRepository;

	
    @Autowired
    public void setDevicesRepository(DevicesRepository devicesRepository) {
        this.devicesRepository = devicesRepository;
    }
	
	@Override
    public Iterable<Device> listAllDevices() {
        return devicesRepository.findAll();
	

	}


	@Override
  public void deleteDevicesById(Integer id) {
        Device address = devicesRepository.findOne(id);
        devicesRepository.delete(address);
		
		
	}

	@Override
    public Device getById(Integer id) {
        return devicesRepository.findOne(id);
	}

	@Override
    public Device saveDevices(Device device) {
        return devicesRepository.save(device);
    }

	@Override
    public void updateDevices(Device device) {
        devicesRepository.save(device);
    }

	@Override
    public boolean isDevicesExist(Device device) {
        return getById(device.getId()) != null;
    }

}
