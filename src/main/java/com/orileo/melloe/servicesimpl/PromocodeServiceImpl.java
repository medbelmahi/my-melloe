package com.orileo.melloe.servicesimpl;

import com.orileo.melloe.model.promocode.PromoCode;
import com.orileo.melloe.repositories.PromocodeRepository;
import com.orileo.melloe.servicesinterface.PromocodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.Optional.ofNullable;


@Service
@Transactional
public class PromocodeServiceImpl<promoCodeStatusRepository> implements PromocodeService{

	private final PromocodeRepository promocodeRepository;

	@Autowired
	public PromocodeServiceImpl(PromocodeRepository promocodeRepository)
	{
		this.promocodeRepository = promocodeRepository;
	}

	@Override
	public List<PromoCode> listAllPromoCodes() {
		return newArrayList(ofNullable(promocodeRepository.findAll())
				.orElse(Collections.emptyList()));
	}

	@Override
	public Optional<PromoCode> getPromoCode(String promoCode) {
		return ofNullable(promocodeRepository.findOne(promoCode));
	}

	@Override
	public PromoCode savePromoCode(@NotNull PromoCode promoCode) {
		return promocodeRepository.save(promoCode);
	}

	@Override
	public void deletePromoCode(@NotNull String promoCode) {
		promocodeRepository.delete(promoCode);
	}

	@Override
	public boolean isPromocodeExists(String id) {
		return promocodeRepository.exists(id);
	}
}