package com.orileo.melloe.servicesimpl;

import com.orileo.melloe.model.customer.CustomerLevel;
import com.orileo.melloe.model.customer.DiscountDetails;
import com.orileo.melloe.model.customer.DiscountType;
import com.orileo.melloe.model.exceptions.MelloeException;
import com.orileo.melloe.repositories.CustomerLevelRepository;
import com.orileo.melloe.servicesinterface.CustomerLevelService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.Optional.ofNullable;

@Slf4j
@Service
public class CustomerLevelServiceImpl implements CustomerLevelService{

	private final CustomerLevelRepository customerLevelRepository;

	@Autowired
	public CustomerLevelServiceImpl(CustomerLevelRepository customerLevelRepository) {
		this.customerLevelRepository = customerLevelRepository;
	}

	@Override
	public List<CustomerLevel> listAllCustomerLevels() {
		return newArrayList(ofNullable(customerLevelRepository.findAll())
				.orElse(Collections.emptyList()));
	}

	@Override
  public Optional<CustomerLevel> getCustomerLevel(Integer id) {
    return ofNullable(customerLevelRepository.findOne(id));
	}

    @Transactional
    @Override
    public CustomerLevel saveCustomerLevel(@NotNull CustomerLevel level) {
        validateDiscountAmount(level.getDiscountDetails());
        return customerLevelRepository.save(level);
    }

	private void validateDiscountAmount(DiscountDetails discountDetails) {
		double disAmount = discountDetails.getDisAmount();
		if (discountDetails.getDiscountType() == DiscountType.PERCENT
				&& (disAmount < 0 || disAmount > 100)) {
			log.error("Invalid percent discount amount specified {} . It should be 0 to 100", disAmount);
			throw new MelloeException("You've chosen discount type Percent. " +
					"The discount amount should be in range 0 to 100 ");
		}
	}

    @Transactional
    @Override
    public void deleteCustomerLevel(Integer id) {
        customerLevelRepository.delete(id);
    }

    @Transactional
    @Override
    public boolean isCustomerLevelExist(Integer id) {
        return customerLevelRepository.exists(id);
    }
}
