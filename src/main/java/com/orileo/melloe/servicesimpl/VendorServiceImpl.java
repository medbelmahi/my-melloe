package com.orileo.melloe.servicesimpl;

import com.orileo.melloe.model.vendor.Vendor;
import com.orileo.melloe.repositories.VendorRepository;
import com.orileo.melloe.servicesinterface.VendorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.Optional.ofNullable;

@Service
@Transactional
public class VendorServiceImpl implements VendorService{

	private final VendorRepository vendorRepository;

    @Autowired
	public VendorServiceImpl(VendorRepository vendorRepository) {
		this.vendorRepository = vendorRepository;
	}

	@Override
	public List<Vendor> listAllVendors() {
		return newArrayList(ofNullable(vendorRepository.findAll()).orElse(Collections.emptyList()));
	}

    @Override
    public Optional<Vendor> findVendorByEmail(String username) {
        return ofNullable(vendorRepository.findByEmail(username));
    }

    @Transactional
    @Override
    public void deleteVendor(Integer id) {
        vendorRepository.delete(id);
    }

	@Override
  public Optional<Vendor> getVendor(Integer id) {
    return ofNullable(vendorRepository.findOne(id));
	}

    @Transactional
    @Override
    public Vendor saveVendor(@NotNull Vendor vendor) {
        return vendorRepository.save(vendor);
    }
}