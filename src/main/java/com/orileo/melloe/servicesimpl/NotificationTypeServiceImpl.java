package com.orileo.melloe.servicesimpl;

import com.orileo.melloe.model.notifications.NotificationType;
import com.orileo.melloe.repositories.NotificationTypeRepository;
import com.orileo.melloe.servicesinterface.NotificationTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.Optional.ofNullable;


@Service
public class NotificationTypeServiceImpl implements NotificationTypeService{

	private final NotificationTypeRepository notificationTypeRepository;

	@Autowired
	public NotificationTypeServiceImpl(NotificationTypeRepository notificationTypeRepository) {
		this.notificationTypeRepository = notificationTypeRepository;
	}

	@Override
	public List<NotificationType> listAllNotificationTypes() {
		return newArrayList(ofNullable(notificationTypeRepository.findAll())
				.orElse(Collections.emptyList()));

	}

	@Override
	public Optional<NotificationType> getNotificationType(Integer id) {
		return ofNullable(notificationTypeRepository.findOne(id));
	}

    @Transactional
    @Override
    public NotificationType saveNotificationType(@NotNull NotificationType notificationType) {
        return notificationTypeRepository.save(notificationType);
    }

    @Transactional
    @Override
    public void deleteNotificationType(Integer id) {
        notificationTypeRepository.delete(id);
    }

    @Transactional
    @Override
    public boolean isNotificationTypeExist(Integer id) {
        return notificationTypeRepository.exists(id);
    }
}
