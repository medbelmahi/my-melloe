package com.orileo.melloe.servicesimpl;

import com.orileo.melloe.model.customer.Reminder;
import com.orileo.melloe.repositories.ReminderRepository;
import com.orileo.melloe.servicesinterface.ReminderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.Optional.ofNullable;

@Service
public class ReminderServiceImpl implements ReminderService {

	private final ReminderRepository reminderRepository;

	@Autowired
	public ReminderServiceImpl(ReminderRepository reminderRepository) {
		this.reminderRepository = reminderRepository;
	}

	@Override
	public List<Reminder> listAllReminders() {
		return newArrayList(ofNullable(reminderRepository.findAll())
				.orElse(Collections.emptyList()));
	}

	@Override
	public Optional<Reminder> getReminder(Integer id) {
		return ofNullable(reminderRepository.findOne(id));
	}

    @Transactional
    @Override
    public Reminder saveReminder(@NotNull Reminder remainder) {
        return reminderRepository.save(remainder);
    }

    @Transactional
    @Override
    public void deleteReminder(Integer id) {
        reminderRepository.delete(id);
    }

    @Transactional
    @Override
    public boolean isReminderExists(Integer id) {
        return reminderRepository.exists(id);
    }
}
