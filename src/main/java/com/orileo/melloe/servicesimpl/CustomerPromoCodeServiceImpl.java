package com.orileo.melloe.servicesimpl;

import com.orileo.melloe.model.customer.CustomerPromoCode;
import com.orileo.melloe.repositories.CustomerPromoCodeRepository;
import com.orileo.melloe.servicesinterface.CustomerPromoCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.Optional.ofNullable;

import java.util.Collections;


@Service
public class CustomerPromoCodeServiceImpl implements CustomerPromoCodeService{

	private final CustomerPromoCodeRepository customerPromoCodeRepository;

	@Autowired
	public CustomerPromoCodeServiceImpl(CustomerPromoCodeRepository customerPromoCodeRepository) {
		this.customerPromoCodeRepository = customerPromoCodeRepository;
	}

	@Override
	public List<CustomerPromoCode> listCustomerPromocodes(List<Integer> customerID, List<String> promocode) {
        return customerPromoCodeRepository.findByCustomerIDAndPromocode(customerID, promocode);
    }

	@Override
  public Optional<CustomerPromoCode> getCustomerPromocode(Integer id) {
    return ofNullable(customerPromoCodeRepository.findOne(id));
	}

    @Transactional
    @Override
    public CustomerPromoCode saveCustomerPromocode(@NotNull CustomerPromoCode customerPromocode) {
        return customerPromoCodeRepository.save(customerPromocode);
    }

    @Transactional
    @Override
    public void deleteCustomerPromocode(Integer id) {
        customerPromoCodeRepository.delete(id);
    }

    @Transactional
    @Override
    public boolean isCustomerPromocodeExists(Integer id) {
        return customerPromoCodeRepository.exists(id);
    }

    @Override
	public List<CustomerPromoCode> getCustomerPromoCodeStatus(Integer customerId, String promocode, String ipAddress, String appId)
	{
		return newArrayList(ofNullable(customerPromoCodeRepository.findPromoCodeStatus(customerId, promocode, ipAddress, appId))
				.orElse(Collections.emptyList()));
	}
}
