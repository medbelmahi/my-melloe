package com.orileo.melloe.servicesimpl;

import com.orileo.melloe.controllers.webmodel.CartResourceConverter;
import com.orileo.melloe.model.cart.Cart;
import com.orileo.melloe.model.customer.Customer;
import com.orileo.melloe.model.customer.CustomerAddress;
import com.orileo.melloe.security.IEnrichedUserDetails;
import com.orileo.melloe.servicesinterface.CartService;
import com.orileo.melloe.servicesinterface.CustomerService;
import com.orileo.melloe.servicesinterface.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.Optional;

/**
 * Created by Mohamed BELMAHI on 22/07/2017.
 */
@Service
public class SessionServiceImpl implements SessionService {

    @Autowired
    private CartResourceConverter cartResourceConverter;

    @Autowired
    private CartService cartService;

    @Autowired
    private CustomerService customerService;

    @Override
    public Cart getSessionCart(HttpSession session) {
        Cart currentCart = null;

        final Integer customerId = getCustomerId();
        if (customerId != null) {
            final Optional<Customer> customer = customerService.getCustomer(customerId);
            if (customer.isPresent()) {
                final Cart customerCart = customer.get().getCart();
                currentCart = customerCart;
            }
        }
        if (currentCart == null) {
            currentCart = getCartFromSession(session);
        }
        return currentCart;
    }

    private Cart getCartFromSession(HttpSession session) {
        Cart currentCart;
        currentCart = (Cart) session.getAttribute(CURRENT_CART);
        if (currentCart == null) {
            Optional<Cart> cart = cartService.getCartBySession(session.getId());
            if (cart.isPresent()) {
                currentCart = cart.get();
            }else {
                final Integer customerId = getCustomerId();
                if (customerId != null) {
                    final Optional<Customer> customer = customerService.getCustomer(customerId);
                    if (customer.isPresent()) {
                        final Cart customerCart = customer.get().getCart();
                        if (currentCart != null) {
                            currentCart = customerCart;
                        }
                    }
                }
                if (currentCart == null) {
                    currentCart = createNewCart();
                    currentCart.setSessionID(session.getId());
                    cartService.saveCart(currentCart);
                    if (customerId != null) {
                        cartResourceConverter.setCustomer(currentCart, customerId);
                    }
                    cartService.saveCart(currentCart);
                }
            }
            session.setAttribute(CURRENT_CART, currentCart);
        }
        return currentCart;
    }

    @Override
    public void setSessionCart(HttpSession session, Cart cart) {
        session.setAttribute(CURRENT_CART, cart);
    }

    private Cart createNewCart() {
        final Cart cart = new Cart();
        cart.setTotalPrice(BigDecimal.ZERO.doubleValue());
        cart.setGstCharge(BigDecimal.ZERO.doubleValue());
        cart.setHandlingCharge(BigDecimal.ZERO.doubleValue());
        cart.setPromoCodeAmount(BigDecimal.ZERO.doubleValue());
        cart.setTotalOrderPriceWithPromocode(BigDecimal.ZERO.doubleValue());
        cart.setPackagingCharges(BigDecimal.ZERO.doubleValue());
        CustomerAddress customerAddress = new CustomerAddress();
        customerAddress.setAddressLine("");
        customerAddress.setAddressLine_2("");
        customerAddress.setAddressName("");
        customerAddress.setCity("");
        customerAddress.setPincode("");
        customerAddress.setNearestLandmark("");
        customerAddress.setState("");
        cart.setDeliveryAddress(customerAddress);
        return cart;
    }

    private Integer getCustomerId() {
        return getCurrentUserDetails().getCustomerId();
    }

    private IEnrichedUserDetails getCurrentUserDetails() {
        return (IEnrichedUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}