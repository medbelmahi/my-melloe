package com.orileo.melloe.servicesimpl;

import com.orileo.melloe.model.filter.ProductFilter;
import com.orileo.melloe.repositories.ProductFilterRepository;
import com.orileo.melloe.servicesinterface.ProductFilterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.Optional.ofNullable;

@Component
public class ProductFilterServiceImpl implements ProductFilterService {
    private final ProductFilterRepository productFilterRepository;

    @Autowired
    public ProductFilterServiceImpl(ProductFilterRepository productFilterRepository) {
        this.productFilterRepository = productFilterRepository;
    }

    @Override
    public List<ProductFilter> listAllProductFiltersForCustomer(Integer customerID) {
        return newArrayList(ofNullable(productFilterRepository.findAllForCustomer(customerID))
                .orElse(Collections.emptyList()));
    }

    @Override
    public Optional<ProductFilter> getProductFilter(Integer id, Integer customerID) {
        return productFilterRepository.getByIdAndCustomer(id, customerID);
    }

    @Override
    public Optional<ProductFilter> findByName(String name) {
        return Optional.ofNullable(productFilterRepository.findByName(name));
    }

    @Override
    public ProductFilter saveProductFilter(@NotNull ProductFilter productFilter) {
        return productFilterRepository.save(productFilter);
    }

    @Override
    public void deleteProductFilter(Integer id) {
        productFilterRepository.delete(id);
    }
}
