package com.orileo.melloe.servicesimpl;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.orileo.melloe.controllers.dto.AddToCartInput;
import com.orileo.melloe.controllers.dto.PaymentTransactionDto;
import com.orileo.melloe.controllers.webmodel.OrderResource;
import com.orileo.melloe.model.customer.Customer;
import com.orileo.melloe.model.exceptions.CheckPaidAmountException;
import com.orileo.melloe.model.exceptions.ProductIsNotFromSameVendorException;
import com.orileo.melloe.model.cart.Cart;
import com.orileo.melloe.model.cart.CartDetails;
import com.orileo.melloe.model.payment.PaymentTransaction;
import com.orileo.melloe.model.product.Product;
import com.orileo.melloe.repositories.CartRepository;
import com.orileo.melloe.servicesinterface.CartService;
import com.orileo.melloe.servicesinterface.CustomerService;
import com.orileo.melloe.servicesinterface.PaymentService;
import com.orileo.melloe.servicesinterface.ProductService;
import com.razorpay.Payment;
import com.razorpay.RazorpayException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@PropertySource("classpath:application.properties")
public class CartServiceImpl implements CartService {

	private final CartRepository cartRepository;
    private final ProductService productService;
    private final PaymentService paymentService;
    private final CustomerService customerService;

    @Value("${melloe.order.handling.charge}")
    private Double handlingCharge;

    @Value("${melloe.order.gst.charge}")
    private Double gstCharge;

	@Autowired
	public CartServiceImpl(CartRepository ordersRepository, ProductService productService, PaymentService paymentService, CustomerService customerService) {
		this.cartRepository = ordersRepository;
        this.productService = productService;
        this.paymentService = paymentService;
        this.customerService = customerService;
    }

	@Override
	public Optional<Cart> getCart(Integer id, Integer customerID) {
        return cartRepository.findByIDAndCustomerID(id, customerID);
    }

    @Override
    public Cart getCart(Integer id) {
        return cartRepository.findOne(id);
    }

    @Transactional
    @Override
    public Cart saveCart(@NotNull Cart cart) {
        return cartRepository.save(cart);
    }

    @Transactional
    @Override
    public void deleteCart(Integer id) {
        cartRepository.delete(id);
    }

    @Transactional
    @Override
    public void deleteCart(Cart cart, Integer customerId) {
        if (cart.getCustomer() != null) {
            System.err.println("customer : " + cart.getCustomer().getLastName());

        } else {
            System.err.println("No customer attached .......");
        }
        cart.setCustomer(null);
        cart.setCartDetails(null);
        saveCart(cart);

        final Optional<Customer> customer = customerService.getCustomer(customerId);
        if (customer.isPresent()) {
            customer.get().setCart(null);
            customerService.saveCustomer(customer.get());
        }
        cartRepository.delete(cart);
    }

    @Transactional
    @Override
    public boolean isCartExist(Integer id) {
        return cartRepository.exists(id);
    }

    @Override
    public void calculateCart(Cart cart) {
        BigDecimal bigTotalPrice = BigDecimal.ZERO;
        BigDecimal packagingTotalPrice = BigDecimal.ZERO;
        BigDecimal promoCodePrice = BigDecimal.ZERO;
        BigDecimal grandTotalpromoCodePrice = BigDecimal.ZERO;
        
        if(cart.getPromoCodeAmount() != null && cart.getPromoCodeAmount() > 0)
        {
        	promoCodePrice = BigDecimal.valueOf(cart.getPromoCodeAmount().doubleValue());
        }

        if (!CollectionUtils.isEmpty(cart.getCartDetails())) {
            for (CartDetails cartDetails : cart.getCartDetails()) {
                BigDecimal itemTotalPrice = BigDecimal.valueOf(cartDetails.getQuantity()).multiply(BigDecimal.valueOf(cartDetails.getPricePerUnit()));
                cartDetails.setTotalPrice(itemTotalPrice.doubleValue());
                bigTotalPrice = bigTotalPrice.add(BigDecimal.valueOf(cartDetails.getTotalPrice()));
                packagingTotalPrice  = packagingTotalPrice.add(BigDecimal.valueOf(cartDetails.getPackagingPrice()));
            }
        }

        if (BigDecimal.ZERO.equals(bigTotalPrice)) {
            cart.setHandlingCharge(BigDecimal.ZERO.doubleValue());
            cart.setGstCharge(BigDecimal.ZERO.doubleValue());
            cart.setPackagingCharges(BigDecimal.ZERO.doubleValue());
            cart.setTotalPrice(bigTotalPrice.doubleValue());
            cart.setPromoCodeAmount(BigDecimal.ZERO.doubleValue());
            cart.setTotalOrderPriceWithPromocode(BigDecimal.ZERO.doubleValue());
            if(cart.getPromoCodeAmount() != null && cart.getPromoCodeAmount() > 0)
            {
            	grandTotalpromoCodePrice = bigTotalPrice.subtract(promoCodePrice); 
            	cart.setPromoCodeAmount(promoCodePrice.doubleValue());
            	cart.setTotalOrderPriceWithPromocode(grandTotalpromoCodePrice.doubleValue());
            }
        } else {
            cart.setHandlingCharge(handlingCharge);
            cart.setPackagingCharges(packagingTotalPrice.doubleValue());
            bigTotalPrice = bigTotalPrice.add(BigDecimal.valueOf(handlingCharge));
            bigTotalPrice = bigTotalPrice.add(packagingTotalPrice);

            final BigDecimal bigGstCharge = BigDecimal.valueOf(handlingCharge)
                    .multiply(BigDecimal.valueOf(gstCharge)).divide(BigDecimal.valueOf(100));
            bigTotalPrice = bigTotalPrice.add(bigGstCharge);
            cart.setGstCharge(bigGstCharge.doubleValue());
            
            bigTotalPrice = bigTotalPrice.setScale(0, RoundingMode.CEILING);
            cart.setTotalPrice(bigTotalPrice.doubleValue());
            
            if(cart.getPromoCodeAmount() != null && cart.getPromoCodeAmount() > 0)
            {
            	grandTotalpromoCodePrice = bigTotalPrice.subtract(promoCodePrice); 
            	cart.setPromoCodeAmount(promoCodePrice.doubleValue());
            	cart.setTotalOrderPriceWithPromocode(grandTotalpromoCodePrice.doubleValue());
            }

        }
    }

    @Override
    public Optional<Cart> getCartBySession(String sessionId) {
        return cartRepository.findBySessionId(sessionId);
    }

    @Override
    public void addToCart(Cart cart, Integer productId, Integer vendorId) throws ProductIsNotFromSameVendorException {
        if (cart.getVendorId() == null || vendorId.equals(cart.getVendorId())) {
            Optional<Product> productResult = productService.findProduct(productId, vendorId);
            if (productResult.isPresent()) {
                Product product = productResult.get();
                updateOrCreateCartDetails(cart, product, 1);
                calculateCart(cart);
                if (cart.getVendorId() == null) {
                    cart.setVendorId(vendorId);
                }
                saveCart(cart);
            }
        } else {
            throw new ProductIsNotFromSameVendorException("Your cart already contains some desserts. You can either choose " +
                    "from the suggested lip smacking desserts below or Reset Cart to try some different desserts");
        }
    }

    @Override
    public void remove(Cart cart, Integer productId) {
        if (!CollectionUtils.isEmpty(cart.getCartDetails())) {
            CartDetails cartDetails = null;
            for (CartDetails cartD : cart.getCartDetails()) {
                if (cartD.getProduct().getId() == productId) {
                    cartDetails = cartD;
                    break;
                }
            }

            if (cartDetails != null) {
                cart.getCartDetails().remove(cartDetails);
                calculateCart(cart);
            }

            if (CollectionUtils.isEmpty(cart.getCartDetails())) {
                cart.setVendorId(null);
            }
        }
        saveCart(cart);
    }

    @Override
    public void updateQty(Cart cart, Integer productId, Integer qty) {
        if (!CollectionUtils.isEmpty(cart.getCartDetails())) {
            CartDetails cartDetails = null;
            for (CartDetails cartD : cart.getCartDetails()) {
                if (cartD.getProduct().getId() == productId) {
                    cartDetails = cartD;
                    break;
                }
            }
            if (cartDetails != null) {
                cartDetails.setQuantity(qty);
                calculateCart(cart);
            }
        }
        saveCart(cart);
    }

    @Override
    public void addToCart(Cart cart, AddToCartInput productInput) {
        Optional<Product> productResult = productService.findProduct(productInput.getProductId(), productInput.getVendorId());
        if (productResult.isPresent()) {
            Product product = productResult.get();
            updateOrCreateCartDetails(cart, product, productInput.getQty());
            calculateCart(cart);
            saveCart(cart);
        }
    }

    @Override
    public void checkPaidAmount(Cart cart, OrderResource orderResource) throws CheckPaidAmountException {
        if ("ONLINE_ASYNC".equals(orderResource.getOrderType())) {
            try {
                final Payment paymentInfo = paymentService.getPaymentInfo(orderResource.getPaymentId());
                Gson gson = new GsonBuilder().create();
                PaymentTransactionDto paymentTransactionDto = gson.fromJson(paymentInfo.toString(), PaymentTransactionDto.class);
                if (paymentTransactionDto.getAmount() < cart.getTotalPrice().intValue()) {
                    log.info("paid amount(" + paymentTransactionDto.getAmount()
                            + " not equal cart(" + cart.getId()
                            + ") total price(" + cart.getTotalPrice() + ")");
                    throw new CheckPaidAmountException();
                }

                if (!paymentTransactionDto.isCaptured()) {
                    paymentService.makePaymentCapture(paymentTransactionDto.getId(), paymentTransactionDto.getAmount());
                } else {
                    PaymentTransaction paymentTransaction = new PaymentTransaction();
                    paymentTransaction.setId(paymentTransactionDto.getId());
                    paymentTransaction.setAmount(paymentTransactionDto.getAmount());
                    paymentTransaction.setCaptured(paymentTransactionDto.isCaptured());
                    paymentTransaction.setOrderId(paymentTransactionDto.getOrder_id());
                    paymentTransaction.setStatus(paymentTransactionDto.getStatus());

                    paymentService.savePaymentTransaction(paymentTransaction);
                }

            } catch (RazorpayException e) {
                log.info(e.getMessage(), e);
                throw new CheckPaidAmountException();
            }
        }
    }

    private void updateOrCreateCartDetails(Cart cart, Product product, Integer qty) {
        CartDetails cartDetails = null;
        if (!CollectionUtils.isEmpty(cart.getCartDetails())) {
            for (CartDetails cartD : cart.getCartDetails()) {
                if (cartD.getProduct().getId().equals(product.getId())) {
                    cartDetails = cartD;
                    break;
                }
            }
        }

        if (cartDetails != null) {
            cartDetails.setQuantity(cartDetails.getQuantity() + qty);
        } else {
            cartDetails = new CartDetails();
            cartDetails.setProduct(product);
            cartDetails.setPricePerUnit(product.getProductPrice().getPrice());
            cartDetails.setPackagingPrice(product.getProductPrice().getPackagingCharges());
            cartDetails.setQuantity(qty);
            if (CollectionUtils.isEmpty(cart.getCartDetails())) {
                List<CartDetails> cartDetailsList = new ArrayList<>();
                cartDetailsList.add(cartDetails);
                cart.setCartDetails(cartDetailsList);
            } else {
                cart.getCartDetails().add(cartDetails);
            }
        }
        cartDetails.setTotalPrice(BigDecimal.valueOf(cartDetails.getPricePerUnit()).multiply(
                                    BigDecimal.valueOf(cartDetails.getQuantity())).doubleValue());
    }

	@Override
	public void applyPromocode(Cart cart)
	{
		calculateCart(cart);
        saveCart(cart);
	}
}
