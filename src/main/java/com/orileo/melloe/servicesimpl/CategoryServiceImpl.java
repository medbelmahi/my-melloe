package com.orileo.melloe.servicesimpl;

import com.orileo.melloe.model.category.Category;
import com.orileo.melloe.repositories.CategoryRepository;
import com.orileo.melloe.servicesinterface.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.Optional.ofNullable;

@Service

public class CategoryServiceImpl implements CategoryService {

	private final CategoryRepository categoryRepository;

	@Autowired
	public CategoryServiceImpl(CategoryRepository categoryRepository) {
		this.categoryRepository = categoryRepository;
	}

	public List<Category> listAllCategories() {
		return newArrayList(categoryRepository.findAll());
	}

  public Optional<Category> getCategory(Integer id) {
    return ofNullable(categoryRepository.findOne(id));
	}

    @Transactional
    public Category saveCategory(@NotNull Category category) {
		return categoryRepository.save(category);
	}

    @Transactional
    public void deleteCategory(Integer id) {
      categoryRepository.delete(id);
	}

    @Transactional
    @Override
    public boolean isCategoryExists(Integer id) {
        return categoryRepository.exists(id);
    }

	@Override
	public Optional<Category> findByName(String category) {
		return ofNullable(categoryRepository.findByName(category));
	}

}
