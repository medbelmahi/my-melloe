package com.orileo.melloe.servicesimpl;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.orileo.melloe.servicesinterface.NotificationSenderService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class NotificationSenderServiceImpl implements NotificationSenderService {

	private final JavaMailSender mailSender;
	private final TemplateEngine templateEngine;
	private final RestTemplate restTemplate;
	private final String smsApiKey;
	private final String smsUsername;
	private final String senderName;
	private final String smsType = "TRANS";

	@Autowired
	public NotificationSenderServiceImpl(JavaMailSender mailSender, TemplateEngine templateEngine,
			@Value("${melloe.sms.api.key}") String smsApiKey, @Value("${melloe.sms.username}") String smsUsername,
			@Value("${melloe.sms.sender.name}") String senderName) {
		this.mailSender = mailSender;
		this.templateEngine = templateEngine;
		this.smsApiKey = smsApiKey;
		this.smsUsername = smsUsername;
		this.senderName = senderName;
		this.restTemplate = new RestTemplate();
	}

	private String buildMessage(String templateName, Map<String, String> variableMap) {
		Context context = new Context();
		for (String key : variableMap.keySet()) {
			context.setVariable(key, variableMap.get(key));
		}
		return templateEngine.process(/* "mail_template" */templateName, context);
	}

	private void sendMessage(String templateName, Map<String, String> variableMap, String subject, String emailIdTo)
			throws Exception {
		MimeMessagePreparator messagePreparator = mimeMessage -> {
			MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
			messageHelper.setTo(emailIdTo);
			// messageHelper.setTo("success@simulator.amazonses.com");
			messageHelper.setSubject(subject);
			String content = buildMessage(templateName, variableMap);
			messageHelper.setText(content, true);
		};
		this.mailSender.send(messagePreparator);
	}
	
	public boolean sendSMS(String message, String phoneNumber)
	{
		System.out.println("In Message IMPL message : "+message+ "PHONE : "+phoneNumber);
		String url = new StringBuilder("http://sms.hspsms.com/sendSMS?username=").append(smsUsername)
				.append("&message=").append(message).append("&sendername=").append(senderName).append("&smstype=")
				.append(smsType).append("&numbers=").append(phoneNumber).append("&apikey=").append(smsApiKey)
				.toString();
		System.out.println("URL"+url);
		ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
		System.out.println("message status" +response.getBody());
/*		JSONObject obj = new JSONObject(response.getBody());
		
		if (obj.isNull("msgid")) {
			return false;
		} else {
			return true;
		}
*/
		return true;
	}
	
	/**
	 * @param emailTemplateName
	 * @param variableMap
	 * @param subject
	 * @param emailIdTo
	 * @param numbers
	 * @param smsMessage
	 */
	public void sendNotification(String emailTemplateName, Map<String, String> variableMap, String subject, String emailIdTo, String numbers, String smsMessage) {
		try {
			sendMessage(emailTemplateName, variableMap, subject, emailIdTo);
		} catch (Exception ex) {
			log.error("Email notification sending failed.", ex);
		}

		try {
			sendSMS(smsMessage, numbers);
		} catch (Exception ex) {
			log.error("SMS notification sending failed.", ex);
		}
	}

}