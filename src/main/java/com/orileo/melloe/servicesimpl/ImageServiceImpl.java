package com.orileo.melloe.servicesimpl;

import java.io.IOException;
import java.io.InputStream;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.orileo.melloe.servicesinterface.ImageService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ImageServiceImpl implements ImageService {

	private final String bucketName;
	private final String s3BaseURL;
	private final AmazonS3Client amazonS3Client;

	@Autowired
	public ImageServiceImpl(AmazonS3Client amazonS3Client, @Value("${melloe.aws.s3.bucket}") String bucketName,
			@Value("${melloe.aws.s3.baseurl}") String s3BaseURL) {
		this.amazonS3Client = amazonS3Client;
		this.bucketName = bucketName;
		this.s3BaseURL = s3BaseURL;
	}

	@Override
	public boolean uploadFile(MultipartFile multipartFile, String id) {
		boolean isUploaded = false;
		PutObjectResult putObjectResult = null;
		if (!StringUtils.isEmpty(multipartFile.getOriginalFilename())) {
			try {
				putObjectResult = upload(multipartFile.getInputStream(), id);
				isUploaded = true;
			} catch (IOException e) {
				log.error("Image upload failed for product: " + id + " in bucket: " + bucketName, e);
			}
			log.info("{} - Image is uploaded successfully.", putObjectResult.toString());
		}
		return isUploaded;
	}

	/**
	 * Private method to upload product image to AWS S3
	 * @param inputStream
	 * @param uploadKey
	 * @return
	 */
	private PutObjectResult upload(InputStream inputStream, String uploadKey) {
		PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, uploadKey, inputStream,
				new ObjectMetadata());
		putObjectRequest.setCannedAcl(CannedAccessControlList.PublicRead);
		PutObjectResult putObjectResult = amazonS3Client.putObject(putObjectRequest);
		IOUtils.closeQuietly(inputStream);
		return putObjectResult;
	}

	@Override
	public String generateLink(String id) {
		return String.join("/", s3BaseURL, bucketName, id);
	}

	@Override
	public void deleteFile(String id) {
		amazonS3Client.deleteObject(new DeleteObjectRequest(bucketName, id));
		
		log.info("{} - Image is deleted successfully.", id);
	}
}
