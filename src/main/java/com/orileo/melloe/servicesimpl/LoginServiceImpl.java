package com.orileo.melloe.servicesimpl;

import com.orileo.melloe.model.exceptions.DuplicateException;
import com.orileo.melloe.model.forgotpassword.PasswordResetToken;
import com.orileo.melloe.model.login.MasterLogin;
import com.orileo.melloe.model.vendor.UserType;
import com.orileo.melloe.repositories.ForgotRepository;
import com.orileo.melloe.repositories.LoginRepository;
import com.orileo.melloe.servicesinterface.LoginService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.Optional.ofNullable;

import java.util.Arrays;
import java.util.Calendar;

@Slf4j
@Service
@Transactional
public class LoginServiceImpl implements LoginService {

	private final LoginRepository loginRepository;
	
	private final ForgotRepository forgotRepository;

	private final BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	public LoginServiceImpl(LoginRepository loginRepository,
							BCryptPasswordEncoder bCryptPasswordEncoder,ForgotRepository forgotRepository ) {
		this.loginRepository = loginRepository;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
		this.forgotRepository = forgotRepository;
	}

	@Override
	public List<MasterLogin> listAllLogins() {
        return newArrayList(ofNullable(loginRepository.findAll())
                .orElse(Collections.emptyList()))
                .stream().map(LoginServiceImpl::clearPassword)
                .collect(Collectors.toList());
	}

    private static MasterLogin clearPassword(MasterLogin login) {
        login.setPassword(StringUtils.EMPTY);
        return login;
    }

	@Override
  public Optional<MasterLogin> getLogin(Integer id) {
        return ofNullable(clearPassword(loginRepository.findOne(id)));
    }

	@Override
	public MasterLogin saveLogin(MasterLogin login) {
		login.setPassword(bCryptPasswordEncoder.encode(login.getPassword()));
		return loginRepository.save(login);
	}


	@Override
	public Optional<MasterLogin> findLoginByEmail(String email) {
		return ofNullable(loginRepository.findByEmailId(email));
	}

	@Override
	public Optional<MasterLogin> findByUserType(UserType userType) {
		return ofNullable(loginRepository.findByUserType(userType));
	}

	@Override
  public void deleteLogin(Integer id) {
    loginRepository.delete(id);
	}

	@Override
	public void isMasterLoginExists(MasterLogin userId) {
		Optional<MasterLogin> oldLogin = findLoginByEmail(userId.getEmailId());
		if (oldLogin.isPresent()) {
			log.warn("Email Address {} already present", userId.getEmailId());
			throw new DuplicateException("User", "email " + userId.getEmailId());
		}
	}

	@Override
  public boolean isUserExists(Integer id) {
    return loginRepository.exists(id);
	}

	@Override
	public void deleteLogins(List<MasterLogin> userId) {
		loginRepository.delete(userId);
	}

    @Override
    public PasswordResetToken createPasswordResetTokenForUser(MasterLogin user, String token) {
        PasswordResetToken myToken = new PasswordResetToken();
        myToken.setToken(token);
        myToken.setUser(user);
        forgotRepository.save(myToken);
        return myToken;
    }
    
    @Override
    public String validatePasswordResetToken(long id, String token) {
        PasswordResetToken passToken = 
          forgotRepository.findByToken(token);
        if ((passToken == null) || (passToken.getUser()
            .getId() != id)) {
            return "invalidToken";
        }
     
        Calendar cal = Calendar.getInstance();
        System.out.println(passToken.getExpiryDate().getTime());
        System.out.println(cal.getTime());
        System.out.println(cal.getTime().getTime());
        System.out.println(passToken.getExpiryDate().getTime() - cal.getTime().getTime());
        if ((passToken.getExpiryDate()
            .getTime() - cal.getTime()
            .getTime()) <= 0)
        {
            return "expired";
        }
     
        MasterLogin user = passToken.getUser();
        Authentication auth = new UsernamePasswordAuthenticationToken(
          user, null, Arrays.asList(
          new SimpleGrantedAuthority("CHANGE_PASSWORD_PRIVILEGE")));
        SecurityContextHolder.getContext().setAuthentication(auth);
        return null;
    }
    
    @Override
    public void changeUserPassword(MasterLogin user, String password) {
        user.setPassword(bCryptPasswordEncoder.encode(password));
        loginRepository.save(user);
    }
    
    @Override
	public MasterLogin findUserByToken(String token) {
    	PasswordResetToken resetToken=forgotRepository.findByToken(token);
    	return resetToken.getUser();
	}

}
