package com.orileo.melloe.servicesimpl;


import com.orileo.melloe.model.category.SubCategory;
import com.orileo.melloe.repositories.SubCategoryRepository;
import com.orileo.melloe.servicesinterface.SubCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.Optional.ofNullable;

@Service
public class SubCategoryServiceImpl implements SubCategoryService{

	private final SubCategoryRepository subCategoryRepository;

	@Autowired
	public SubCategoryServiceImpl(SubCategoryRepository subCategoryRepository) {
		this.subCategoryRepository = subCategoryRepository;
	}

	public List<SubCategory> listAllSubCategories(List<Integer> categoryIDs) {
		return newArrayList(ofNullable(subCategoryRepository.findAll()).orElse(Collections.emptyList()));
	}

	@Override
	public List<SubCategory> listAllSubcategoriesForCategory(Integer categoryId) {
		return ofNullable(subCategoryRepository.findSubCategoryByCategoryId(categoryId)).orElse(Collections.emptyList());
	}

	public Optional<SubCategory> getSubCategory(Integer id) {
		return ofNullable(subCategoryRepository.findOne(id));
	}

	@Override
	public Optional<SubCategory> findByName(String name) {
		return ofNullable(subCategoryRepository.findByName(name));
	}

    @Transactional
    public SubCategory saveSubCategory(@NotNull SubCategory subCategory) {
        return subCategoryRepository.save(subCategory);
    }

    @Transactional
		public void deleteSubCategory(Integer id) {
			subCategoryRepository.delete(id);
    }

    @Transactional
    @Override
		public boolean isSubcategoryExists(Integer id) {
			return subCategoryRepository.exists(id);
    }

}
