package com.orileo.melloe.servicesimpl;

import com.orileo.melloe.model.order.Order;
import com.orileo.melloe.model.order.OrderDetails;
import com.orileo.melloe.model.order.OrderNumber;
import com.orileo.melloe.repositories.OrderNumberRepository;
import com.orileo.melloe.repositories.OrdersRepository;
import com.orileo.melloe.servicesinterface.OrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.Optional.ofNullable;

@Service
public class OrdersServiceImpl implements OrdersService {

	private final OrdersRepository ordersRepository;
	private final OrderNumberRepository orderNumberRepository;

    @Value("${melloe.order.handling.charge}")
    private Double handlingCharge;

    @Value("${melloe.order.gst.charge}")
    private Double gstCharge;

	@Autowired
	public OrdersServiceImpl(OrdersRepository ordersRepository, OrderNumberRepository orderNumberRepository)
	{
		this.ordersRepository = ordersRepository;
		this.orderNumberRepository = orderNumberRepository;
	}

	@Override
	public List<Order> listAllOrders(Integer vendorID, Integer customerID) {
        return newArrayList(ofNullable(ordersRepository.findByVendorAndCustomerID(vendorID, customerID))
                .orElse(Collections.emptyList()));
    }

	@Override
	public Optional<Order> getOrder(Integer id, Integer customerID, Integer vendorID) {
        return ordersRepository.findByIDAndVendorAndCustomerID(id, customerID, vendorID);
    }
	
	@Override
	public Optional<Order> getOrderByIdAndCustomerId(Integer id, Integer customerID) {
        return ordersRepository.findByIDAndCustomerID(id, customerID);
    }

    @Transactional
    @Override
    public Order saveOrder(@NotNull Order order)
    {
        return ordersRepository.save(order);
    }

    @Transactional
    @Override
    public void deleteOrder(Integer id) {
        ordersRepository.delete(id);
    }

    @Transactional
    @Override
    public boolean isOrderExist(Integer id, Integer customerId, Integer vendorId) {
        return ordersRepository.exists(id);
    }

	@Override
	public List<Order> getOrdersByStatus(int vendorId)
	{
        return newArrayList(ofNullable(ordersRepository.getNewOrders(vendorId)).orElse(Collections.emptyList()));
	}
    @Override
    public void calculateOrder(Order order) {
        BigDecimal bigTotalPrice = BigDecimal.ZERO;
        BigDecimal packagingTotalPrice = BigDecimal.ZERO;

        for (OrderDetails orderDetails : order.getOrderDetails()) {
            BigDecimal productTotalPrice = BigDecimal.valueOf(orderDetails.getQuantity())
                    .multiply(BigDecimal.valueOf(orderDetails.getProduct().getProductPrice().getPrice()));
            orderDetails.setTotalPrice(productTotalPrice.doubleValue());
            
            if(orderDetails.getProduct().getProductPrice().isPackagingChargesApplicable())
            {
            	packagingTotalPrice = packagingTotalPrice.add(BigDecimal.valueOf(orderDetails.getProduct().getProductPrice().getPackagingCharges()));
            }
            bigTotalPrice = bigTotalPrice.add(productTotalPrice);
        }
        order.setPackagingCharges(packagingTotalPrice.doubleValue());
        order.setHandlingCharge(handlingCharge);
        bigTotalPrice = bigTotalPrice.add(packagingTotalPrice);
        bigTotalPrice = bigTotalPrice.add(BigDecimal.valueOf(handlingCharge));

        final BigDecimal bigGstCharge = BigDecimal.valueOf(handlingCharge)
                .multiply(BigDecimal.valueOf(gstCharge)).divide(BigDecimal.valueOf(100));
        bigTotalPrice = bigTotalPrice.add(bigGstCharge);
        order.setGstCharge(bigGstCharge.doubleValue());

        bigTotalPrice = bigTotalPrice.setScale(0, RoundingMode.CEILING);
        order.setTotalPrice(bigTotalPrice.doubleValue());
    }

	@Override
	public void saveOrderNumber(@NotNull OrderNumber orderNumber)
	{
		orderNumberRepository.save(orderNumber);
	}

	@Override
	public Optional<OrderNumber> getOrderNumber(Integer id)
	{
		return ofNullable(orderNumberRepository.findOne(id));
	}
}