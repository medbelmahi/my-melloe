package com.orileo.melloe.servicesimpl;

import com.orileo.melloe.model.order.DeliveryBoy;
import com.orileo.melloe.repositories.DeliveryBoyRepository;
import com.orileo.melloe.servicesinterface.DeliveryBoyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.Optional.ofNullable;


@Service
public class DeliveryBoyServiceImpl implements DeliveryBoyService{

	private final DeliveryBoyRepository deliveryBoyRepository;

	@Autowired
	public DeliveryBoyServiceImpl(DeliveryBoyRepository deliveryBoyRepository) {
		this.deliveryBoyRepository = deliveryBoyRepository;
	}


	@Override
	public List<DeliveryBoy> listAllDeliveryBoys() {
		return newArrayList(ofNullable(deliveryBoyRepository.findAll())
				.orElse(Collections.emptyList()));
	}

	@Override
	public Optional<DeliveryBoy> getDeliveryBoy(Integer id) {
		return ofNullable(deliveryBoyRepository.findOne(id));
	}

    @Transactional
    @Override
    public DeliveryBoy saveDeliveryBoy(@NotNull DeliveryBoy deliveryboy) {
        return deliveryBoyRepository.save(deliveryboy);
    }

    @Transactional
    @Override
    public void deleteDeliveryBoy(Integer id) {
        deliveryBoyRepository.delete(id);
    }

    @Transactional
    @Override
    public boolean isDeliveryBoyExists(Integer id) {
        return deliveryBoyRepository.exists(id);
    }
}
