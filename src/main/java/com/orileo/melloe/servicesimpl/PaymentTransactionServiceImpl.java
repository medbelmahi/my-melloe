package com.orileo.melloe.servicesimpl;

import com.orileo.melloe.model.payment.PaymentTransaction;
import com.orileo.melloe.repositories.PaymentTransactionRepository;
import com.orileo.melloe.servicesinterface.PaymentTransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Mohamed BELMAHI on 21/08/2017.
 */
@Service
public class PaymentTransactionServiceImpl implements PaymentTransactionService {

    private PaymentTransactionRepository paymentTransactionRepository;

    @Autowired
    public PaymentTransactionServiceImpl(PaymentTransactionRepository paymentTransactionRepository) {
        this.paymentTransactionRepository = paymentTransactionRepository;
    }

    @Override
    public void save(PaymentTransaction paymentTransaction) {
        paymentTransactionRepository.save(paymentTransaction);
    }

    @Override
    public PaymentTransaction getPaymentTransactionById(String id) {
        return paymentTransactionRepository.findOne(id);
    }

    @Override
    public List<PaymentTransaction> getAuthorizedPaymentTransactions() {
        return null;
    }

}
