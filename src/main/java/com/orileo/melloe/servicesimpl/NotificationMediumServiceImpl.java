package com.orileo.melloe.servicesimpl;

import com.orileo.melloe.model.notifications.NotificationMedium;
import com.orileo.melloe.repositories.NotificationMediumRepository;
import com.orileo.melloe.servicesinterface.NotificationMediumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.Optional.ofNullable;


@Service
public class NotificationMediumServiceImpl implements NotificationMediumService{

	private final NotificationMediumRepository notificationMediumRepository;

	@Autowired
	public NotificationMediumServiceImpl(NotificationMediumRepository notificationMediumRepository) {
		this.notificationMediumRepository = notificationMediumRepository;
	}

	@Override
	public List<NotificationMedium> listAllNotificationMediums() {
		return newArrayList(ofNullable(notificationMediumRepository.findAll())
				.orElse(Collections.emptyList()));
	}

	@Override
	public Optional<NotificationMedium> getNotificationMedium(Integer id) {
		return ofNullable(notificationMediumRepository.findOne(id));
	}

    @Transactional
    @Override
    public NotificationMedium saveNotificationMedium(@NotNull NotificationMedium notificationMedium) {
        return notificationMediumRepository.save(notificationMedium);
    }

    @Transactional
    @Override
    public void deleteNotificationMedium(Integer id) {
        notificationMediumRepository.delete(id);
    }

    @Transactional
    @Override
    public boolean isNotificationMediumExist(Integer id) {
        return notificationMediumRepository.exists(id);
    }
}
