package com.orileo.melloe.servicesimpl;

import com.orileo.melloe.model.notifications.Notification;
import com.orileo.melloe.repositories.NotificationsRepository;
import com.orileo.melloe.servicesinterface.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.Optional.ofNullable;


@Service
public class NotificationServiceImpl implements NotificationService {

    private final NotificationsRepository notificationsRepository;

    @Autowired
    public NotificationServiceImpl(NotificationsRepository notificationsRepository) {
        this.notificationsRepository = notificationsRepository;
    }

    @Override
    public List<Notification> listAllNotifications() {
        return newArrayList(ofNullable(notificationsRepository.findAll())
                .orElse(Collections.emptyList()));
    }

    @Override
    public Optional<Notification> getNotification(Integer id) {
        return ofNullable(notificationsRepository.findOne(id));
    }

    @Transactional
    @Override
    public Notification saveNotification(@NotNull Notification notification) {
        return notificationsRepository.save(notification);
    }

    @Transactional
    @Override
    public void deleteNotification(Integer id) {
        notificationsRepository.delete(id);
    }

    @Transactional
    @Override
    public boolean isNotificationExist(Integer id) {
        return notificationsRepository.exists(id);
    }
}
