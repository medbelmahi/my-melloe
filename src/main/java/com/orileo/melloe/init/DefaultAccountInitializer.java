package com.orileo.melloe.init;

import com.orileo.melloe.model.login.MasterLogin;
import com.orileo.melloe.model.vendor.UserType;
import com.orileo.melloe.servicesinterface.LoginService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Slf4j
@Component
public class DefaultAccountInitializer implements InitializingBean {
    private final LoginService loginService;
    private final String defaultAdminLogin;
    private final String defaultAdminPassword;

    @Autowired
    public DefaultAccountInitializer(LoginService loginService,
                                     @Value("${default.admin.login}")
                                             String defaultAdminLogin,
                                     @Value("${default.admin.password}")
                                             String defaultAdminPassword) {
        this.loginService = loginService;
        this.defaultAdminLogin = defaultAdminLogin;
        this.defaultAdminPassword = defaultAdminPassword;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Optional<MasterLogin> adminLogin = loginService.findByUserType(UserType.ADMIN);
        if (!adminLogin.isPresent()) {
            MasterLogin masterLogin = new MasterLogin(null, defaultAdminLogin,
                    defaultAdminPassword, UserType.ADMIN);
            loginService.saveLogin(masterLogin);
            log.info("Created default admin with login {}", defaultAdminLogin);
        }
    }
}
