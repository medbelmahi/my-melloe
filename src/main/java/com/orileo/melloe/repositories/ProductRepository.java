package com.orileo.melloe.repositories;

import com.orileo.melloe.model.category.Category;
import com.orileo.melloe.model.product.Product;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@EnableJpaRepositories
public interface ProductRepository extends CrudRepository<Product, Integer>{
    @Query("select p from Product p join fetch p.category pc where " +
            " (:categoryID is null or pc.id in (:categoryID)) and " +
            "(:vendorID is null or p.vendor.id in (:vendorID))")
    List<Product> findProducts(@Param("categoryID") List<Integer> categoryID,
                               @Param("vendorID") List<Integer> vendorID);

	List<Product> findProductsByCategoryContainsAndVendorIdIn(Category category, List<Integer> vendorID);

	@Query("select p from Product p join fetch p.category pc "
    		+ "where p.vendor.id =:vendorID)")
    List<Product> findProductsByVendorId(@Param("vendorID") Integer vendorID);

	@Query("select p from Product p " +
			  "where (:maxPrice is null or p.productPrice.price<=:maxPrice) and " +
			  "(:minPrice is null or p.productPrice.price>=:minPrice) and " +
			  "(:name is null or p.name=:name) and " +
			  "(:vegetarian is null or p.isVegetarian=:vegetarian) and " +
			  "(:discounted is null or p.isDiscounted=:discounted)")
			List<Product> findByQuery(
				@Param("maxPrice") Double maxPrice,
				@Param("minPrice") Double minPrice,
				@Param("name") String name,
				@Param("discounted") Boolean discounted,
				@Param("vegetarian") Boolean vegetarian);
  
	@Query("select p from Product p where  p.vendor.id in (:vendorID)")
	List<Product> vendorsProduct(@Param("vendorID") List<Integer> vendorID);
}