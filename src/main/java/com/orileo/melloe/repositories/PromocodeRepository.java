package com.orileo.melloe.repositories;

import com.orileo.melloe.model.promocode.PromoCode;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;




@Transactional
@EnableJpaRepositories
public interface PromocodeRepository extends CrudRepository<PromoCode, String> {
}
