package com.orileo.melloe.repositories;

import java.io.Serializable;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;
import com.orileo.melloe.model.customer.CustomerContactForm;

@Transactional
@EnableJpaRepositories
public interface CustomerContactFormRepository extends CrudRepository<CustomerContactForm, Serializable>
{

}
