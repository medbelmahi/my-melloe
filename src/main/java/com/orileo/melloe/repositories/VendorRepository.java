package com.orileo.melloe.repositories;

import com.orileo.melloe.model.vendor.Vendor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

@EnableJpaRepositories
public interface VendorRepository extends CrudRepository<Vendor, Integer>{
    @Query("select v from Vendor v join fetch v.userId e where e.emailId=:email")
    Vendor findByEmail(@Param("email") String email);
}
