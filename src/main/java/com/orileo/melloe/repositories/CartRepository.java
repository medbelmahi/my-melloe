package com.orileo.melloe.repositories;

import java.io.Serializable;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.orileo.melloe.model.cart.Cart;

@Transactional
@EnableJpaRepositories
public interface CartRepository extends CrudRepository<Cart, Serializable> {

	@Query("select o from Cart o join fetch o.cartDetails od"
			+ " where o.id=:id and (:customerID is null or o.customer.id=:customerID) ")
	Optional<Cart> findByIDAndCustomerID(@Param("id") Integer id, @Param("customerID") Integer customerID);

	@Query("select c from Cart c where c.sessionID=:sessionID")
	Optional<Cart> findBySessionId(@Param("sessionID") String sessionId);
}
