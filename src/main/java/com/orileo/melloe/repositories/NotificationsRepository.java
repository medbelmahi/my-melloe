package com.orileo.melloe.repositories;

import com.orileo.melloe.model.notifications.Notification;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;





@Transactional
@EnableJpaRepositories
public interface NotificationsRepository extends CrudRepository<Notification, Integer> {

}
