package com.orileo.melloe.repositories;

import com.orileo.melloe.model.payment.PaymentTransaction;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.io.Serializable;

/**
 * Created by Mohamed BELMAHI on 21/08/2017.
 */
@Transactional
@EnableJpaRepositories
public interface PaymentTransactionRepository extends CrudRepository<PaymentTransaction, Serializable> {
}
