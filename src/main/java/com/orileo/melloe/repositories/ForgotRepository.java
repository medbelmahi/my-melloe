package com.orileo.melloe.repositories;

import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;
import com.orileo.melloe.model.forgotpassword.PasswordResetToken;
import com.orileo.melloe.model.login.MasterLogin;

@Transactional
@EnableJpaRepositories
public interface ForgotRepository extends CrudRepository<PasswordResetToken, Integer>{
	PasswordResetToken findByUser(MasterLogin user);
	PasswordResetToken findByToken(String token);
	
	@Modifying
    @Query("delete from PasswordResetToken t where t.expiryDate <= ?1")
    void deleteAllExpiredSince(Date now);
}
