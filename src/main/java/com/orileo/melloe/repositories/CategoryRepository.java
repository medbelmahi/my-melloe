package com.orileo.melloe.repositories;

import com.orileo.melloe.model.category.Category;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;


@Transactional
@EnableJpaRepositories
public interface CategoryRepository extends CrudRepository<Category, Integer>{

    Category findByName(String name);
}
