package com.orileo.melloe.repositories;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;

import com.orileo.melloe.model.customer.CustomerLevel;



@Transactional
@EnableJpaRepositories
public interface CustomerLevelRepository extends CrudRepository<CustomerLevel, Integer> {

}
