package com.orileo.melloe.repositories;

import com.orileo.melloe.model.product.ProductReview;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.Optional;


@Transactional
@EnableJpaRepositories
public interface ProductReviewRepository extends CrudRepository<ProductReview, Integer> {

    @Query("select p from ProductReview p where id=:id and (:customerID is null or p.customer.id=:customerID)")
    Optional<ProductReview> findByIDAndCustomerID(@Param("id") Integer id, @Param("customerID") Integer customerID);
}
