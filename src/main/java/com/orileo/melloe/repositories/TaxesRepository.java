package com.orileo.melloe.repositories;

import com.orileo.melloe.model.payment.Tax;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;




@Transactional
@EnableJpaRepositories
public interface TaxesRepository extends CrudRepository<Tax, Integer> {

}
