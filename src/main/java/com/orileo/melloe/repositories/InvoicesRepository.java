package com.orileo.melloe.repositories;

import com.orileo.melloe.model.payment.Invoice;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;


@Transactional
@EnableJpaRepositories
public interface InvoicesRepository extends CrudRepository<Invoice, Integer> {

    @Query("select i from Invoice i where i.order.id=:orderID")
    List<Invoice> listAllInvoicesForOrder(@Param("orderID") Integer orderID);

    @Query("select i from Invoice i where i.order.customer.id=:customerID")
    List<Invoice> listAllInvoicesForCustomer(@Param("customerID") int customerID);

  @Query("select i from Invoice i where i.order.id=:orderID")
  Invoice findByOrder(@Param("orderID") int id);
}
