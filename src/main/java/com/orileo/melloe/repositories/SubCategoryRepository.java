package com.orileo.melloe.repositories;

import com.orileo.melloe.model.category.SubCategory;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;


@Transactional
@EnableJpaRepositories
public interface SubCategoryRepository extends CrudRepository<SubCategory, Integer>{

    List<SubCategory> findSubCategoryByCategoryId(int categoryId);

    SubCategory findByName(String name);
}
