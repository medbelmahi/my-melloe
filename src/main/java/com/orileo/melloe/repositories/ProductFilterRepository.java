package com.orileo.melloe.repositories;

import com.orileo.melloe.model.customer.Customer;
import com.orileo.melloe.model.filter.ProductFilter;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Transactional
@EnableJpaRepositories
public interface ProductFilterRepository extends CrudRepository<ProductFilter, Integer> {
    List<ProductFilter> findByCustomer(Customer customer);

    ProductFilter findByName(String name);

    @Query("select p from ProductFilter p where id=:id and (:customerID is null or p.customer.id=:customerID)")
    Optional<ProductFilter> getByIdAndCustomer(@Param("id") int id, @Param("customerID") Integer customerID);

    @Query("select p from ProductFilter p where :customerID is null or p.customer.id=:customerID")
    List<ProductFilter> findAllForCustomer(@Param("customerID") Integer customerID);
}
