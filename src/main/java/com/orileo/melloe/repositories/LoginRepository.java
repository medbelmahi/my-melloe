package com.orileo.melloe.repositories;

import com.orileo.melloe.model.login.MasterLogin;
import com.orileo.melloe.model.vendor.UserType;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;


@Transactional
@EnableJpaRepositories
public interface LoginRepository extends CrudRepository<MasterLogin, Integer> {

	MasterLogin findByEmailId(String emailId);

    MasterLogin findByUserType(UserType userType);
}
