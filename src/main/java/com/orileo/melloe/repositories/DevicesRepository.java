package com.orileo.melloe.repositories;

import com.orileo.melloe.model.notifications.Device;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;





@Transactional
@EnableJpaRepositories
public interface DevicesRepository extends CrudRepository<Device, Integer> {

}
