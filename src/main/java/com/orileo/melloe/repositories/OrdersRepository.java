package com.orileo.melloe.repositories;

import com.orileo.melloe.model.order.Order;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;

@Transactional
@EnableJpaRepositories
public interface OrdersRepository extends CrudRepository<Order, Serializable> {

    @Query("select distinct o from Order o join fetch o.orderDetails od" +
            " where (:customerID is null or o.customer.id=:customerID) and " +
            " (:vendorID is null or od.product.vendor.id=:vendorID)")
    List<Order> findByVendorAndCustomerID(@Param("vendorID") Integer vendorID, @Param("customerID") Integer customerID);

    @Query("select o from Order o join fetch o.orderDetails od" +
            " where o.id=:id and (:customerID is null or o.customer.id=:customerID) and " +
            " (:vendorID is null or od.product.vendor.id=:vendorID)")
    Optional<Order> findByIDAndVendorAndCustomerID(@Param("id") Integer id, @Param("customerID") Integer customerID,
                                                   @Param("vendorID") Integer vendorID);

    @Query("select o from Order o join fetch o.orderDetails od" +
            " where o.id=:id and (:customerID is null or o.customer.id=:customerID) ")
    Optional<Order> findByIDAndCustomerID(@Param("id") Integer id, @Param("customerID") Integer customerID);

    @Query("select distinct o from Order o join fetch o.orderDetails od" +
            " where od.product.vendor.id=:vendorID and o.orderStatus='ORDER_CREATED'")
    List<Order> getNewOrders(@Param("vendorID") Integer vendorID);
}