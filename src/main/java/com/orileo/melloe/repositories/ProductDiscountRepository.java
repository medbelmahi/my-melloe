package com.orileo.melloe.repositories;

import com.orileo.melloe.model.product.ProductDiscount;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.Optional;

@Transactional
@EnableJpaRepositories
public interface ProductDiscountRepository extends CrudRepository<ProductDiscount, Integer>{

    @Query("select p from ProductDiscount p where id=:id and (:vendorID is null or p.product.vendor.id=:vendorID)")
    Optional<ProductDiscount> findByIdAndDiscount(@Param("id") int id, @Param("vendorID") Integer vendorId);
}
