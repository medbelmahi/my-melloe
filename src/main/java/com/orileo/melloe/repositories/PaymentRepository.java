package com.orileo.melloe.repositories;

import com.orileo.melloe.model.payment.PaymentTransaction;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Mohamed BELMAHI on 24/08/2017.
 */
@Transactional
@EnableJpaRepositories
public interface PaymentRepository extends CrudRepository<PaymentTransaction, Serializable> {
    @Query
    List<PaymentTransaction> findAllByCapturedIsFalse();
}
