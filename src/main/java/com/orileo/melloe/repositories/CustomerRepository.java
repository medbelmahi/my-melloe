package com.orileo.melloe.repositories;

import com.orileo.melloe.model.customer.Customer;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;

@Transactional
@EnableJpaRepositories
public interface CustomerRepository extends CrudRepository<Customer, Integer>{

    @Query("select c from Customer c where c.userId.emailId=:email")
    Customer findByEmail(@Param("email") String email);
}

