package com.orileo.melloe.repositories;

import com.orileo.melloe.model.customer.Customer;
import com.orileo.melloe.model.customer.CustomerPromoCode;
import com.orileo.melloe.model.promocode.PromoCode;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;

@Transactional
@EnableJpaRepositories
public interface CustomerPromoCodeRepository extends CrudRepository<CustomerPromoCode, Serializable> {
	
	List<CustomerPromoCode> findCustomerPromoCodeByPromoCode(PromoCode promocode);
	List<CustomerPromoCode> findCustomerPromoCodeByCustomerId(Customer customer);
	CustomerPromoCode findCustomerPromoCodeByPromoCodeAndCustomerId(
			PromoCode promocode, Customer customer);

    @Query("select cp " +
            "from CustomerPromoCode cp where " +
            " (:customerID is null or cp.customer.id in :customerID) and" +
            " (:promocode is null or cp.promoCode in :promocode)")
    List<CustomerPromoCode> findByCustomerIDAndPromocode(@Param("customerID") List<Integer> customerID,
                                                         @Param("promocode") List<String> promocode);
    
    
    
    @Query("select pcs from CustomerPromoCode pcs where " +
            " (:promoCode is null or pcs.promoCode.promoCode =:promoCode) and " +
            "(:idAddress is null or pcs.ipAddress =:idAddress) and "
            + "(:appID is null or pcs.appId =:appID) and"
            + "(:customerID is null or pcs.customer.id =:customerID) ")
    List<CustomerPromoCode> findPromoCodeStatus(@Param("customerID") Integer customerId,
											@Param("promoCode") String promocode,
											@Param("idAddress") String ipAddress,
											@Param("appID") String appId);
}
