package com.orileo.melloe.commonmethods;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.HmacUtils;

import com.orileo.melloe.model.order.Order;

public class CommonMethods {
	public static String generateHash(String input) {
		StringBuilder hash = new StringBuilder();

		try {
			MessageDigest sha = MessageDigest.getInstance("SHA-1");
			byte[] hashedBytes = sha.digest(input.getBytes());
			char[] digits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
					'a', 'b', 'c', 'd', 'e', 'f' };
            for (byte b : hashedBytes) {
                hash.append(digits[(b & 0xf0) >> 4]);
                hash.append(digits[b & 0x0f]);
            }
        } catch (NoSuchAlgorithmException e) {
			// handle error here.
		}

		return hash.toString();
	}
	
	/**
	 * Create HMAC hash value for data based on the private key passed in parameters.
	 * @param privateKey
	 * @param data
	 * @return
	 */
	public static String generateHmacKey(String privateKey, String data) {
		byte[] macValue = HmacUtils.hmacSha256(privateKey, data);
		return Hex.encodeHexString(macValue);
	}
	
	
	/**
	* <b>Note:</b> common methods act like templets
	* @author  Kapil Halewale
	* @version 0.2 Beta
	* @since   2017-08-23
	*/
	
	public static String vendorMessage(Order order)
	{
		final String message ="Dear, " +order.getOrderDetails().get(0).getProduct().getVendor().getName()+ "" +System.lineSeparator()
				+ "new order "+order.getOrderNumber()+" is placed." +System.lineSeparator()
				+ "Open the app or website and 'Accept' the Order";
System.out.println("Vendor SMS " +message);

		return message;
	}
	
	public static String customerMessage(Order order)
	{
		final String message = "Order Received "+order.getOrderNumber()+" for "
								+ "'"+order.getOrderDetails().get(0).getProduct().getVendor().getName()+"' " +System.lineSeparator()
								+ "and will be delivered on "+order.getShippedDate()+". " +System.lineSeparator()
								+ "Please login & goto past orders to track your order online";
System.out.println("Customer SMS " +message);
		
		return message;
	}
	
	public static String customerOrderAssignedMessage()
	{
		final String message = "Order is assigned to a delivery boy. "
								+ "Your order will arrive shortly. "
								+ "Please login & goto past orders to track your order online";
		
		return message;
	}
	
	public static String customerOrderDeliveredMessage()
	{
		final String message = "Order delivered successfully. MELLOE is always here for your dessert needs."
							+ "	Have anything to say? Drop a mail to support@melloe.in. Have a great day";
		
		return message;
	}
}