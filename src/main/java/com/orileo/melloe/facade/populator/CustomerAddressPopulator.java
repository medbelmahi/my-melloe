package com.orileo.melloe.facade.populator;

import com.google.maps.model.LatLng;
import com.orileo.melloe.controllers.dto.LocateMeForm;
import com.orileo.melloe.model.customer.CustomerAddress;
import com.orileo.melloe.servicesinterface.GoogleMapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Mohamed BELMAHI on 31/08/2017.
 */
@Component("customerAddressPopulator")
public class CustomerAddressPopulator implements Populator<CustomerAddress, LocateMeForm> {

    @Autowired
    private GoogleMapService googleMapService;

    @Override
    public void populate(CustomerAddress customerAddress, LocateMeForm locateMeForm) {

    }

    @Override
    public CustomerAddress convert(LocateMeForm locateMeForm) {
        final LatLng latLngOrigin = new LatLng(Double.valueOf(locateMeForm.getLat()), Double.valueOf(locateMeForm.getLng()));
        CustomerAddress customerAddress = googleMapService.getNewAddressFrom(latLngOrigin);
        customerAddress.setAddressName(locateMeForm.getAddressName());
        customerAddress.setAddressLine(locateMeForm.getAddress());
        customerAddress.setAddressLine_2(locateMeForm.getHouse());
        customerAddress.setNearestLandmark(locateMeForm.getLandmark());
        customerAddress.setLatitude(locateMeForm.getLat().substring(0, locateMeForm.getLat().length() < 15 ? locateMeForm.getLat().length() : 15));
        customerAddress.setLongitude(locateMeForm.getLng().substring(0, locateMeForm.getLng().length() < 15 ? locateMeForm.getLng().length() : 15));
        return customerAddress;
    }
}
