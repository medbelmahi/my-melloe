package com.orileo.melloe.facade.populator;

import com.orileo.melloe.controllers.dto.SelectedAddress;
import com.orileo.melloe.model.customer.CustomerAddress;
import org.springframework.stereotype.Component;

/**
 * Created by Mohamed BELMAHI on 08/09/2017.
 */
@Component("cartAddressPopulator")
public class CartAddressPopulator implements Populator<CustomerAddress, SelectedAddress> {

    @Override
    public void populate(CustomerAddress customerAddress, SelectedAddress selectedAddress) {

    }

    @Override
    public CustomerAddress convert(SelectedAddress selectedAddress) {
        CustomerAddress customerAddress = new CustomerAddress();
        customerAddress.setAddressLine_2(selectedAddress.getAddressLine_2());
        customerAddress.setAddressLine(selectedAddress.getAddressLine());
        customerAddress.setPincode(selectedAddress.getPincode());
        customerAddress.setState(selectedAddress.getState());
        customerAddress.setCity(selectedAddress.getCity());
        customerAddress.setAddressName(selectedAddress.getAddressName());
        customerAddress.setNearestLandmark(selectedAddress.getNearestLandmark());
        customerAddress.setLatitude(selectedAddress.getLatitude());
        customerAddress.setLongitude(selectedAddress.getLongitude());
        return customerAddress;
    }
}
