package com.orileo.melloe.facade.populator;

/**
 * Created by Mohamed BELMAHI on 31/08/2017.
 */
public interface Populator<SOURCE, TARGET> {
    void populate(SOURCE source, TARGET target);
    SOURCE convert(TARGET target);
}
