package com.orileo.melloe.model.category;

import com.fasterxml.jackson.annotation.JsonBackReference;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Setter
@Getter
@EqualsAndHashCode
@ToString
@Entity(name = "subCategory")
@Table(name="sub_category")
public class SubCategory {
	private static final long serialVersionUID = 1L;

    @ApiModelProperty(hidden = true)
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

	@ApiModelProperty(notes = "The name of the subcategory", required = true,
			example = "new-york cheescake")
	@NotNull
	@Size(max = 50)
	@Column(name = "name", nullable = false, length = 50)
	private String name;

	@JsonBackReference
	@ApiModelProperty(notes = "Parent Category", required = true, hidden = true)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "category_id")
	private Category category;
}
