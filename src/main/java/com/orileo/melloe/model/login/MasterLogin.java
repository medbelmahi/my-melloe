package com.orileo.melloe.model.login;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.orileo.melloe.model.vendor.UserType;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="master_logins")
public class MasterLogin implements Serializable {
    private static final long serialVersionUID = -1742192218541616372L;

    @ApiModelProperty(hidden = true)
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @ApiModelProperty(notes = "Email of the User")
    @NotNull
    @Size(max = 100)
    @Column(name = "email_id", unique = true, nullable = false, length = 100)
    private String emailId;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ApiModelProperty(notes = "User password")
    @Size(max = 100)
    @Column(name = "password", length = 100)
    private String password;

    @Enumerated(EnumType.STRING)
    @Column(name = "user_type", nullable = false)
    private UserType userType;
}
