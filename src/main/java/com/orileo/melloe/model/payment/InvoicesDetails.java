package com.orileo.melloe.model.payment;

import com.orileo.melloe.model.product.Product;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Setter
@Getter
@EqualsAndHashCode
@ToString
@Embeddable
public class InvoicesDetails implements Serializable{
    private static final long serialVersionUID = 1L;

    @ManyToOne(cascade=CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name ="product_id")
    private Product product;

    @ApiModelProperty(notes = "Quantity of the Product")
    @NotNull
    @Column(name="quantity")
    private Integer quantity;

    @ApiModelProperty(notes = "Price per Unit")
    @NotNull
    @Column(name="price_per_unit")
    private Double pricePerUnit;

    @ApiModelProperty(notes = "Total Price")
    @NotNull
    @Column(name="total_price")
    private double totalprice;
}
