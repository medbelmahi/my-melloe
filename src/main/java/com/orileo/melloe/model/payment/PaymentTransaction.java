package com.orileo.melloe.model.payment;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by Mohamed BELMAHI on 21/08/2017.
 */
@Setter
@Getter
@EqualsAndHashCode
@ToString
@Entity
@Table(name="payment_transaction")
public class PaymentTransaction implements Serializable{

    @Id
    @Column(name = "id", unique = true)
    private String id;
    @Column(name = "amount")
    private Integer amount;
    @Column(name = "status")
    private String status;
    @Column(name = "order_id")
    private String orderId;
    @Column(name = "capture")
    private boolean captured;
    @Column(name = "created_at")
    private long created_at;
}
