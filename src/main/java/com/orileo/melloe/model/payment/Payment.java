package com.orileo.melloe.model.payment;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Setter
@Getter
@EqualsAndHashCode
@ToString
@Embeddable
public class Payment implements Serializable {
    private static final long serialVersionUID = 1L;

    private double paymentAmount;

    private String paymentType;
}
