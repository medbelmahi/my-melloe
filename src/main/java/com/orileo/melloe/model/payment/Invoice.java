package com.orileo.melloe.model.payment;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.orileo.melloe.model.customer.CustomerPromoCode;
import com.orileo.melloe.model.order.Order;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class,
        property = "@id")
@Setter
@Getter
@EqualsAndHashCode
@ToString
@Entity
@Table(name="invoices")
public class Invoice implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(hidden = true)
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ApiModelProperty(notes = "from order table")
    @NotNull
    @ManyToOne
    @JoinColumn(name ="order_id")
    private Order order;

    @ApiModelProperty(notes = "from customer promo table")
    @ManyToOne
    @JoinColumn(name = "customer_promo_code_id")
    private CustomerPromoCode promoCode;

    @ApiModelProperty(notes = "date of the payment")
    @Column(name="payment_date")
    private Date paymentDate;

    @ApiModelProperty(notes = "Total Amount of the Order")
    @Column(name="total_amount")
    private Double totalAmount;

    @ApiModelProperty(notes = "Delivery Charges")
    @Column(name="delivery_charges")
    private Double deliveryCharges;

    @ApiModelProperty(notes = "Packing Charges")
    @Column(name="packing_charges")
    private Double packingCharges;

    @ApiModelProperty(notes = "Other Taxes")
    @Column(name="other_taxes")
    private Double otherTaxes;

    @ApiModelProperty(notes = "Grand Total")
    @Column(name="grand_total")
    private Double grandTotal;

    @ApiModelProperty(notes = "Is Invoice Refunded")
    @Column(name="is_refunded")
    private Boolean isRefunded;

    @ApiModelProperty(notes = "Refund Amount")
    @Column(name="refund_amount")
    private Double refundAmount;

    @ApiModelProperty(notes = "Attachment")
    @Column(name = "Attachment")
    private String attachment;

    @ApiModelProperty(notes = "Invoice Details for individual products")
    @LazyCollection(LazyCollectionOption.FALSE)
    @ElementCollection
    @Embedded
    private List<InvoicesDetails> invoicesDetails;


    @ApiModelProperty(notes = "Payments for this Invoice")
    @LazyCollection(LazyCollectionOption.FALSE)
    @ElementCollection
    @Embedded
    private List<Payment> payments;
}
