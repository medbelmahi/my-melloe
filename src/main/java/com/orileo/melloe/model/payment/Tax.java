package com.orileo.melloe.model.payment;

import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Setter
@Getter
@EqualsAndHashCode
@ToString
@Entity
@Table(name="taxes")
public class Tax implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(hidden = true)
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ApiModelProperty(name = "Type of the tax", required = true)
    @NotNull
    @Column(name = "tax_type", nullable = false)
    private String taxType;

    @ApiModelProperty(name = "Percentage Amount", required = true)
    @NotNull
    @Column(name = "percentage_amount", nullable = false)
    private double percentageAmount;
}
