package com.orileo.melloe.model.order;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.orileo.melloe.model.customer.Customer;
import com.orileo.melloe.model.customer.CustomerAddress;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by Mohamed BELMAHI on 06/08/2017.
 */
@Setter
@Getter
@MappedSuperclass
public abstract class AbstractOrder implements Serializable {
    @ApiModelProperty(hidden = true)
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ApiModelProperty(hidden = true)
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "customer_id")
    @JsonBackReference
    private Customer customer;

    @ApiModelProperty(notes = "Total Price of the Order")
    @NotNull
    private Double totalPrice;

    @ApiModelProperty(notes = "handling charge")
    @NotNull
    private Double handlingCharge;

    @ApiModelProperty(notes = "gst charge")
    @NotNull
    private Double gstCharge;
    
	@ApiModelProperty(notes = "Total Packaging Charges")
	private Double packagingCharges;

    @ApiModelProperty(notes = "Delivery address of customer for this product")
    private CustomerAddress deliveryAddress;
    
    @ApiModelProperty(notes = "promocode discounted amount")
    private Double promoCodeAmount;

    @ApiModelProperty(notes = "totalWithPromocode")
    private Double totalOrderPriceWithPromocode;
    

    @ApiModelProperty(notes = "promoCode")
    private String promoCode;

}