package com.orileo.melloe.model.order;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.orileo.melloe.model.customer.CustomerAddress;

import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@EqualsAndHashCode(callSuper = false)
@ToString
@Entity
@Table(name = "orders")
public class Order extends AbstractOrder implements Serializable {
	private static final long serialVersionUID = -13646745294137693L;

	@ApiModelProperty(notes = "Created Date")
	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm")
	private Date createdDate;

	@ApiModelProperty(notes = "Date of the Last Update")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_updated")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm")
	private Date lastUpdated;

	@ApiModelProperty(notes = "Date when the Order is Required")
	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "required_date")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm")
	private Date requiredDate;

	@ApiModelProperty(notes = "Shipment Date")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "shipped_date")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm")
	private Date shippedDate;

	@ApiModelProperty(notes = "Status of Order")
	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "order_status")
	private OrderStatus orderStatus;
	@ApiModelProperty(notes = "Order Details on Individual Products")
	@NotNull
	@ElementCollection
	@LazyCollection(LazyCollectionOption.FALSE)
	@Embedded
	private List<OrderDetails> orderDetails;

	@ApiModelProperty(notes = "Currency of the Order", required = true, example = "GBP")
	@NotNull
	@Size(max = 10)
	@Column(name = "currency", nullable = false, length = 10)
	private String currency;
	
	@ApiModelProperty(notes = "Order comments")
	@Column(name = "comments")
	private String comment;
	
	@ApiModelProperty(notes = "Order Type")
	@Column(name = "order_type")
	private String orderType;
	
	@ApiModelProperty(notes = "Grab Order Id")
	@Column(name = "grab_order_id")
	private Integer grabOrderId;

	
	@ApiModelProperty(notes = "order number")
	@Column(name = "order_number", length=20, nullable=true)	
	@Size(max = 20)
	private String orderNumber;

	@Override
	@NotNull
	public CustomerAddress getDeliveryAddress() {
		return super.getDeliveryAddress();
	}
}
