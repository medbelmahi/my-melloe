package com.orileo.melloe.model.order;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@EqualsAndHashCode
@ToString
@Entity
@Table(name="order_number")
public class OrderNumber implements Serializable
{
	private static final long serialVersionUID = -7005484460166361580L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)    
    private int id;
	
    @NotNull
    @Column(name = "number", nullable = false, unique = true)
    private int number;
}