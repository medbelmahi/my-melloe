package com.orileo.melloe.model.order;

import com.orileo.melloe.model.product.Product;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Setter
@Getter
@EqualsAndHashCode
@ToString
@Embeddable
public class OrderDetails  implements Serializable{
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(notes = "Order's product")
	@NotNull
	@ManyToOne
	@JoinColumn(name = "productID")
	private Product product;

	@ApiModelProperty(notes = "Quantity of the Product")
	@NotNull
	private int quantity;

	@ApiModelProperty(notes = "Actual Unit Price of the Product")
	@NotNull
	private Double pricePerUnit;
	
	@ApiModelProperty(notes = "Total Price of the Product in the Order")
	@NotNull
	private Double totalPrice;
	
	@ApiModelProperty(notes = "Price of the Packaging in the Order")
	@JoinColumn(name = "packaging_pricce")
	private Double packagingPrice;
}