package com.orileo.melloe.model.order;

import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Setter
@Getter
@EqualsAndHashCode
@ToString
@Entity
@Table(name="delivery_boy")
public class DeliveryBoy implements Serializable {
	private static final long serialVersionUID = 1L;

    @ApiModelProperty(hidden = true)
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

	@ApiModelProperty(notes = "Delivery Boy first name", required = true)
	@NotNull
	@Size(max = 50)
	@Column(name = "first_name", length = 50, nullable = false)
	private String firstName;

	@ApiModelProperty(notes = "Delivery Boy last name", required = true)
	@NotNull
	@Size(max = 50)
	@Column(name = "last_name", length = 50, nullable = false)
	private String lastName;

	@ApiModelProperty(notes = "Delivery Boy phone number", required = true)
	@NotNull
	@Size(max = 30)
	@Column(name = "phone_number", length = 30, nullable = false)
	private String phoneNumber;
}
