package com.orileo.melloe.model.exceptions;

public class MelloeException extends RuntimeException {

	private static final long serialVersionUID = -6410398532738591684L;

	public MelloeException(String message) {
        super(message);
    }

    public MelloeException() {
        super("Something went wrong on Melloe application. Please contact support for help");
    }
}
