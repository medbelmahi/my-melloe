package com.orileo.melloe.model.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by Mohamed BELMAHI on 24/08/2017.
 */
@ResponseStatus(value= HttpStatus.BAD_REQUEST, reason="Your cart already contains some desserts. You can either choose " +
                "from the suggested lip smacking desserts below or Reset Cart to try some different desserts")
public class ProductIsNotFromSameVendorException extends MelloeException {
    public ProductIsNotFromSameVendorException(String message) {
        super(message);
    }
}
