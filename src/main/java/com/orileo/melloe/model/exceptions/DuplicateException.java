package com.orileo.melloe.model.exceptions;

import static java.lang.String.format;

public class DuplicateException extends MelloeException {
	private static final long serialVersionUID = 1667909927406180923L;

	public DuplicateException(String entityType, String duplicateText) {
        super(format("The %s with %s already exists. Please specify a new one",
                entityType, duplicateText));
    }
}
