package com.orileo.melloe.model.exceptions;

public class EntityNotFoundException extends MelloeException {

	private static final long serialVersionUID = -91651412072722734L;

	public EntityNotFoundException(String entity, Object id) {
        super(String.format("%s with ID %s not found", entity, id));
    }

    public EntityNotFoundException(String entityTypes) {
        super(String.format("No %s found", entityTypes));
    }
}
