package com.orileo.melloe.model.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by Mohamed BELMAHI on 03/09/2017.
 */
@ResponseStatus(value= HttpStatus.BAD_REQUEST, reason="Your address is not under 3.5km radius")
public class IsNotUnderRadius extends MelloeException {
    public IsNotUnderRadius(String message) {
        super(message);
    }
}
