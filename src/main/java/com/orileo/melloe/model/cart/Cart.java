package com.orileo.melloe.model.cart;

import com.orileo.melloe.model.order.AbstractOrder;

import groovy.transform.ToString;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Setter
@Getter
@Entity
@Table(name="cart")
@ToString
public class Cart extends AbstractOrder implements Serializable {
	private static final long serialVersionUID = -2476345447750808614L;

    @ApiModelProperty(notes = "User Session ID")
    private String sessionID;

	@ApiModelProperty(notes = "Cart Details on Individual Products")
	@ElementCollection
	@LazyCollection(LazyCollectionOption.FALSE)
	@Cascade(CascadeType.ALL)
	@Embedded
	private List<CartDetails> cartDetails;

	@ApiModelProperty(notes = "Cart Vendor ID")
	@Column(name = "vendor_id")
	private Integer vendorId;
}
