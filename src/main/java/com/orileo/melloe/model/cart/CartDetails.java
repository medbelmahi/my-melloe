package com.orileo.melloe.model.cart;

import com.orileo.melloe.model.product.Product;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Setter
@Getter
@Embeddable
public class CartDetails  implements Serializable {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(notes = "Cart's product")
	@NotNull
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "productID")
	private Product product;

	@ApiModelProperty(notes = "Quantity of the Product")
	@NotNull
	private int quantity;

	@ApiModelProperty(notes = "Actual Unit Price of the Product")
	@NotNull
	private Double pricePerUnit;

	@ApiModelProperty(notes = "Price of the Packaging in the Order")
	@JoinColumn(name = "packaging_pricce")
	private Double packagingPrice;
	
	@ApiModelProperty(notes = "Total Price of the Product in the Order")
	@NotNull
	private Double totalPrice;
}
