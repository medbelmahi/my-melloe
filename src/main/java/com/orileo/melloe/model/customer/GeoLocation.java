package com.orileo.melloe.model.customer;

import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by Mohamed BELMAHI on 01/09/2017.
 */
@Setter
@Getter
@EqualsAndHashCode
@ToString
@Entity
@Table(name="geo_location")
public class GeoLocation implements Serializable {
    @ApiModelProperty(hidden = true)
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @ApiModelProperty(notes = "latitude", required = true)
    @NotNull
    @Column(name = "lat", nullable = false)
    private String lat;
    @ApiModelProperty(notes = "longitude", required = true)
    @NotNull
    @Column(name = "lng", nullable = false)
    private String lng;
}