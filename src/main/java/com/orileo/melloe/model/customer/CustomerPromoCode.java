package com.orileo.melloe.model.customer;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.orileo.melloe.model.promocode.PromoCode;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
@EqualsAndHashCode
@ToString
@Entity
@Table(name="customer_promo_code")
public class CustomerPromoCode implements Serializable{
	private static final long serialVersionUID = -2359074967042959886L;

    @ApiModelProperty(hidden = true)
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ApiModelProperty(notes = "Promocode details", required = true, hidden = true)
    @ManyToOne
	@JoinColumn(name = "promo_code_id")
	private PromoCode promoCode;

	@ApiModelProperty(notes = "Start date of the promocode for particular customer")
	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern="yyyy-MM-dd")
	@Column(name = "from_date", nullable = false)
	private Date fromDate;

	@ApiModelProperty(notes = "End date of the promocode for particular customer")
	@NotNull
	@JsonFormat(pattern="yyyy-MM-dd")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "to_date", nullable = false)
	private Date toDate;

	@ApiModelProperty(notes = "Is promocode being used")
	@NotNull
	@Column(name="is_used")
	private boolean isUsed;

	@JsonBackReference
	@ApiModelProperty(hidden = true)
	@ManyToOne
	@JoinColumn(name = "customer_id")
	private Customer customer;
	
	@ApiModelProperty(notes = "promocode used date")
	@NotNull
	@JsonFormat(pattern="yyyy-MM-dd")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "promo_code_used_on", nullable = false)
	private Date promoCodeUsedOn;
	
	@ApiModelProperty(notes = "promo code used desktop device")
	@Size(max = 40)
	@Column(name="ip_address",length = 40)	
	private String ipAddress;

	@ApiModelProperty(notes = "promo code used mobile device")
	@Size(max = 40)
	@Column(name="app_id",length = 40)
	private String appId;

}