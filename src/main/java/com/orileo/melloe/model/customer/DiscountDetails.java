package com.orileo.melloe.model.customer;

import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;


@Setter
@Getter
@EqualsAndHashCode
@ToString
@Embeddable
public class DiscountDetails {

    @ApiModelProperty(notes = "The discount amount", required = true)
    @NotNull
    @Column(name = "dis_amount")
    private Double disAmount;

    @ApiModelProperty(notes = "The type of the discount", required = true)
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "discount_type", nullable = false)
    private DiscountType discountType;
}
