package com.orileo.melloe.model.customer;

import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Setter
@Getter
@EqualsAndHashCode
@ToString
@Entity
@Table(name="customer_level")
public class CustomerLevel implements Serializable {
	private static final long serialVersionUID = 5362459756265133231L;

	@ApiModelProperty(hidden = true)
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@ApiModelProperty(notes = "The name of the Customer Level", required = true)
	@NotNull
	@Column(name = "name", nullable = false)
	private String name;

	@ApiModelProperty(notes = "Details of the discount for this Customer Level", required = true)
	@NotNull
	@Embedded
	private DiscountDetails discountDetails;

	@ApiModelProperty(notes = "The maximum limit for points for this Customer Level")
	@Column(name="max_limit")
	private Double maxLimit;

	@ApiModelProperty(notes = "The minimum limit of points for this Customer Level", required = true)
	@NotNull
	@Column(name = "min_limit", nullable = false)
	private Double minLimit;


}
