package com.orileo.melloe.model.customer;

import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.io.Serializable;

@Setter
@Getter
@EqualsAndHashCode
@ToString
@Embeddable
public class CustomerAddress implements Serializable {
	private static final long serialVersionUID = 5362459756265133231L;

	@ApiModelProperty(notes = "The address name of the Customer")
	@NotNull
	@Column(name = "address_name", nullable = false, length = 50)
	private String addressName;

	@ApiModelProperty(notes = "Address Line")
	@NotNull
	@Column(name = "address_line", nullable = false, length = 250)
	private String addressLine;

	@ApiModelProperty(notes = "Address Line 2")
	@Column(name = "address_line_2", length = 250)
	private String addressLine_2;

	@ApiModelProperty(notes = "City")
	@NotNull
	@Column(name = "city", nullable = false, length = 50)
	private String city;

	@ApiModelProperty(notes = "State")
	@NotNull
	@Column(name = "state", nullable = false, length = 70)
	private String state;

	@ApiModelProperty(notes = "Pincode")
	@NotNull
	@Column(name = "pincode", nullable = false, length = 25)
	private String pincode;

	@ApiModelProperty(notes = "The nearest landmark")
	@NotNull
	@Column(name = "nearestlandmark")
	private String nearestLandmark;
	
	@ApiModelProperty(notes = "Latitude Coordinate")
	@Size(max = 50)
	@Column(name = "latitude", length = 50)
	private String latitude;

	@ApiModelProperty(notes = "Latitude Coordinate")
	@Size(max = 50)
	@Column(name = "longitude", length = 50)
	private String longitude;
}
