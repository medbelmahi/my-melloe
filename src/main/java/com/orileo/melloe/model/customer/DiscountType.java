package com.orileo.melloe.model.customer;

public enum DiscountType {
    PERCENT, AMOUNT
}
