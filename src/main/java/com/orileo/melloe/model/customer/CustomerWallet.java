package com.orileo.melloe.model.customer;

import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Setter
@Getter
@EqualsAndHashCode
@ToString
@Embeddable
public class CustomerWallet implements Serializable{
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(notes = "The number of points earned by Customer")
	@Column(name = "points_earned", nullable = false)
	private int pointsEarned;


	@ApiModelProperty(notes = "The Level of the Customer", required = true)
	@ManyToOne
	private CustomerLevel level;
}
