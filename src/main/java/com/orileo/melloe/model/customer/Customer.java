package com.orileo.melloe.model.customer;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.orileo.melloe.model.cart.Cart;
import com.orileo.melloe.model.login.MasterLogin;
import com.orileo.melloe.model.notifications.Notification;
import com.orileo.melloe.model.order.Order;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Setter
@Getter
@EqualsAndHashCode
@ToString
@Entity
@Table(name="customer")
public class Customer implements Serializable {
	private static final long serialVersionUID = -3331112524416820980L;

    @ApiModelProperty(hidden = true)
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

	@ApiModelProperty(notes = "Customer first Name", required = true)
	@NotNull
	@Column(name = "first_name", nullable = false, length = 50)
	private String firstName;

	@ApiModelProperty(notes = " Customer last name", required = true)
	@NotNull
	@Column(name = "last_name", nullable = false, length = 50)
	private String lastName;

	@ApiModelProperty(notes = "Customer phone number")
	@NotNull
	@Column(name = "phone_number", nullable = false, length = 30)
	private String phoneNumber;

	@ApiModelProperty(notes = "Customer Login")
	@NotNull
	@OneToOne
	private MasterLogin userId;

	@ApiModelProperty(notes = "The addresses of the Customer")
	@NotNull
	@ElementCollection
	@LazyCollection(LazyCollectionOption.FALSE)
	@Embedded
	private List<CustomerAddress> address;

	@ApiModelProperty(notes = "Customer Wallet")
	@NotNull
	@Embedded
	private CustomerWallet customerWallet;

	@JsonManagedReference
	@ApiModelProperty(notes = "Customer Promo Codes", hidden = true)
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(cascade = CascadeType.REMOVE, orphanRemoval = true, mappedBy = "customer")
	private List<CustomerPromoCode> customerPromoCodes;

	@JsonManagedReference
	@ApiModelProperty(notes = "Customer Reminders", hidden = true)
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(cascade = CascadeType.REMOVE, orphanRemoval = true, mappedBy = "customer")
	private List<Reminder> reminders;

	@JsonManagedReference
	@ApiModelProperty(notes = "Customer Orders", hidden = true)
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(cascade = CascadeType.REMOVE, orphanRemoval = true, mappedBy = "customer")
	private List<Order> orders;

	@JsonManagedReference
	@ApiModelProperty(notes = "Customer current Cart", hidden = true)
	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "cart_id")
	private Cart cart;

	@JsonManagedReference
	@ApiModelProperty(notes = "Customer Notifications", hidden = true)
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(cascade = CascadeType.REMOVE, orphanRemoval = true, mappedBy = "customer")
	private List<Notification> notifications;
}
