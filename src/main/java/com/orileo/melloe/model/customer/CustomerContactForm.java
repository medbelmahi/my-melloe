package com.orileo.melloe.model.customer;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class,property = "@id")
@Setter
@Getter
@EqualsAndHashCode
@ToString
@Entity(name="customerContactForm")
@Table(name="customer_contact_form")
public class CustomerContactForm implements Serializable 
{

	private static final long serialVersionUID = 6306545417486362571L;

	@ApiModelProperty(hidden = true)
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ApiModelProperty(notes = "Name of the contacted person")
    @NotNull
    @Size(max = 100)
    @Column(name = "name", nullable = false, length = 100)
    private String name;
    
    @ApiModelProperty(notes = "Email of the contacted person")
    @NotNull
    @Size(max = 100)
    @Column(name = "email_id", unique = false, nullable = false, length = 100)
    private String emailId;
	
	@ApiModelProperty(notes = "customer contacted Date and Time")
	@JsonFormat(pattern="yyyy-MM-dd")
	@Column(name = "contacted_date")
	private Date contactedDate;

	
	@ApiModelProperty(notes = "description")
	@NotNull
	@Size(max = 500)
	@Column(name = "description", length = 500)
	private String description;
	
	
	@ApiModelProperty(notes = "refName")
	@Size(max = 15)
	@Column(name = "ref_name", length = 15)
	private String refName;

}