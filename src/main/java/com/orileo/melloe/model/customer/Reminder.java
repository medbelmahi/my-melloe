package com.orileo.melloe.model.customer;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class,
		property = "@id")
@Setter
@Getter
@EqualsAndHashCode
@ToString
@Entity
@Table(name="reminder")
public class Reminder implements Serializable {
	private static final long serialVersionUID = -5070114631784653273L;

    @ApiModelProperty(hidden = true)
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

	@JsonBackReference
	@ApiModelProperty(hidden = true)
	@ManyToOne
	@JoinColumn(name = "customer_id")
	private Customer customer;

	@ApiModelProperty(notes = "Reminder Date and Time")
	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "reminder_date", nullable = false)
	private Date reminderDate;

	@ApiModelProperty(notes = "Reminder Text")
	@NotNull
	@Size(max = 500)
	@Column(name = "reminder_text", length = 500, nullable = false)
	private String reminderText;

}
