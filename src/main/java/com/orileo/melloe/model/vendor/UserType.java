package com.orileo.melloe.model.vendor;


public enum UserType {
    VENDOR, ADMIN, CUSTOMER
}
