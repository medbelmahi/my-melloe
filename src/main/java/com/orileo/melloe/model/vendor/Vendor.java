package com.orileo.melloe.model.vendor;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.orileo.melloe.model.login.MasterLogin;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

//@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
@Setter
@Getter
@EqualsAndHashCode
@ToString
@Entity
@Table(name="vendor")
public class Vendor implements Serializable{
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(hidden = true)
    @Id
    @Column(name = "vendor_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @ApiModelProperty(notes = "The name of the vendor", required = true)
    @NotNull
    @Size(max = 50)
    @Column(name = "name", nullable = false, length = 50)
    private String name;

    @ApiModelProperty(notes = "List of cell phone numbers of the vendor", required = true)
    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    @Embedded
    private List<CellNumber> cellNumbers;

    @ApiModelProperty(notes = "The address of the vendor", required = true)
    @NotNull
    @Embedded
    private Address address;

    @ApiModelProperty(notes = "List of vendor's logins. Vendor can have up to 4 logins", required = true)
    @NotNull
    @OneToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinColumn(name = "vendor_id")
    private List<MasterLogin> userId;
}
