package com.orileo.melloe.model.vendor;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@Embeddable
public class Address implements Serializable {

	private static final long serialVersionUID = -7818312086279161319L;

	@ApiModelProperty(notes = "Line 1 of the Address")
	@NotNull
	@Size(max = 250)
	@Column(name = "line1", nullable = false, length = 250)
	private String line1;

	@ApiModelProperty(notes = "Line 2 of the Address")
	@Size(max = 250)
	@Column(name = "line2", length = 250)
	private String line2;

	@ApiModelProperty(notes = "Line 3 of the Address")
	@NotNull
	@Size(max = 250)
	@Column(name = "line3", length = 250)
	private String line3;

	@ApiModelProperty(notes = "City")
	@NotNull
	@Size(max = 50)
	@Column(name = "city", nullable = false, length = 50)
	private String city;

	@ApiModelProperty(notes = "State")
	@NotNull
	@Size(max = 70)
	@Column(name = "state", nullable = false, length = 70)
	private String state;

	@ApiModelProperty(notes = "Country")
	@NotNull
	@Size(max = 50)
	@Column(name = "country", nullable = false, length = 50)
	private String country;

	@ApiModelProperty(notes = "Zipcode")
	@NotNull
	@Size(max = 35)
	@Column(name = "zipcode", nullable = false, length = 35)
	private String zipcode;

	@ApiModelProperty(notes = "Landmark nearby")
	@Size(max = 100)
	@Column(name = "land_mark", length = 100)
	private String landMark;

	@ApiModelProperty(notes = "Landline")
	@Size(max = 100)
	@Column(name = "land_line", length = 100)
	private String landLine;

	@ApiModelProperty(notes = "Latitude Coordinate")
	@Size(max = 20)
	@Column(name = "latitude", length = 20)
	private String latitude;

	@ApiModelProperty(notes = "Latitude Coordinate")
	@Size(max = 20)
	@Column(name = "longitude", length = 20)
	private String longitude;

	/**
	 * Method to return address of vendor for order tracking.
	 * @return
	 */
	public String geTrackingAddress() {
		return String.join(",", line1, line2, line3, landMark, city, state);
	}
}
