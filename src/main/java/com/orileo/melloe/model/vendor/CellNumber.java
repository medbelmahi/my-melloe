package com.orileo.melloe.model.vendor;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class CellNumber {

    @ApiModelProperty(notes = "The cell number", required = true)
    @NotNull
    @Size(max = 30)
    @Column(nullable = false, length = 30)
    private String phoneNumber;

    @ApiModelProperty(notes = "The name of the contact person", required = true)
    @NotNull
    @Size(max = 50)
    @Column(nullable = false, length = 50)
    private String contactPersonName;
}
