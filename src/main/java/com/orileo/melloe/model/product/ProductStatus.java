package com.orileo.melloe.model.product;

public enum ProductStatus {
    AVAILABLE, OUT_OF_STOCK
}
