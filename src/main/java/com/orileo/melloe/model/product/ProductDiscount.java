package com.orileo.melloe.model.product;

import com.fasterxml.jackson.annotation.JsonBackReference;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
@EqualsAndHashCode
@ToString
@Entity
@Table(name="product_discount")
public class ProductDiscount implements Serializable{
	private static final long serialVersionUID = 1L;

    @ApiModelProperty(hidden = true)
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

	@ApiModelProperty(notes = "Promo Code of the Discount", required = true)
	@NotNull
	@Size(max = 30)
	@Column(name = "promo_code", nullable = false, length = 30)
	private String promoCode;

	@ApiModelProperty(notes = "Start Date of the Discount", required = true)
	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "from_date", nullable = false)
	private Date fromDate;

	@ApiModelProperty(notes = "End Date of the Discount", required = true)
	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "to_date", nullable = false)
	private Date toDate;

	@ApiModelProperty(notes = "The Amount of the Discount", required = true)
	@NotNull
	@Column(name = "dis_amount", nullable = false)
	private double disAmount;

	@ApiModelProperty(notes = "Minimum Amount of Product for the Discount")
	@Column(name = "min_amount")
	private double minAmount;

	@ApiModelProperty(notes = "Maximum Amount of Product for the Discount")
	@Column(name = "max_amount")
	private double maxAmount;

	@ApiModelProperty(notes = "Is Discount is fixed")
	@Column(name = "is_fixed")
	private Boolean isFixed;

	@JsonBackReference
	@ApiModelProperty(hidden = true)
	@ManyToOne
	@JoinColumn(name = "product_id", nullable = false)
	private Product product;
}
