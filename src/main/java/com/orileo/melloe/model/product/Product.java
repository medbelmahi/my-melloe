package com.orileo.melloe.model.product;

import com.fasterxml.jackson.annotation.*;
import com.orileo.melloe.model.category.Category;
import com.orileo.melloe.model.category.SubCategory;
import com.orileo.melloe.model.vendor.Vendor;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Setter
@Getter
@EqualsAndHashCode(doNotUseGetters = true)
@Entity
@Table(name="product")
public class Product implements Serializable {
	private static final long serialVersionUID = 1L;

    @ApiModelProperty(hidden = true)
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

	@ApiModelProperty(notes = "Name of the product", required = true)
	@NotNull
	@Size(max = 50)
	@Column(name = "name", nullable = false, length = 50)
	private String name;

	@ApiModelProperty(notes = "Product's description", required = true)
	@NotNull
	@Size(max = 1000)
	@Column(name = "description", nullable = false, length = 1000)
	private String description;

	
	@ApiModelProperty(notes = "Product's preparaion time")
	@Size(max = 200)
	@Column(name = "preparaion_time", length = 200)
	private String preparaionTime;
	
	@ApiModelProperty(notes = "Product's delivery time")
	@Size(max = 50)
	@Column(name = "delivery_time", length = 50)
	private String deliveryTime;
	
	
	@ApiModelProperty(notes = "The URL to the product's picture")
	@Column(name="picture")
	private String picture;

	@ApiModelProperty(notes = "Flag that shows if product is discounted", required = true)
	@NotNull
	@Column(name = "is_discounted", nullable = false)
	private boolean isDiscounted;

	@ApiModelProperty(notes = "Flag that shows if product is a vegetarian", required = true)
	@NotNull
	@Column(name = "is_vegetarian", nullable = false)
	private boolean isVegetarian;


	@ApiModelProperty(notes = "Count of product available", required = true)
	@NotNull
	@Column(name = "product_count", nullable = false)
	private int productCount;

	@ApiModelProperty(notes = "The availability status of the product", required = true)
	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "product_status", nullable = false)
	private ProductStatus productStatus;

	@ApiModelProperty(notes = "Product's categories", required = true)
	@ManyToMany
	@JoinColumn(name = "category_id")
	private List<Category> category;

	@ApiModelProperty(notes = "Product's subcategories", required = true)
	@ManyToMany
	@JoinColumn(name = "subcategory_id")
	private Set<SubCategory> subcategory;

	@ApiModelProperty(notes = "Product's vendor", required = true)
	@ManyToOne
	@JoinColumn(name = "vendor_id")
	private Vendor vendor;

	@ApiModelProperty(notes = "Product's price")
	@Embedded
	private ProductPrice productPrice;

	@JsonManagedReference
	@ApiModelProperty(notes = "Product's Discounts")
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(cascade = CascadeType.REMOVE, orphanRemoval = true, mappedBy = "product")
	private List<ProductDiscount> productDiscounts;

	@JsonManagedReference
	@ApiModelProperty(notes = "Product's Reviews")
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(cascade = CascadeType.REMOVE, orphanRemoval = true, mappedBy = "product")
	private List<ProductReview> productReviews;
}