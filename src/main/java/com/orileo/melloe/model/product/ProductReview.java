package com.orileo.melloe.model.product;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.orileo.melloe.model.customer.Customer;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
@EqualsAndHashCode
@ToString
@Entity
@Table(name = "product_review")
public class ProductReview implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(hidden = true)
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @ApiModelProperty("The text of the product's review")
    @NotNull
    @Size(max = 250)
    @Column(name = "reviewText", nullable = false, length = 250)
    private String reviewText;

    @ApiModelProperty("The rating of the product")
    @NotNull
    @Column(name = "rating", nullable = false)
    private int rating;

    @ApiModelProperty("The date of the review")
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "review_date")
    private Date reviewDate;

    @ApiModelProperty(value = "The reviewer of the product", hidden = true)
    @ManyToOne
    @JoinColumn(name = "customer_id", nullable = false)
    private Customer customer;

    @JsonBackReference
    @ApiModelProperty(hidden = true)
    @ManyToOne
    @JoinColumn(name = "product_id", nullable = false)
    private Product product;

}
