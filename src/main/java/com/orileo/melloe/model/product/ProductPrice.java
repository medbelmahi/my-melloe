package com.orileo.melloe.model.product;

import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Setter
@Getter
@EqualsAndHashCode
@ToString
@Embeddable
public class ProductPrice implements Serializable{
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(notes = "Unit of Measurement for Product", required = true, example = "kg")
	@NotNull
	@Size(max = 10)
	@Column(name = "unit_measurment", nullable = false, length = 10)
	private String unitMeasurment;

	@ApiModelProperty(notes = "The Price of the Product", required = true)
	@NotNull
	@Column(name = "price", nullable = false)
	private double price;

	@ApiModelProperty(notes = "Currency of the Product's Price", required = true, example = "GBP")
	@NotNull
	@Size(max = 10)
	@Column(name = "currency", nullable = false, length = 10)
	private String currency;

	@ApiModelProperty(notes = "Is Product's Price Default", required = true)
	@NotNull
	@Column(name = "is_default", nullable = false)
	private boolean isDefault;
	
	@ApiModelProperty(notes = "Is Packaging Charges Applicable")
	@NotNull
	@Column(name = "is_packaging_charges_applicable")
	private boolean isPackagingChargesApplicable;
	
	@ApiModelProperty(notes = "Packaging Charges Of Product")
	@NotNull
	@Column(name = "packaging_charges")
	private double packagingCharges;
}