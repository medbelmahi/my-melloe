package com.orileo.melloe.model.forgotpassword;

import java.util.Date;

import javax.persistence.*;

import com.orileo.melloe.model.login.MasterLogin;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@EqualsAndHashCode
@ToString
@Entity
public class PasswordResetToken {
  
    private static final int EXPIRATION = 60 * 24;
  
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
  
    private String token;
  
    @OneToOne(targetEntity = MasterLogin.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "user_id")
    private MasterLogin user;
  
    
    private java.util.Date expiryDate;
    
    @PrePersist
    protected void onCreate() {
    	expiryDate = new Date();
    }
}
