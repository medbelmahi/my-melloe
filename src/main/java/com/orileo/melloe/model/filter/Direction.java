package com.orileo.melloe.model.filter;

public enum Direction {
    ASC, DESC
}
