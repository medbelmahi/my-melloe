package com.orileo.melloe.model.filter;

import com.orileo.melloe.model.customer.Customer;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@Entity
@Table(name = "product_filters")
public class ProductFilter {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;
    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<String> categoryNames;
    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<String> subCategoryNames;
    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Integer> vendorIDs;
    private Boolean isDiscounted;
    private Boolean isVegetarian;
    private Double minPrice;
    private Double maxPrice;

    @ApiModelProperty(notes = "Customer who created filter")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_id")
    private Customer customer;

    @Embedded
    @ElementCollection
    private List<FilterOrder> filterOrder;
}
