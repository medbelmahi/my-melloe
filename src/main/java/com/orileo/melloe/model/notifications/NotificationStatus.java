package com.orileo.melloe.model.notifications;

public enum NotificationStatus {
    PENDING, SENT
}
