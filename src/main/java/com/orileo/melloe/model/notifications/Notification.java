package com.orileo.melloe.model.notifications;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.orileo.melloe.model.customer.Customer;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;


@Setter
@Getter
@EqualsAndHashCode
@ToString
@Entity
@Table(name = "notifications")
public class Notification {

    @ApiModelProperty(hidden = true)
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @JsonBackReference
    @ApiModelProperty(hidden = true)
    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;


    @ApiModelProperty(notes = "The type of the Notification", required = true)
    @ManyToOne
    private NotificationType notificationType;

    @ApiModelProperty(notes = "Notification message", required = true)
    @NotNull
    @Size(max = 1000)
    @Column(name = "message", length = 1000, nullable = false)
    private String message;

    @ApiModelProperty(notes = "Notification Token", required = true)
    @NotNull
    @Size(max = 100)
    @Column(name = "token", length = 100, nullable = false)
    private String token;

    @ApiModelProperty(notes = "Notification Status", required = true)
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private NotificationStatus status;

    @ApiModelProperty(notes = "The time and date when the Notification was triggered", required = true)
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "time_stamp", nullable = false)
    private Date timeStamp;
}
