package com.orileo.melloe.model.notifications;

import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Setter
@Getter
@EqualsAndHashCode
@ToString
@Entity
@Table(name="notification_medium")
public class NotificationMedium implements Serializable{
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(hidden = true)
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

    @ApiModelProperty(notes = "Notification type", hidden = true)
	@ManyToOne
	private NotificationType notificationType;

	@ApiModelProperty(notes = "The medium of the notification")
	@NotNull
	@Size(max = 50)
	@Column(name = "medium", nullable = false, length = 50)
	private String medium;

	@ApiModelProperty(notes = "The text of the notification")
	@NotNull
	@Size(max = 1000)
	@Column(name = "notification_text", length = 1000, nullable = false)
	private String notificationText;

}
