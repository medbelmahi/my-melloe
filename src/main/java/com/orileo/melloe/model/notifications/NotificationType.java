package com.orileo.melloe.model.notifications;

import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Setter
@Getter
@EqualsAndHashCode
@ToString
@Entity
@Table(name="notification_type")
public class NotificationType {

    @ApiModelProperty(hidden = true)
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

	@ApiModelProperty(notes = "The name of the notification type")
	@NotNull
	@Size(max = 100)
    @Column(name = "notification_type", length = 100, unique = true)
    private String notificationType;
}
