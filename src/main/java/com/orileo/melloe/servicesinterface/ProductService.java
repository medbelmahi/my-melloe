package com.orileo.melloe.servicesinterface;

import com.orileo.melloe.model.filter.ProductFilter;
import com.orileo.melloe.model.product.Product;

import java.util.List;
import java.util.Optional;

public interface ProductService
{

	List<Product> listProducts(List<Integer> categoryID, List<Integer> subcategoryID, List<Integer> vendorID);
	
	List<Product> getProductsByVendorId(Integer vendorID);
		
	Optional<Product> getProduct(Integer id);
	
	Optional<Product> findProduct(Integer id, Integer vendorId);
	
	Product saveProduct(Product product);
	
	void deleteProduct(Integer id);
	
	boolean isProductExists(Integer id);
	
	List<Product> listProducts(ProductFilter productFilter);
		
	List<Product> vendorsProduct(List<Integer> vendorID);

    List<Product> listProductsByVendorsAndCat(Integer categoryID, List<Integer> vendorID);
}