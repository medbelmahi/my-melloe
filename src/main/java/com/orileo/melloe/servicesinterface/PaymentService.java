package com.orileo.melloe.servicesinterface;

import com.orileo.melloe.controllers.dto.PaymentGatewayData;
import com.orileo.melloe.model.payment.PaymentTransaction;
import com.razorpay.Payment;
import com.razorpay.RazorpayException;

import java.util.List;

/**
 * Created by Mohamed BELMAHI on 19/08/2017.
 */
public interface PaymentService {
    PaymentGatewayData initPaymentObject();
    Payment getPaymentInfo(String paymentId) throws RazorpayException;
    void makePaymentCapture(String id, Integer amount);
    void savePaymentTransaction(PaymentTransaction paymentTransaction);
    List<PaymentTransaction> findAllByNotCaptured();
}
