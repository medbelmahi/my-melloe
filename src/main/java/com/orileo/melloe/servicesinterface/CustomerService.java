package com.orileo.melloe.servicesinterface;

import com.orileo.melloe.model.customer.Customer;
import com.orileo.melloe.model.customer.CustomerContactForm;

import java.util.List;
import java.util.Optional;

import javax.validation.constraints.NotNull;


public interface CustomerService
{

	List<Customer> listAllCustomers();
	
	Optional<Customer> getCustomer(Integer id);
	
	Customer saveCustomer(Customer customer);
	
	void deleteCustomer(Integer id);
	
	boolean isCustomerExists(Integer id);
	
	Optional<Customer> findCustomerByEmail(String username);
	
	List<CustomerContactForm> listAllCustomerContactForm();
	
	CustomerContactForm saveCustomerContactForm(CustomerContactForm customerContactForm);

}