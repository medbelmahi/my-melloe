package com.orileo.melloe.servicesinterface;

import com.orileo.melloe.model.promocode.PromoCode;
import java.util.List;
import java.util.Optional;

public interface PromocodeService {

	List<PromoCode> listAllPromoCodes();

	Optional<PromoCode> getPromoCode(String promocode);

	PromoCode savePromoCode(PromoCode promocode);

	void deletePromoCode(String promocode);

	boolean isPromocodeExists(String id);
}