package com.orileo.melloe.servicesinterface;

import com.orileo.melloe.model.customer.CustomerPromoCode;

import java.util.List;
import java.util.Optional;


public interface CustomerPromoCodeService
{
	List<CustomerPromoCode> listCustomerPromocodes(List<Integer> customerID, List<String> promocode);
	
	Optional<CustomerPromoCode> getCustomerPromocode(Integer id);
	
	CustomerPromoCode saveCustomerPromocode(CustomerPromoCode customerPromocode);
	
	void deleteCustomerPromocode(Integer customerPromoCode);
	
	boolean isCustomerPromocodeExists(Integer id);
	
	List<CustomerPromoCode> getCustomerPromoCodeStatus(Integer customerId, String promocode, String ipAddress, String appId);
}