package com.orileo.melloe.servicesinterface;

import com.google.maps.model.LatLng;
import com.orileo.melloe.model.customer.CustomerAddress;
import com.orileo.melloe.model.product.Product;

import java.util.List;

/**
 * Created by Mohamed BELMAHI on 01/09/2017.
 */
public interface GoogleMapService {
    boolean isUnderRadius(LatLng latLngOrigin);

    List<Product> getProductOfVendorsUnderRadius(double lat, double lng);

    List<Product> getProductOfCategoryAndVendorsUnderRadius(double lat, double lng, Integer catId);

    CustomerAddress getNewAddressFrom(LatLng latLngOrigin);

    boolean isUnderRadius(LatLng cartLatLong, LatLng vendorLatLng);
}