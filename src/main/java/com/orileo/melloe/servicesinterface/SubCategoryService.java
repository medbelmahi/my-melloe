package com.orileo.melloe.servicesinterface;

import com.orileo.melloe.model.category.SubCategory;

import java.util.List;
import java.util.Optional;


public interface SubCategoryService {

	List<SubCategory> listAllSubCategories(List<Integer> categoryIDs);

  List<SubCategory> listAllSubcategoriesForCategory(Integer categoryId);

  Optional<SubCategory> getSubCategory(Integer id);

	Optional<SubCategory> findByName(String name);

	SubCategory saveSubCategory(SubCategory subCategory);

  void deleteSubCategory(Integer id);

  boolean isSubcategoryExists(Integer id);
}
