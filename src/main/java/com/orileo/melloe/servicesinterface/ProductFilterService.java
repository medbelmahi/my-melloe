package com.orileo.melloe.servicesinterface;

import com.orileo.melloe.model.filter.ProductFilter;

import java.util.List;
import java.util.Optional;

public interface ProductFilterService {

    List<ProductFilter> listAllProductFiltersForCustomer(Integer customerID);

  Optional<ProductFilter> getProductFilter(Integer id, Integer customerID);

    Optional<ProductFilter> findByName(String name);

    ProductFilter saveProductFilter(ProductFilter productFilter);

  void deleteProductFilter(Integer id);

}
