package com.orileo.melloe.servicesinterface;

import com.orileo.melloe.model.order.Order;
import com.orileo.melloe.model.order.OrderNumber;

import java.util.List;
import java.util.Optional;

public interface OrdersService
{
	List<Order> listAllOrders(Integer vendorID, Integer customerID);

	Optional<Order> getOrder(Integer id, Integer customerId, Integer vendorId);
	
	Optional<Order> getOrderByIdAndCustomerId(Integer id, Integer customerID);

	Order saveOrder(Order order);

	void deleteOrder(Integer id);

	boolean isOrderExist(Integer id, Integer customerId, Integer vendorId);
	
	List<Order> getOrdersByStatus(int vendorId);

	void calculateOrder(Order order);
	
	void saveOrderNumber(OrderNumber order);
	
	Optional<OrderNumber> getOrderNumber(Integer id);
}
