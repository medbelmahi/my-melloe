package com.orileo.melloe.servicesinterface;


import com.orileo.melloe.model.payment.Tax;

import java.util.List;
import java.util.Optional;

public interface TaxesService {

    List<Tax> listAllTaxes();

    Optional<Tax> getTax(Integer id);

    Tax save(Tax tax);

    void deleteTax(Integer id);

    boolean isTaxExist(Integer id);
}
