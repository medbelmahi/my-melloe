package com.orileo.melloe.servicesinterface;

import java.util.Map;

public interface NotificationSenderService {

	public void sendNotification(String emailTemplateName, Map<String, String> variableMap, String subject, String emailIdTo, String numbers, String smsMessage);

	public boolean sendSMS(String message, String phoneNumber);
}