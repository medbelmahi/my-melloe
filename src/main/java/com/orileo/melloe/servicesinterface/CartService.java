package com.orileo.melloe.servicesinterface;

import com.orileo.melloe.controllers.dto.AddToCartInput;
import com.orileo.melloe.controllers.webmodel.OrderResource;
import com.orileo.melloe.model.exceptions.CheckPaidAmountException;
import com.orileo.melloe.model.exceptions.ProductIsNotFromSameVendorException;
import com.orileo.melloe.model.cart.Cart;

import java.util.Optional;

public interface CartService {
//	List<Cart> listAllCarts(Integer vendorID, Integer customerID);

	Optional<Cart> getCart(Integer id, Integer customerId);

	Cart getCart(Integer id);

	Cart saveCart(Cart cart);

	void deleteCart(Integer id);

	void deleteCart(Cart cart, Integer customerId);

	boolean isCartExist(Integer id);

	void calculateCart(Cart cart);

	Optional<Cart> getCartBySession(String sessionId);

    void addToCart(Cart cart, Integer productId, Integer vendorId) throws ProductIsNotFromSameVendorException;

	void remove(Cart cart, Integer productId);

	void updateQty(Cart cart, Integer productId, Integer qty);

	void addToCart(Cart cart, AddToCartInput productInput);

    void checkPaidAmount(Cart cart, OrderResource orderResource) throws CheckPaidAmountException;
    
    void applyPromocode(Cart cart);
}
