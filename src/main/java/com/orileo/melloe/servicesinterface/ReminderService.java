package com.orileo.melloe.servicesinterface;

import com.orileo.melloe.model.customer.Reminder;

import java.util.List;
import java.util.Optional;

public interface ReminderService {

    List<Reminder> listAllReminders();

    Optional<Reminder> getReminder(Integer id);

	Reminder saveReminder(Reminder remainder);

  void deleteReminder(Integer id);

  boolean isReminderExists(Integer id);
}
