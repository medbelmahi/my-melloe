package com.orileo.melloe.servicesinterface;

import org.springframework.web.multipart.MultipartFile;

public interface ImageService {
	boolean uploadFile(MultipartFile file, String id);

	String generateLink(String id);

	void deleteFile(String id);
}
