package com.orileo.melloe.servicesinterface;

import com.orileo.melloe.model.notifications.NotificationType;

import java.util.List;
import java.util.Optional;

public interface NotificationTypeService {

	List<NotificationType> listAllNotificationTypes();

	Optional<NotificationType> getNotificationType(Integer id);

	 NotificationType saveNotificationType(NotificationType notificationType);

  void deleteNotificationType(Integer id);

	boolean isNotificationTypeExist(Integer id);
	
}
