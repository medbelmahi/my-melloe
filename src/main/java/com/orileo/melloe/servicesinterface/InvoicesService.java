package com.orileo.melloe.servicesinterface;

import com.orileo.melloe.model.order.Order;
import com.orileo.melloe.model.payment.Invoice;

import java.util.List;
import java.util.Optional;

public interface InvoicesService {

    List<Invoice> listAllInvoices();

    Optional<Invoice> getInvoice(Integer id);

    Invoice saveInvoice(Invoice invoice);

  void deleteInvoice(Integer id);

    List<Invoice> listAllInvoicesForOrder(Integer orderID);

  List<Invoice> listAllInvoicesForCustomer(Integer customerID);

    void generateInvoice(Order order);
}
