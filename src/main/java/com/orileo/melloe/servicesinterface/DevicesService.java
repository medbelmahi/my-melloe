package com.orileo.melloe.servicesinterface;

import com.orileo.melloe.model.notifications.Device;


public interface DevicesService {

    Iterable<Device> listAllDevices();

    Device getById(Integer id);

    Device saveDevices(Device device);

    void updateDevices(Device device);

  void deleteDevicesById(Integer id);

    boolean isDevicesExist(Device device);

}
