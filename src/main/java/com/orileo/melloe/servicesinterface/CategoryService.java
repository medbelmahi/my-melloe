package com.orileo.melloe.servicesinterface;

import com.orileo.melloe.model.category.Category;

import java.util.List;
import java.util.Optional;

public interface CategoryService {

    List<Category> listAllCategories();

  Optional<Category> getCategory(Integer id);

    Optional<Category> findByName(String category);


    Category saveCategory(Category category);

  void deleteCategory(Integer id);

  boolean isCategoryExists(Integer id);
}
