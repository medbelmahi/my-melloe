package com.orileo.melloe.servicesinterface;

import com.orileo.melloe.model.product.ProductDiscount;

import java.util.List;
import java.util.Optional;

public interface ProductDiscountService {

	List<ProductDiscount> listAllProductDiscounts();

	Optional<ProductDiscount> getProductDiscount(Integer id);

	ProductDiscount saveProductDiscount(ProductDiscount product);

  void deleteProductDiscount(Integer id);

  boolean isProductDiscountExists(Integer id);

  Optional<ProductDiscount> findProductDiscount(Integer id, Integer vendorId);

}
