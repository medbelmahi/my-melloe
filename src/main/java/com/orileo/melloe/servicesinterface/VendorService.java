package com.orileo.melloe.servicesinterface;

import com.orileo.melloe.model.vendor.Vendor;

import java.util.List;
import java.util.Optional;

public interface VendorService {

	List<Vendor> listAllVendors();

    Optional<Vendor> findVendorByEmail(String username);

  Optional<Vendor> getVendor(Integer id);

	Vendor saveVendor(Vendor vendor);

  void deleteVendor(Integer id);

}
