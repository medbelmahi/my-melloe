package com.orileo.melloe.servicesinterface;

import com.orileo.melloe.model.cart.Cart;
import javax.servlet.http.HttpSession;

/**
 * Created by Mohamed BELMAHI on 22/07/2017.
 */
public interface SessionService {
    public static final String CURRENT_CART = "currentCart";
    Cart getSessionCart(HttpSession session);
    void setSessionCart(HttpSession session, Cart cart);
}