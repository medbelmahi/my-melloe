package com.orileo.melloe.servicesinterface;

import com.orileo.melloe.model.customer.CustomerLevel;

import java.util.List;
import java.util.Optional;


public interface CustomerLevelService {

	List<CustomerLevel> listAllCustomerLevels();

  Optional<CustomerLevel> getCustomerLevel(Integer id);

	CustomerLevel saveCustomerLevel(CustomerLevel level);

  void deleteCustomerLevel(Integer id);

  boolean isCustomerLevelExist(Integer id);

}
