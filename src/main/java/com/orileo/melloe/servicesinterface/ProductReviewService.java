package com.orileo.melloe.servicesinterface;

import com.orileo.melloe.model.product.ProductReview;

import java.util.List;
import java.util.Optional;

public interface ProductReviewService {

	List<ProductReview> listAllProductReviews(List<Integer> customers,
											  List<Integer> products);

	Optional<ProductReview> getProductReview(Integer id);

    Optional<ProductReview> getProductReview(Integer id, Integer customerID);

	 ProductReview saveProductReview(ProductReview productReview);

  void deleteProductReview(Integer id);

  boolean isProductReviewExists(Integer id);

}
