package com.orileo.melloe.servicesinterface;

import com.orileo.melloe.model.payment.PaymentTransaction;

import java.util.List;

/**
 * Created by Mohamed BELMAHI on 21/08/2017.
 */
public interface PaymentTransactionService {
    void save(PaymentTransaction paymentTransaction);
    PaymentTransaction getPaymentTransactionById(String id);
    List<PaymentTransaction> getAuthorizedPaymentTransactions();
}
