package com.orileo.melloe.servicesinterface;

import com.orileo.melloe.model.notifications.NotificationMedium;

import java.util.List;
import java.util.Optional;

public interface NotificationMediumService {

    List<NotificationMedium> listAllNotificationMediums();

    Optional<NotificationMedium> getNotificationMedium(Integer id);

	 NotificationMedium saveNotificationMedium(NotificationMedium notificationMedium);

  void deleteNotificationMedium(Integer id);

  boolean isNotificationMediumExist(Integer id);

}
