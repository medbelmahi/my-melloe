package com.orileo.melloe.servicesinterface;

import com.orileo.melloe.model.forgotpassword.PasswordResetToken;
import com.orileo.melloe.model.login.MasterLogin;
import com.orileo.melloe.model.vendor.UserType;

import java.util.List;
import java.util.Optional;

public interface LoginService {

    List<MasterLogin> listAllLogins();

  Optional<MasterLogin> getLogin(Integer id);

    Optional<MasterLogin> findLoginByEmail(String email);

    Optional<MasterLogin> findByUserType(UserType userType);

    MasterLogin saveLogin(MasterLogin login);

  void deleteLogin(Integer id);

    void isMasterLoginExists(MasterLogin email);

  boolean isUserExists(Integer id);

    void deleteLogins(List<MasterLogin> userId);
    
    public PasswordResetToken createPasswordResetTokenForUser(MasterLogin user, String token);

	String validatePasswordResetToken(long id, String token);

	void changeUserPassword(MasterLogin user, String password);

	MasterLogin findUserByToken(String token);

}
