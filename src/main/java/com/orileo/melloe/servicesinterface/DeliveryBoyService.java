package com.orileo.melloe.servicesinterface;

import com.orileo.melloe.model.order.DeliveryBoy;

import java.util.List;
import java.util.Optional;

public interface DeliveryBoyService {

    List<DeliveryBoy> listAllDeliveryBoys();

    Optional<DeliveryBoy> getDeliveryBoy(Integer id);

	 DeliveryBoy saveDeliveryBoy(DeliveryBoy deliveryboy);

  void deleteDeliveryBoy(Integer id);

  boolean isDeliveryBoyExists(Integer id);

}
