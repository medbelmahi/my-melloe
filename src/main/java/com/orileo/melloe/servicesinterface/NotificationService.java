package com.orileo.melloe.servicesinterface;

import com.orileo.melloe.model.notifications.Notification;

import java.util.List;
import java.util.Optional;

public interface NotificationService {

	List<Notification> listAllNotifications();

	Optional<Notification> getNotification(Integer id);

	Notification saveNotification(Notification notifications);

  void deleteNotification(Integer id);

  boolean isNotificationExist(Integer id);

}
