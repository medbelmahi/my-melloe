package com.orileo.melloe.security;

import com.google.common.collect.Sets;
import com.orileo.melloe.model.login.MasterLogin;
import com.orileo.melloe.repositories.LoginRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

import static java.util.Optional.ofNullable;

@Component
public class UserDetailsServiceImpl implements UserDetailsService {
    private final LoginRepository loginRepository;
    private final List<IUserDetailsEnricher> userDetailsEnrichers;

    @Autowired
    public UserDetailsServiceImpl(LoginRepository loginRepository,
                                  List<IUserDetailsEnricher> userDetailsEnrichers) {
        this.loginRepository = loginRepository;
        this.userDetailsEnrichers = userDetailsEnrichers;
    }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        MasterLogin masterLogin = ofNullable(loginRepository.findByEmailId(username))
                .orElseThrow(() -> new AuthorizationServiceException("User with email not found : " + username));
        Set<GrantedAuthority> grantedAuthorities = Sets
                .newHashSet(new SimpleGrantedAuthority(masterLogin.getUserType().name()));
        UserDetails userDetails = new User(masterLogin.getEmailId(), masterLogin.getPassword(), grantedAuthorities);
        return userDetailsEnrichers.stream()
                .filter(u -> u.canEnrich(masterLogin.getUserType()))
                .map(u -> u.enrichUserDetails(userDetails))
                .findAny().orElseThrow(() ->
                        new AuthorizationServiceException("Failed to authorize user with email " + username));
    }
}
