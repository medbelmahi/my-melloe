package com.orileo.melloe.security;

import com.orileo.melloe.model.vendor.UserType;
import org.springframework.security.core.userdetails.UserDetails;

public interface IUserDetailsEnricher {
    boolean canEnrich(UserType userType);

    IEnrichedUserDetails enrichUserDetails(UserDetails userDetails);
}
