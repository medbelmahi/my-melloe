package com.orileo.melloe.security;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

@Component
public class HttpLogoutSuccessHandler implements LogoutSuccessHandler {
	@Override
	public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
			throws IOException {

		if (authentication != null)
			authentication.setAuthenticated(false);

		System.out.println("****Logout Successful!! ");
		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		response.getWriter().flush();
	}
}
