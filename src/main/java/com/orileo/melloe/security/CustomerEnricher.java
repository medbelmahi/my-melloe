package com.orileo.melloe.security;

import com.orileo.melloe.model.customer.Customer;
import com.orileo.melloe.model.vendor.UserType;
import com.orileo.melloe.servicesinterface.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Slf4j
@Component
public class CustomerEnricher implements IUserDetailsEnricher {
    private final CustomerService customerService;

    @Autowired
    public CustomerEnricher(CustomerService customerService) {
        this.customerService = customerService;
    }

    @Override
    public boolean canEnrich(UserType userType) {
        return userType == UserType.CUSTOMER;
    }

    @Override
    public IEnrichedUserDetails enrichUserDetails(UserDetails userDetails) {
        Optional<Customer> customer = customerService.findCustomerByEmail(userDetails.getUsername());
        if (!customer.isPresent()) {
            log.error("No customer associated with email {} , failed to authorize user", userDetails.getUsername());
            throw new AuthorizationServiceException(
                    "Failed to authorize user with email " + userDetails.getUsername());
        } else {
            return new BaseUserDetailsWrapper(userDetails) {
                @Override
                public Integer getVendorId() {
                    return null;
                }

                @Override
                public Integer getCustomerId() {
                    return customer.get().getId();
                }
            };
        }

    }
}
