package com.orileo.melloe.security;

import com.orileo.melloe.model.vendor.UserType;
import com.orileo.melloe.model.vendor.Vendor;
import com.orileo.melloe.servicesinterface.VendorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Slf4j
@Component
public class VendorEnricher implements IUserDetailsEnricher {
    private final VendorService vendorService;

    @Autowired
    public VendorEnricher(VendorService vendorService) {
        this.vendorService = vendorService;
    }

    @Override
    public boolean canEnrich(UserType userType) {
        return userType == UserType.VENDOR;
    }

    @Override
    public IEnrichedUserDetails enrichUserDetails(UserDetails userDetails) {
        Optional<Vendor> vendor = vendorService.findVendorByEmail(userDetails.getUsername());
        if (!vendor.isPresent()) {
            log.error("No vendor associated with email {} , failed to authorize user", userDetails.getUsername());
            throw new AuthorizationServiceException(
                    "Failed to authorize user with email " + userDetails.getUsername());
        } else return new BaseUserDetailsWrapper(userDetails) {
            @Override
            public Integer getVendorId() {
                return vendor.get().getId();
            }

            @Override
            public Integer getCustomerId() {
                return null;
            }
        };
    }
}
