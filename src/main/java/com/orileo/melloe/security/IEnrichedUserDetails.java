package com.orileo.melloe.security;

import org.springframework.security.core.userdetails.UserDetails;

public interface IEnrichedUserDetails extends UserDetails {
    Integer getVendorId();

    Integer getCustomerId();
}
