package com.orileo.melloe.security;

import com.orileo.melloe.model.vendor.UserType;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
public class AdminEnricher implements IUserDetailsEnricher {
    @Override
    public boolean canEnrich(UserType userType) {
        return UserType.ADMIN == userType;
    }

    @Override
    public IEnrichedUserDetails enrichUserDetails(UserDetails userDetails) {
        return new BaseUserDetailsWrapper(userDetails) {
            @Override
            public Integer getVendorId() {
                return null;
            }

            @Override
            public Integer getCustomerId() {
                return null;
            }
        };
    }
}
