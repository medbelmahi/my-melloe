package com.orileo.melloe.email;

public interface MailService {
	  public void sendEmail(Mail mail, String template);
}
