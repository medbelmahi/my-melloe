package com.orileo.melloe.email;

import java.util.Map;
import java.util.Properties;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.ui.freemarker.FreeMarkerConfigurationFactoryBean;
 
@Configuration
@ComponentScan(basePackages = "com.orileo.melloe.email")
public class EmailConfig {
	
	public Mail setMailCredentials(String mailTo, String subject,  Map < String, Object > model) {
	Mail mail = new Mail();
    mail.setMailFrom("support@melloe.in");
    mail.setMailTo(mailTo);
    mail.setMailSubject(subject);
    mail.setModel(model);
    return mail;
	}
 
    @Bean
    public JavaMailSender getMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
 
        mailSender.setHost("email-smtp.us-west-2.amazonaws.com");
        mailSender.setPort(465);
        mailSender.setUsername("AKIAIBPD6FPSPZV7RMTA");
        mailSender.setPassword("AvEPcx8sRT1OHDJoFsrUElLYCJXH+OjwmgtgH7Pllzji");
 
        Properties javaMailProperties = new Properties();
        javaMailProperties.put("mail.smtp.starttls.enable", "true");
        javaMailProperties.put("mail.smtp.auth", "true");
        javaMailProperties.put("mail.transport.protocol", "smtps");
        javaMailProperties.put("mail.debug", "true");
 
        mailSender.setJavaMailProperties(javaMailProperties);
        return mailSender;
    }
}