package com.orileo.melloe.controllers;

import com.orileo.melloe.model.exceptions.EntityNotFoundException;
import com.orileo.melloe.model.filter.ProductFilter;
import com.orileo.melloe.model.product.Product;
import com.orileo.melloe.servicesinterface.ProductFilterService;
import com.orileo.melloe.servicesinterface.ProductService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Slf4j
@Controller
public class ProductFilterController extends BaseControllerImpl {
    private final ProductFilterService productFilterService;
    private final ProductService productService;

    @Autowired
    public ProductFilterController(ProductFilterService productFilterService,
                                   ProductService productService) {
        this.productFilterService = productFilterService;
        this.productService = productService;
    }


    @ApiOperation(value = "Save Product Filter", response = ProductFilter.class,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Product filter succesfully created")})
    @PreAuthorize("hasAuthority('CUSTOMER')or hasAuthority('ADMIN')")
    @RequestMapping(value = "/productfilter", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> create(@RequestBody @Valid ProductFilter productFilter,
                                       UriComponentsBuilder ucBuilder) {
        productFilterService.saveProductFilter(productFilter);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/product/{id}").buildAndExpand(productFilter.getId()).toUri());
        log.info("Created Product Successfully with Name : " + productFilter.getName());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @ApiOperation(value = "Filter prodcucts", response = ProductFilter.class,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Product succesfully found")})
    @PreAuthorize("hasAuthority('CUSTOMER')or hasAuthority('ADMIN')")
    @RequestMapping(value = "/product/filter", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Product>> filterProducts(@RequestBody @Valid ProductFilter productFilter) {
        List<Product> products = productService.listProducts(productFilter);
        if (CollectionUtils.isEmpty(products)) {
            log.warn("Failed to find any products for filter {}", productFilter);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(products, HttpStatus.OK);
        }
    }

    @ApiOperation(value = "Get All filters for customer", response = ProductFilter.class,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Product filter succesfully returned")})
    @PreAuthorize("hasAuthority('CUSTOMER')or hasAuthority('ADMIN')")
    @RequestMapping(value = "/productfilter", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ProductFilter>> getAllFilters() {
        List<ProductFilter> products = productFilterService.listAllProductFiltersForCustomer(getCustomerId());
        if (CollectionUtils.isEmpty(products)) {
            log.warn("Failed to find any productfilters");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(products, HttpStatus.OK);
        }
    }

    @ApiOperation(value = "Delete product filter",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Product filter succesfully deleted")})
    @PreAuthorize("hasAuthority('CUSTOMER')or hasAuthority('ADMIN')")
    @RequestMapping(value = "/productfilter/{id}", method = RequestMethod.DELETE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Product>> delete(Integer id) {
        Optional<ProductFilter> filter = productFilterService.getProductFilter(id, getCustomerId());
        if (!filter.isPresent()) {
            log.warn("Unable to find product filer with ID {}", id);
            throw new EntityNotFoundException("Product Filter", id);
        }
        productFilterService.deleteProductFilter(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
