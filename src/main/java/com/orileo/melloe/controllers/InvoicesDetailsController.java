package com.orileo.melloe.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;

@Slf4j
@Controller
public class InvoicesDetailsController extends BaseControllerImpl {

//	private InvoicesDetailsService invoicesDetailsService;
//	private InvoicesService invoicesService;
//	private ProductPriceService productPriceService;
//	private ProductService productService;
//
//	@Autowired
//	public void setInvoiceService(InvoicesService invoicesService) {
//		this.invoicesService = invoicesService;
//	}
//
//
//
//	@Autowired
//	public void setProductService(ProductService productService) {
//		this.productService = productService;
//	}
//	@Autowired
//	public void setProductPriceService(ProductPriceService productPriceService) {
//		this.productPriceService = productPriceService;
//	}
//	@Autowired
//	public void setInvoiceDetailsService(InvoicesDetailsService invoicesDetailsService) {
//		this.invoicesDetailsService = invoicesDetailsService;
//	}
//
//	@RequestMapping(value = "/invoicedetails", method = RequestMethod.GET)
//	public ResponseEntity<List<InvoicesDetails>> list() {
//		System.out.println("Inside controller");
//		List<InvoicesDetails> InvoicesDetails = (List<InvoicesDetails>) invoicesDetailsService.listAllInvoices();
//		if(InvoicesDetails.isEmpty()){
//            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
//        }
//        return new ResponseEntity<>(InvoicesDetails, HttpStatus.OK);
//    }
//
//	@RequestMapping(value="/invoicedetails/{id}", method = RequestMethod.GET)
//	public ResponseEntity<InvoicesDetails> getListById(@PathVariable int id) {
//
//		InvoicesDetails InvoicesDetails = invoicesDetailsService.getById(id);
//		if(InvoicesDetails==null){
//            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
//        }
//        return new ResponseEntity<>(InvoicesDetails, HttpStatus.OK);
//    }
//
//	@RequestMapping(value="/invoicedetails",method = RequestMethod.POST)
//	public ResponseEntity<Void> create(@RequestBody Map<String, Object> payload ) {
//		InvoicesDetails invoicesDetails=new InvoicesDetails();
//
//		HashMap invoicesMap=(HashMap) payload.get("invoices");
//		HashMap productMap=(HashMap) payload.get("product");
//		HashMap productpriceMap=(HashMap) payload.get("productprice");
//
//		int invoiceId=Integer.parseInt(invoicesMap.get("invoiceid").toString());
//		int productId=Integer.parseInt(productMap.get("productid").toString());
//		int productpriceId=Integer.parseInt(productpriceMap.get("productpriceid").toString());
//
//		Invoices invoices=invoicesService.getById(invoiceId);
//        Optional<Product> product = productService.getProduct
//                (productId);
//        ProductPrice price=productPriceService.getProductPriceById(productpriceId);
//
//		invoicesDetails.setInvoices(invoices);
//        invoicesDetails.setProduct(product.get());
//        invoicesDetails.setProductprice(price);
//		invoicesDetails.setPricePerUnit(Double.parseDouble(payload.get("priceperunit").toString()));
//		invoicesDetails.setQuantity(Integer.parseInt(payload.get("quantity").toString()));
//		invoicesDetails.setTotalprice(Double.parseDouble(payload.get("totalprice").toString()));
//		invoicesDetailsService.saveInvoices(invoicesDetails);
//
//
//        return new ResponseEntity<>(HttpStatus.CREATED);
//    }
//
//	@RequestMapping(value = "/invoicedetails/{id}", method = RequestMethod.PUT)
//	public ResponseEntity<InvoicesDetails> update(@PathVariable int id, @RequestBody Map<String, Object> payload) {
//		InvoicesDetails invoicesDetails =invoicesDetailsService.getById(id) ;
//		if (invoicesDetails == null) {
//            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//        }
//
//		HashMap invoicesMap=(HashMap) payload.get("invoices");
//		HashMap productMap=(HashMap) payload.get("product");
//		HashMap productpriceMap=(HashMap) payload.get("productprice");
//
//		int invoiceId=Integer.parseInt(invoicesMap.get("invoiceid").toString());
//		int productId=Integer.parseInt(productMap.get("productid").toString());
//		int productpriceId=Integer.parseInt(productpriceMap.get("productpriceid").toString());
//
//		Invoices invoices=invoicesService.getById(invoiceId);
//        Optional<Product> product = productService.getProduct(productId);
//        ProductPrice price=productPriceService.getProductPriceById(productpriceId);
//
//		invoicesDetails.setInvoices(invoices);
//        invoicesDetails.setProduct(product.get());
//        invoicesDetails.setProductprice(price);
//		invoicesDetails.setPricePerUnit(Double.parseDouble(payload.get("priceperunit").toString()));
//		invoicesDetails.setQuantity(Integer.parseInt(payload.get("quantity").toString()));
//		invoicesDetails.setTotalprice(Double.parseDouble(payload.get("totalprice").toString()));
//		invoicesDetailsService.updateInvoices(invoicesDetails);
//
//
//        return new ResponseEntity<>(invoicesDetails, HttpStatus.OK);
//    }
//
//	@RequestMapping(value="/invoicedetails/{id}",method=RequestMethod.DELETE)
//	public ResponseEntity<InvoicesDetails> delete(@PathVariable int id) {
//		System.out.println("Fetching & Deleting Product with id " + id);
//
//		InvoicesDetails InvoicesDetails = invoicesDetailsService.getById(id);
//
//		if (InvoicesDetails == null) {
//			System.out.println("Unable to delete. product with id " + id + " not found");
//            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//        }
//
//		invoicesDetailsService.deleteInvoicesById(id);
//        return new ResponseEntity<>(HttpStatus.OK);
//    }
}
