package com.orileo.melloe.controllers;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import com.orileo.melloe.model.exceptions.CheckPaidAmountException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.util.UriComponentsBuilder;
import com.orileo.melloe.commonmethods.CommonMethods;
import com.orileo.melloe.controllers.webmodel.OrderResource;
import com.orileo.melloe.controllers.webmodel.OrderResourceConverter;
import com.orileo.melloe.email.EmailConfig;
import com.orileo.melloe.email.Mail;
import com.orileo.melloe.email.MailService;
import com.orileo.melloe.model.cart.Cart;
import com.orileo.melloe.model.customer.Customer;
import com.orileo.melloe.model.customer.CustomerPromoCode;
import com.orileo.melloe.model.exceptions.EntityNotFoundException;
import com.orileo.melloe.model.order.Order;
import com.orileo.melloe.model.order.OrderNumber;
import com.orileo.melloe.model.promocode.PromoCode;
import com.orileo.melloe.servicesinterface.CartService;
import com.orileo.melloe.servicesinterface.CustomerPromoCodeService;
import com.orileo.melloe.servicesinterface.InvoicesService;
import com.orileo.melloe.servicesinterface.NotificationSenderService;
import com.orileo.melloe.servicesinterface.OrdersService;
import com.orileo.melloe.servicesinterface.PromocodeService;
import com.orileo.melloe.servicesinterface.SessionService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class OrderController extends BaseControllerImpl {
    private final OrdersService ordersService;
    private final InvoicesService invoicesService;
    private final OrderResourceConverter resourceConverter;
    private final MailService mailService;
    private final NotificationSenderService notificationSenderService;
    private final SessionService sessionService;
    private final CartService cartService;
    private final PromocodeService promocodeService;
    private final CustomerPromoCodeService customerPromoCodeService;

    @Value("${melloe.order.prefix}")
    private String ordePrefix;
    
    @Autowired
    public OrderController(OrdersService ordersService,
                           OrderResourceConverter resourceConverter,
                           InvoicesService invoicesService,
                           MailService mailService,
                           NotificationSenderService notificationSenderService,
                           CustomerPromoCodeService customerPromoCodeService,
                           PromocodeService promocodeService,
                           @Value("${melloe.order.deliverycharges}") double deliveryCharges, SessionService sessionService, CartService cartService) {
        this.ordersService = ordersService;
        this.resourceConverter = resourceConverter;
        this.invoicesService = invoicesService;
        this.mailService = mailService;
        this.notificationSenderService = notificationSenderService;
        this.sessionService = sessionService;
        this.cartService = cartService;
        this.promocodeService = promocodeService;
        this.customerPromoCodeService = customerPromoCodeService;
        
    }

    @ApiOperation(value = "Find all orders", response = Order.class, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "No orders found"),
            @ApiResponse(code = 200, message = "Orders succesfully returned")})
    @RequestMapping(value = "/order", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Order>> list() {
        List<Order> orders = ordersService.listAllOrders(getVendorId(), getCustomerId());
        if (CollectionUtils.isEmpty(orders)) {
            log.warn("There are no Orders found");
            throw new EntityNotFoundException("Orders");
        }
        return new ResponseEntity<>(orders, HttpStatus.OK);
    }

    @ApiOperation(value = "Find Order by ID", response = Order.class,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "No orders found"),
            @ApiResponse(code = 200, message = "Order succesfully returned")})
    @RequestMapping(value = "/order/{id}", method = RequestMethod.GET)
    public ResponseEntity<Order> getOrder(@PathVariable int id) {
        Optional<Order> order = ordersService.getOrder(id, getCustomerId(), getVendorId());
        if (!order.isPresent()) {
            log.warn("Order with ID :{} not found ", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(order.get(), HttpStatus.OK);
        }
    }

    @ApiOperation(value = "Find Orders by vendor", response = Order.class,produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "No orders found"),
            @ApiResponse(code = 200, message = "Order succesfully returned")})
    @RequestMapping(value = "/vendorOrder/{vendorID}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Order>> getVendorOrder(@PathVariable int vendorID)
    {
        List<Order> orders = ordersService.listAllOrders(vendorID, getCustomerId());
        System.out.println("Orders count "+orders.size());
        if (CollectionUtils.isEmpty(orders))
        {
            log.warn("Order with vendor ID :{} not found ", vendorID);
            throw new EntityNotFoundException("Orders");
        }
        return new ResponseEntity<>(orders, HttpStatus.OK);
    }
    
    @ApiOperation(value = "Find new Orders by vendor", response = Order.class,produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "No new orders found"),
            @ApiResponse(code = 200, message = "Order succesfully returned")})
    @RequestMapping(value = "/vendorNewOrder/{vendorID}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Order>> getOrdersByStatus(@PathVariable int vendorID)
    {
        List<Order> orders = ordersService.getOrdersByStatus(vendorID);
        if (CollectionUtils.isEmpty(orders))
        {
            log.warn("Order with vendor ID :{} not found ", vendorID);
            throw new EntityNotFoundException("Orders");
        }
        return new ResponseEntity<>(orders, HttpStatus.OK);
    }
    
    @ApiOperation(value = "Create Order", response = OrderResource.class,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Order succesfully created")})
    @PreAuthorize("hasAuthority('CUSTOMER') or hasAuthority('ADMIN')")
    @RequestMapping(value = "/order", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Integer> create(@Valid @RequestBody OrderResource orderResource,
                                          UriComponentsBuilder ucBuilder, HttpSession session) throws CheckPaidAmountException {
        final Cart sessionCart = sessionService.getSessionCart(session);
        final Cart cart = cartService.getCart(sessionCart.getId());
        cartService.checkPaidAmount(cart, orderResource);
        Order order = resourceConverter.convertToOrder(null, orderResource, getCustomerId(), cart);
        ordersService.calculateOrder(order);
        Customer tempCustomer = order.getCustomer();
        // sequential order number code
        Optional<OrderNumber> orderNumberOptional = ordersService.getOrderNumber(1);
        OrderNumber orderNumber = orderNumberOptional.get();
        order.setOrderNumber(ordePrefix+""+orderNumber.getNumber());
//        ordersService.calculateOrder(order);
        ordersService.saveOrder(order);

        // Commented throwing error
//        cartService.deleteCart(cart, getCustomerId());

        session.setAttribute(SessionService.CURRENT_CART, null);

        
        new Thread(new Runnable() {
            public void run() {
	            if(order != null)
	            {
	            	System.out.println("********************DB Thread***********************");
	            	OrderNumber ordNum = new OrderNumber();
	            	ordNum.setId(orderNumber.getId());
	            	ordNum.setNumber((orderNumber.getNumber()+1));
	            	ordersService.saveOrderNumber(ordNum);
	                invoicesService.generateInvoice(order);
	                

	            }
            }
        }).start();


        new Thread(new Runnable() {
            public void run() {
	            if(order != null)
	            {
	            	if(order.getPromoCodeAmount() != null && order.getPromoCodeAmount() > 0)
	            	{
	            		order.setTotalPrice(order.getTotalOrderPriceWithPromocode());
	            	}
	            	
	            	System.out.println("********************Mail Thread***********************");
		            //Mail to be sent to the respective Vendor
		            Map < String, Object > model = new HashMap < String, Object > ();
		            model.put("name", order.getOrderDetails().get(0).getProduct().getVendor().getName());
		            model.put("orders", order.getOrderDetails());
		            EmailConfig emailConfig = new EmailConfig();
		            String vendor_email= order.getOrderDetails().get(0).getProduct().getVendor().getUserId().get(0).getEmailId();
		            Mail mail = emailConfig.setMailCredentials(vendor_email, "New Order Placed", model);
		            mailService.sendEmail(mail, "VendorOrderPlaced.txt");
		            
		            //Mail to be sent to the customer placing the order.
		            //Map < String, Object > modelnew = new HashMap < String, Object > ();
		            //double handlingCharge = deliveryCharges+ ((deliveryCharges *18)/100);
		            model.put("orders", order.getOrderDetails());
		            model.put("totalPrice", order.getOrderDetails().get(0).getTotalPrice());
		            model.put("handlingCharge", order.getHandlingCharge());
		            model.put("gst", order.getGstCharge());
		            
		            if(order.getPromoCodeAmount() != null && order.getPromoCodeAmount() > 0)
		            {
			            model.put("GrandTotal", order.getTotalOrderPriceWithPromocode());
		            }else
		            {
			            model.put("GrandTotal", order.getTotalPrice());
		            }
		            model.put("PromoCodeAmount",order.getPromoCodeAmount());
		            EmailConfig emailConfignew = new EmailConfig();
		            String customer_email= order.getCustomer().getUserId().getEmailId();
		            Mail mailnew = emailConfignew.setMailCredentials(customer_email, "Your Order is Recieved", model);
		            mailService.sendEmail(mailnew, "OrderMail.txt");
		            
		            // Support Email Tracking
		            Mail supportEmail = emailConfignew.setMailCredentials("support@melloe.in", "Order is recieved from customer", model);
		            mailService.sendEmail(supportEmail, "OrderMail.txt");

	            }
            }
        }).start();

        new Thread(new Runnable() {
            public void run() {
	            if(order != null)
	            {
	            	System.out.println("********************SMS Thread***********************");
		            //this is to fire message to respective vendor
		            notificationSenderService.sendSMS(CommonMethods.vendorMessage(order), order.getOrderDetails().get(0).getProduct().getVendor().getCellNumbers().get(0).getPhoneNumber());

		            //this is to fire message to respective customers
		            notificationSenderService.sendSMS(CommonMethods.customerMessage(order), order.getCustomer().getPhoneNumber());

	            }
            }
        }).start();
        
        new Thread(new Runnable() {
            public void run() {
	            if(order != null && order.getPromoCode() != null && order.getPromoCodeAmount() > 0)
	            {
	            	System.out.println("********************PROMO TRACK DB***********************");
	            	Optional<PromoCode> promoCodeObj = promocodeService.getPromoCode(order.getPromoCode());
        			PromoCode promoCode = promoCodeObj.get();
	            	CustomerPromoCode customerPromoCode = new CustomerPromoCode();
	            	customerPromoCode.setPromoCode(promoCode);
	            	customerPromoCode.setAppId("");
	            	customerPromoCode.setIpAddress("");
	            	customerPromoCode.setPromoCodeUsedOn(new Date());
	            	customerPromoCode.setUsed(true);
	            	customerPromoCode.setCustomer(tempCustomer);
	            	customerPromoCode.setFromDate(promoCode.getStartDate());
	            	customerPromoCode.setToDate(promoCode.getEndDate());
	            	customerPromoCodeService.saveCustomerPromocode(customerPromoCode);
	            	
	            }
            }
        }).start();
        
        
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/order/{id}").buildAndExpand(order.getId()).toUri());

        return new ResponseEntity<>(order.getId(), headers, HttpStatus.CREATED);

    }

    @ApiOperation(value = "Update Order", response = OrderResource.class,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Order not found"),
            @ApiResponse(code = 200, message = "Order succesfully updated")})
    @PreAuthorize("hasAuthority('CUSTOMER')  or hasAuthority('ADMIN')")
    @RequestMapping(value = "/order/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Order> update(@PathVariable int id,
                                        @Valid @RequestBody OrderResource orderResource, HttpSession session) {
        if (!ordersService.isOrderExist(id, getVendorId(), getCustomerId())) {
            log.warn("Failed to find order with ID {}", id);
            throw new EntityNotFoundException("Order", id);
        } else {
            final Cart sessionCart = sessionService.getSessionCart(session);
            Order order = resourceConverter.convertToOrder(id, orderResource, getCustomerId(), sessionCart);
            ordersService.saveOrder(order);
          invoicesService.generateInvoice(order);
          return new ResponseEntity<>(order, HttpStatus.OK);
        }
    }

    @ApiOperation(value = "Delete Order", response = Order.class,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Order not found"),
            @ApiResponse(code = 204, message = "Order succesfully deleted")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/order/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Order> delete(@PathVariable int id) {
        if (!ordersService.getOrder(id, getCustomerId(), null).isPresent()) {
            log.warn("Failed to find order with ID {}", id);
            throw new EntityNotFoundException("Order", id);
        } else {
            ordersService.deleteOrder(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }
    }
}