package com.orileo.melloe.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;

@Slf4j
@Controller
public class DevicesController extends BaseControllerImpl {

//	 private DevicesService devicesService;
//	 private LoginService loginService;
//
//	    @Autowired
//	    public void setLoginService(LoginService loginService) {
//	        this.loginService = loginService;
//	    }
//	    @Autowired
//	    public void setDevicesService(DevicesService devicesService) {
//	        this.devicesService = devicesService;
//	    }
//
//	    @RequestMapping(value = "/devices", method = RequestMethod.GET)
//		public ResponseEntity<List<Device>> list() {
//	    	System.out.println("Inside controller");
//			List<Device> devices = (List<Device>) devicesService.listAllDevices();
//			if(devices.isEmpty()){
//                return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
//            }
//            return new ResponseEntity<>(devices, HttpStatus.OK);
//        }
//
//	    @RequestMapping(value="/devices/{id}", method = RequestMethod.GET)
//	    public ResponseEntity<Device> getListById(@PathVariable int id) {
//
//	    	Device devices = devicesService.getById(id);
//			if(devices==null){
//                return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
//            }
//            return new ResponseEntity<>(devices, HttpStatus.OK);
//        }
//
//	    @RequestMapping(value="/devices",method = RequestMethod.POST)
//		public ResponseEntity<Void> create(@RequestBody Map<String, Object> payload) {
//
//			Device devices=new Device();
//
//			devices.setDeviceId(payload.get("deviceid").toString());
//			HashMap loginMap=(HashMap)payload.get("email");
//			String email=loginMap.get("emailid").toString();
//            MasterLogin logins = loginService.findByEmail(email);
//            devices.setEmailId(logins);
//			devicesService.saveDevices(devices);
//
//            return new ResponseEntity<>(HttpStatus.CREATED);
//        }
//
//	    @RequestMapping(value = "/devices/{id}", method = RequestMethod.PUT)
//		public ResponseEntity<Device> update(@PathVariable int id, @RequestBody Map<String, Object> payload) {
//	    	Device devices = devicesService.getById(id);
//
//			if (devices == null) {
//                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//            }
//
//			devices.setDeviceId(payload.get("deviceid").toString());
//			HashMap loginMap=(HashMap)payload.get("email");
//			String email=loginMap.get("emailid").toString();
//            MasterLogin logins = loginService.findByEmail(email);
//            devices.setEmailId(logins);
//			devicesService.updateDevices(devices);
//            return new ResponseEntity<>(devices, HttpStatus.OK);
//        }
//
//	   @RequestMapping(value="/devices/{id}",method=RequestMethod.DELETE)
//	    public ResponseEntity<Device> delete(@PathVariable int id) {
//			System.out.println("Fetching & Deleting Product with id " + id);
//
//			Device devices = devicesService.getById(id);
//
//			if (devices == null) {
//				System.out.println("Unable to delete. product with id " + id + " not found");
//                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//            }
//
//			devicesService.deleteDevicesById(id);
//           return new ResponseEntity<>(HttpStatus.OK);
//       }
}
