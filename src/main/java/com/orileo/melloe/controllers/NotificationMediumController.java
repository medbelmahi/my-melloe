package com.orileo.melloe.controllers;

import com.orileo.melloe.model.exceptions.EntityNotFoundException;
import com.orileo.melloe.model.notifications.NotificationMedium;
import com.orileo.melloe.model.notifications.NotificationType;
import com.orileo.melloe.servicesinterface.NotificationMediumService;
import com.orileo.melloe.servicesinterface.NotificationTypeService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Slf4j
@Controller
public class NotificationMediumController extends BaseControllerImpl {

    public static final String NOTIFICATION_MEDIUM = "Notification medium";
    private final NotificationMediumService notificationMediumService;
    private final NotificationTypeService notificationTypeService;

    @Autowired
    public NotificationMediumController(NotificationMediumService notificationMediumService,
                                        NotificationTypeService notificationTypeService) {
        this.notificationMediumService = notificationMediumService;
        this.notificationTypeService = notificationTypeService;
    }

    @ApiOperation(value = "Find all notification mediums", response = NotificationMedium.class,
            responseContainer = "list",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "No notification mediums found"),
            @ApiResponse(code = 200, message = "Notification mediums succesfully returned")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/notificationmedium", method = RequestMethod.GET)
    public ResponseEntity<List<NotificationMedium>> list() {
        List<NotificationMedium> notificationMediums = notificationMediumService.listAllNotificationMediums();
        if (CollectionUtils.isEmpty(notificationMediums)) {
            log.warn("There are no Notification mediums found");
            throw new EntityNotFoundException("Notification mediums");
        }
        return new ResponseEntity<>(notificationMediums, HttpStatus.OK);
    }

    @ApiOperation(value = "Get Notification Medium by ID", response = NotificationMedium.class,
            responseContainer = "list",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "No notification mediums found"),
            @ApiResponse(code = 200, message = "Notification medium succesfully returned")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/notificationmedium/{id}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<NotificationMedium> getListById(@PathVariable int id) {
        Optional<NotificationMedium> notificationMedium = notificationMediumService.getNotificationMedium(id);
        if (!notificationMedium.isPresent()) {
            log.warn("Notification Medium with ID :{} not found ", id);
            throw new EntityNotFoundException(NOTIFICATION_MEDIUM, id);
        }
        return new ResponseEntity<>(notificationMedium.get(), HttpStatus.OK);
    }

    @ApiOperation(value = "Create Notification Medium", response = NotificationMedium.class,
            responseContainer = "list",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Notification medium succesfully created")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/notificationtype/{notificationTypeID}/notificationmedium",
            method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Void> create(
            @PathVariable Integer notificationTypeID,
            @Valid @RequestBody NotificationMedium notificationMedium,
            UriComponentsBuilder ucBuilder) {
        Optional<NotificationType> notificationType =
                notificationTypeService.getNotificationType(notificationTypeID);
        if (!notificationType.isPresent()) {
            log.error("Notification Type with ID {} not found", notificationTypeID);
            throw new EntityNotFoundException("Notification type", notificationTypeID);
        } else {
            notificationMedium.setNotificationType(notificationType.get());
            notificationMediumService.saveNotificationMedium(notificationMedium);
            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(ucBuilder.path("/notificationmedium/{id}")
                    .buildAndExpand(notificationMedium.getId()).toUri());
            return new ResponseEntity<>(headers, HttpStatus.CREATED);
        }
    }

    @ApiOperation(value = "Update Notification Medium", response = NotificationMedium.class,
            responseContainer = "list",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "No notification mediums found"),
            @ApiResponse(code = 201, message = "Notification medium succesfully created")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/notificationmedium/{id}", method = RequestMethod.PUT)
    public ResponseEntity<NotificationMedium> update(
            @PathVariable int id,
            @Valid @RequestBody NotificationMedium notificationMedium) {
        if (!notificationMediumService.isNotificationMediumExist(id)) {
            log.warn("Notification Medium with ID :{} not found ", id);
            throw new EntityNotFoundException("Notification medium", id);
        } else {
            notificationMedium.setId(id);
            notificationMediumService.saveNotificationMedium(notificationMedium);
            return new ResponseEntity<>(HttpStatus.OK);
        }
    }

    @ApiOperation(value = "Delete Notification Medium", response = NotificationMedium.class,
            responseContainer = "list",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "No notification mediums found"),
            @ApiResponse(code = 204, message = "Notification medium succesfully deleted")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/notificationmedium/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<NotificationMedium> delete(
            @PathVariable int id) {
        if (!notificationMediumService.isNotificationMediumExist(id)) {
            log.warn("Notification Medium with ID :{} not found ", id);
            throw new EntityNotFoundException("Notification medium", id);
        } else {
            notificationMediumService.deleteNotificationMedium(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
}
