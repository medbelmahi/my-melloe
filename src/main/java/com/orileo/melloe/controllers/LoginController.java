package com.orileo.melloe.controllers;

import com.orileo.melloe.email.EmailConfig;
import com.orileo.melloe.email.Mail;
import com.orileo.melloe.email.MailService;
import com.orileo.melloe.model.cart.Cart;
import com.orileo.melloe.model.customer.Customer;
import com.orileo.melloe.model.exceptions.EntityNotFoundException;
import com.orileo.melloe.model.forgotpassword.PasswordResetToken;
import com.orileo.melloe.model.login.MasterLogin;
import com.orileo.melloe.model.vendor.UserType;
import com.orileo.melloe.security.IEnrichedUserDetails;
import com.orileo.melloe.servicesinterface.CartService;
import com.orileo.melloe.servicesinterface.CustomerService;
import com.orileo.melloe.servicesinterface.LoginService;

import com.orileo.melloe.servicesinterface.SessionService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@Controller
public class LoginController extends BaseControllerImpl {

	private final LoginService loginService;
    private final SessionService sessionService;
    private final CustomerService customerService;
    private final CartService cartService;
    private final MailService mailService;

    @Autowired
	 private JavaMailSender mailSender;

	@Autowired
	public LoginController(MailService mailService,LoginService loginService, SessionService sessionService, CustomerService customerService, CartService cartService) {
		this.loginService = loginService;
        this.sessionService = sessionService;
        this.customerService = customerService;
        this.cartService = cartService;
        this.mailService = mailService;
    }

    @RequestMapping(value = "/masterlogin/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MasterLogin> get(@PathVariable("id") int id,
										   @AuthenticationPrincipal UserDetails userDetails) {
		Optional<MasterLogin> masterLogin = loginService.getLogin(id);
		if (!masterLogin.isPresent()) {
			throw new EntityNotFoundException("Login", id);
		} else if (!masterLogin.get().getEmailId().equals(userDetails.getUsername())) {
			throw new AuthorizationServiceException("Attempt to access alien entity");
		} else {
			return new ResponseEntity<MasterLogin>(masterLogin.get(), HttpStatus.OK);
		}
	}

    @RequestMapping(value = "/masterlogin", method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> update(@Valid @RequestBody MasterLogin masterLogin,
                                       @AuthenticationPrincipal UserDetails userDetails) {
        if (!masterLogin.getEmailId().equals(userDetails.getUsername())) {
            log.error("Tried to update password of wrong user. Passed user is {} , actual user is {}");
            throw new AuthorizationServiceException("Failed to update user details");
        } else {
            loginService.saveLogin(masterLogin);
            return new ResponseEntity<>(HttpStatus.OK);
        }
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/masterlogin/{id}", method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> update(@PathVariable("id") int id) {
        loginService.deleteLogin(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

  @RequestMapping(value = "/masterlogin", method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<MasterLogin> getCurrentUser(HttpSession session) {
    IEnrichedUserDetails userDetails = getCurrentUserDetails();
    MasterLogin masterLogin = new MasterLogin();
//    masterLogin.setId(userDetails.getCustomerId());
    masterLogin.setEmailId(userDetails.getUsername());
    UserType userType = userDetails.getAuthorities().stream().map(auth -> UserType.valueOf(auth.getAuthority()))
        .findAny().get();
    masterLogin.setUserType(userType);
    if(UserType.CUSTOMER.equals(userType)) {
    	 masterLogin.setId(userDetails.getCustomerId());
        final Optional<Customer> customer = customerService.getCustomer(userDetails.getCustomerId());
        if (customer.isPresent()) {
            final Customer currentCustomer = customer.get();
            final Cart cart = currentCustomer.getCart();
            if (cart != null) {
                sessionService.setSessionCart(session, cart);
            } else {
                final Cart sessionCart = sessionService.getSessionCart(session);
                Cart cartModel = cartService.getCart(sessionCart.getId());
                currentCustomer.setCart(cartModel);
                customerService.saveCustomer(currentCustomer);
            }
        }

    } else if(UserType.VENDOR.equals(userType)) {
    	 masterLogin.setId(userDetails.getVendorId());
    }
    
    return new ResponseEntity<MasterLogin>(masterLogin, HttpStatus.OK);
  }
  
  @ApiOperation(value = "Reset Password")
  @ApiResponses(value = {
          @ApiResponse(code = 404, message = "Customer not found"),
          @ApiResponse(code = 204, message = "Token sent")})   
  @RequestMapping(value = "/resetPassword", 
          method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<PasswordResetToken> resetPassword(HttpServletRequest request, 
  @RequestParam("email") String userEmail) {
	 Optional<MasterLogin> user= loginService.findLoginByEmail(userEmail);
   if (user == null) {
	 return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
   }
   String token = UUID.randomUUID().toString();
   PasswordResetToken tok= loginService.createPasswordResetTokenForUser(user.get(), token);
   
   Map < String, Object > model = new HashMap < String, Object > ();
   
   String url = "http://test.melloe.in:234/resetpassword.html?id="+user.get().getId() + "&token=" + token;   
   model.put("uri", url);
   model.put("link", "<a href='"+url+"' tagget='blank'>Reset</a>");
   model.put("id", user.get().getId());
   model.put("token", token);
   EmailConfig emailConfig = new EmailConfig();
   String user_email= user.get().getEmailId();
   Mail mail = emailConfig.setMailCredentials(user_email, "Pasword Reset", model);
   mailService.sendEmail(mail, "ForgotPassword.txt");
   
   //mailSender.send(constructResetTokenEmail(getAppUrl(request), 
   //request.getLocale(), token, user.get()));
   return new ResponseEntity<PasswordResetToken>(tok, HttpStatus.OK);
  }
  
  @ApiOperation(value = "Check Token Validity")
  @ApiResponses(value = {
          @ApiResponse(code = 404, message = "Token not found"),
          @ApiResponse(code = 204, message = "Token sent")})
  @RequestMapping(value = "/user/changePassword", method = RequestMethod.GET)
  public ResponseEntity<String> showChangePasswordPage( @RequestParam("id") long id, @RequestParam("token") String token)
  {
      String result = loginService.validatePasswordResetToken(id, token);
      System.out.println("result "+result);
      if (result != null) {
          //model.addAttribute("Invalid_Token");
          return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
         // return "redirect:/login?lang=" + locale.getLanguage();
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
  }
  
  @ApiOperation(value = "Save New Password")
  @ApiResponses(value = {
          @ApiResponse(code = 404, message = "Password not Updated"),
          @ApiResponse(code = 204, message = "Password Updated")})
  @RequestMapping(value = "/user/savePassword", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<String> savePassword(Locale locale, @RequestBody PasswordDto passwordDto)
  {
	  String status = "";
      String result = loginService.validatePasswordResetToken(passwordDto.getId(), passwordDto.getToken());
	  if(result != null)
	  {
		  status = "updated sucessfully"; 
		  System.out.println("Updated Successfully");
		  MasterLogin user = loginService.findUserByToken(passwordDto.getToken());
		  loginService.changeUserPassword(user, passwordDto.getNewPassword());
	      return new ResponseEntity<>(status, HttpStatus.ACCEPTED);
	  }
	  else
	  {
	      return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);
	  }
  }

// ============== NON-API ============
private SimpleMailMessage constructResetTokenEmail(
		  String contextPath, Locale locale, String token, MasterLogin user) {
		    String url = contextPath + "/user/changePassword?id=" + 
		      user.getId() + "&token=" + token;
		    String message ="resetPassword";
		    return constructEmail("Reset Password", message + " \r\n" + url, user);
		}

private SimpleMailMessage constructEmail(String subject, String body, 
		  MasterLogin user) {
		    SimpleMailMessage email = new SimpleMailMessage();
		    email.setSubject(subject);
		    email.setText(body);
		    email.setTo(user.getEmailId());
		    email.setFrom("support@melloe.in");
		    return email;
		}

private String getAppUrl(HttpServletRequest request) {
    return "http://" + request.getServerName() + ":" + request.getServerPort() + "/api" + request.getContextPath();
}

}
