package com.orileo.melloe.controllers;

import com.orileo.melloe.model.payment.Invoice;
import com.orileo.melloe.servicesinterface.InvoicesService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Slf4j
@Controller
public class InvoicesController extends BaseControllerImpl {
    private final InvoicesService invoicesService;

    @Autowired
    public InvoicesController(InvoicesService invoicesService) {
        this.invoicesService = invoicesService;
    }

    @RequestMapping(value = "/order/{orderID}/invoice", method = RequestMethod.GET)
    public ResponseEntity<List<Invoice>> listInvoicesForOrder(@PathVariable Integer orderID) {
        List<Invoice> invoices = invoicesService.listAllInvoicesForOrder(orderID);
        if (CollectionUtils.isEmpty(invoices)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(invoices, HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('CUSTOMER')")
    @RequestMapping(value = "/customer/invoice", method = RequestMethod.GET)
    public ResponseEntity<List<Invoice>> listInvoicesForCustomer() {
        List<Invoice> invoices = invoicesService.listAllInvoicesForCustomer(getCustomerId());
        if (CollectionUtils.isEmpty(invoices)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(invoices, HttpStatus.OK);
    }
}
