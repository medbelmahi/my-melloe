package com.orileo.melloe.controllers.webmodel;

import com.orileo.melloe.model.cart.Cart;
import com.orileo.melloe.model.cart.CartDetails;
import com.orileo.melloe.model.customer.Customer;
import com.orileo.melloe.model.customer.CustomerAddress;
import com.orileo.melloe.model.exceptions.EntityNotFoundException;
import com.orileo.melloe.model.order.Order;
import com.orileo.melloe.model.order.OrderDetails;
import com.orileo.melloe.servicesinterface.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Optional.ofNullable;

@Component
public class OrderResourceConverter {
    private final CustomerService customerService;
    @Autowired
    public OrderResourceConverter(CustomerService customerService) {
        this.customerService = customerService;
    }

    public Order convertToOrder(Integer id, @NotNull OrderResource orderResource, Integer customerId, Cart cart) {
        Order order = new Order();
        if (!(id == null)) {
            order.setId(id);
        }
        setCustomer(order, customerId);
        order.setGstCharge(cart.getGstCharge());
        order.setHandlingCharge(cart.getHandlingCharge());
        order.setPackagingCharges(cart.getPackagingCharges());
        order.setTotalPrice(cart.getTotalPrice());
        setOrderDetails(order, cart.getCartDetails());        
        order.setCreatedDate(orderResource.getCreatedDate());
        order.setLastUpdated(orderResource.getLastUpdated());
        order.setRequiredDate(orderResource.getRequiredDate());
        order.setShippedDate(orderResource.getShippedDate());
        order.setOrderStatus(orderResource.getOrderStatus());
        order.setOrderType(orderResource.getOrderType());
        order.setCurrency(orderResource.getCurrency());
        order.setDeliveryAddress(this.setDeleveryAddress(orderResource, cart));
        //for promocode 
        order.setPromoCodeAmount(cart.getPromoCodeAmount());
        order.setPromoCode(cart.getPromoCode());
        order.setTotalOrderPriceWithPromocode(cart.getTotalOrderPriceWithPromocode());
        return order;
    }

    private void setOrderDetails(Order order, List<CartDetails> cartDetails) {
        List<OrderDetails> convertedDetails = ofNullable(cartDetails).orElse(Collections.emptyList())
                .stream()
                .map(detail -> convertToOrderDetail(detail))
                .collect(Collectors.toList());
        order.setOrderDetails(convertedDetails);
    }

    private OrderDetails convertToOrderDetail(CartDetails detail) {
        OrderDetails orderDetails = new OrderDetails();
        orderDetails.setProduct(detail.getProduct());
        orderDetails.setPackagingPrice(detail.getPackagingPrice());
        orderDetails.setPricePerUnit(detail.getPricePerUnit());
        orderDetails.setQuantity(detail.getQuantity());
        orderDetails.setTotalPrice(detail.getTotalPrice());
        return orderDetails;
    }

    /*private void setProduct(OrderDetails orderDetails, Integer productId) {
        Product product = productService.getProduct(productId)
                .orElseThrow(() -> new EntityNotFoundException("Product", productId));
        orderDetails.setProduct(product);
    }*/

    private void setCustomer(Order order, Integer customerId) {
        Customer customer = customerService.getCustomer(customerId)
                .orElseThrow(() ->
                        new EntityNotFoundException("Customer", customerId));
        order.setCustomer(customer);
    }
    
    
    private CustomerAddress setDeleveryAddress(OrderResource orderResource, Cart cart)
    {
    	String houseNo;
    	if(orderResource.getCustomerAddress().getPincode() != null)
    	{
    		houseNo = orderResource.getCustomerAddress().getPincode();
    	}else
    	{
    		houseNo = "Not Provided";
    	}
    	String landmark = "House No/Area Name : "+houseNo+ " | Landmark : " +orderResource.getCustomerAddress().getNearestLandmark();
    	CustomerAddress customerAddress = new CustomerAddress();
    	customerAddress.setAddressLine(cart.getDeliveryAddress().getAddressLine());
    	customerAddress.setAddressLine_2(cart.getDeliveryAddress().getAddressLine());
    	customerAddress.setAddressName(cart.getDeliveryAddress().getAddressName());
    	customerAddress.setLatitude(cart.getDeliveryAddress().getLatitude());
    	customerAddress.setLongitude(cart.getDeliveryAddress().getLongitude());
    	customerAddress.setNearestLandmark(landmark);
    	customerAddress.setCity("Bangalore");
    	customerAddress.setState("Karnataka");
    	customerAddress.setPincode(cart.getDeliveryAddress().getPincode());
		return customerAddress;
    }
}
