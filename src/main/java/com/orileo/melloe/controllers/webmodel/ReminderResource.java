package com.orileo.melloe.controllers.webmodel;

import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Setter
@Getter
@EqualsAndHashCode
@ToString
public class ReminderResource {

    @ApiModelProperty(notes = "Reminder Date and Time")
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date reminderDate;

    @ApiModelProperty(notes = "Reminder Text")
    @NotNull
    @Size(max = 500)
    private String reminderText;
}
