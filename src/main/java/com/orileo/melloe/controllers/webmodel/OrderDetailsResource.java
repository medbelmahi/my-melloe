package com.orileo.melloe.controllers.webmodel;

import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Setter
@Getter
@EqualsAndHashCode
@ToString
public class OrderDetailsResource {
    @ApiModelProperty(notes = "Order's product ID")
    private Integer productId;

    @ApiModelProperty(notes = "Quantity of the Product")
    @NotNull
    private int quantity;

    @ApiModelProperty(notes = "Actual Unit Price of the Product")
    @NotNull
    private Double pricePerUnit;
}
