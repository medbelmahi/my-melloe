package com.orileo.melloe.controllers.webmodel;

import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Setter
@Getter
@EqualsAndHashCode
@ToString
public class CartDetailsResource {
    @ApiModelProperty(notes = "Cart's product ID")
    private Integer productId;

    @ApiModelProperty(notes = "Quantity of the Product")
    @NotNull
    private int quantity;

    @ApiModelProperty(notes = "Actual Unit Price of the Product")
    @NotNull
    private Double pricePerUnit;

    @ApiModelProperty(notes = "Total Price of the Product in the Cart")
    @NotNull
    private Double totalPrice;
}
