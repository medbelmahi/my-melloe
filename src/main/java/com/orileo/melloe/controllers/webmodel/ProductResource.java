package com.orileo.melloe.controllers.webmodel;

import com.orileo.melloe.model.product.ProductPrice;
import com.orileo.melloe.model.product.ProductStatus;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

@Setter
@Getter
@EqualsAndHashCode
@ToString
public class ProductResource implements Serializable {
    @ApiModelProperty(notes = "Name of the product", required = true, example = "Cupcake")
    @NotNull
    @Size(max = 50)
    private String name;

    @ApiModelProperty(notes = "Product's description", required = true, example = "Description")
    @NotNull
    @Size(max = 100)
    private String description;

    @ApiModelProperty(notes = "The URL to the product's picture")
    private String picture;

    @ApiModelProperty(notes = "Flag that shows if product is discounted", required = true)
    @NotNull
    private boolean isDiscounted;

    @ApiModelProperty(notes = "Flag that shows if product is a vegetarian", required = true)
    @NotNull
    private boolean isVegetarian;

    @ApiModelProperty(notes = "Count of product available", required = true)
    @NotNull
    private int productCount;


    @ApiModelProperty(notes = "VendorID", required = true)
    @NotNull
    private int vendorID;

    @ApiModelProperty(notes = "The availability status of the product", required = true,
            example = "AVAILABLE")
    @NotNull
    private ProductStatus productStatus;

    @ApiModelProperty(notes = "Product's categories", dataType = "List<Integer>",
            required = true, example = "1")
    private List<Integer> categoryIDs;

    @ApiModelProperty(notes = "Product's subcategories", dataType = "List<Integer>",
            required = true, example = "1")
    private List<Integer> subcategoryIDs;

    @ApiModelProperty(notes = "Product's price")
    private ProductPrice productPrice;
}
