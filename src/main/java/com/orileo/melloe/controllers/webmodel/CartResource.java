package com.orileo.melloe.controllers.webmodel;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@EqualsAndHashCode
@ToString
public class CartResource {

    @ApiModelProperty(notes = "Created Date")
    @NotNull
    private Date createdDate;

    @ApiModelProperty(notes = "Date of the Last Update")
    private Date lastUpdated;

    @ApiModelProperty(notes = "Cart Details on Individual Products")
    @NotNull
    private List<CartDetailsResource> cartDetails;

}
