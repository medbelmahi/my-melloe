package com.orileo.melloe.controllers.webmodel;

import com.orileo.melloe.model.customer.CustomerAddress;
import com.orileo.melloe.model.login.MasterLogin;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.util.List;

@Setter
@Getter
@EqualsAndHashCode
@ToString
public class CustomerResource {

    @ApiModelProperty(notes = "The Level of the Customer")
    private Integer levelId;

    @ApiModelProperty(notes = "Customer first Name", required = true)
    @NotNull
    private String firstName;

    @ApiModelProperty(notes = " Customer last name", required = true)
    @NotNull
    private String lastName;

    @ApiModelProperty(notes = "Customer phone number")
    @NotNull
    private String phoneNumber;

    @ApiModelProperty(notes = "Customer Login")
    @NotNull
    private MasterLogin userId;

    @ApiModelProperty(notes = "The addresses of the Customer")
    @NotNull
    private List<CustomerAddress> address;
}
