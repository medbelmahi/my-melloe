package com.orileo.melloe.controllers.webmodel;

import static java.util.Optional.ofNullable;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.orileo.melloe.model.cart.Cart;
import com.orileo.melloe.model.cart.CartDetails;
import com.orileo.melloe.model.customer.Customer;
import com.orileo.melloe.model.exceptions.EntityNotFoundException;
import com.orileo.melloe.model.product.Product;
import com.orileo.melloe.servicesinterface.CustomerService;
import com.orileo.melloe.servicesinterface.ProductService;

@Component
public class CartResourceConverter {

	private final CustomerService customerService;
    private final ProductService productService;

    @Autowired
    public CartResourceConverter(CustomerService customerService,
                                  ProductService productService) {
        this.customerService = customerService;
        this.productService = productService;
    }

    public void convertToCart(Cart cart, @NotNull CartResource cartResource) {
        setCartDetails(cart, cartResource.getCartDetails());
    }

    private void setCartDetails(Cart cart, List<CartDetailsResource> orderDetails) {
        List<CartDetails> convertedDetails = ofNullable(orderDetails).orElse(Collections.emptyList())
                .stream()
                .map(detail -> convertToCartDetail(detail))
                .collect(Collectors.toList());
        cart.setCartDetails(convertedDetails);
    }

    private CartDetails convertToCartDetail(CartDetailsResource detail) {
        CartDetails cartDetails = new CartDetails();
        setProduct(cartDetails, detail.getProductId());
        cartDetails.setPricePerUnit(detail.getPricePerUnit());
        cartDetails.setQuantity(detail.getQuantity());
        cartDetails.setTotalPrice(detail.getTotalPrice());
        return cartDetails;
    }

    private void setProduct(CartDetails cartDetails, Integer productId) {
        Product product = productService.getProduct(productId)
                .orElseThrow(() -> new EntityNotFoundException("Product", productId));
        cartDetails.setProduct(product);
    }
    
    public void setCustomer(Cart cart, Integer customerId) {
        Customer customer = customerService.getCustomer(customerId)
                .orElseThrow(() ->
                        new EntityNotFoundException("Customer", customerId));
        cart.setCustomer(customer);
        customer.setCart(cart);
        customerService.saveCustomer(customer);
    }
}
