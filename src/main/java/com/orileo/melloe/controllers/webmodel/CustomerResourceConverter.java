package com.orileo.melloe.controllers.webmodel;

import com.orileo.melloe.model.customer.Customer;
import com.orileo.melloe.model.customer.CustomerAddress;
import com.orileo.melloe.model.customer.CustomerLevel;
import com.orileo.melloe.model.customer.CustomerWallet;
import com.orileo.melloe.model.exceptions.EntityNotFoundException;
import com.orileo.melloe.servicesinterface.CustomerLevelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.List;

@Component
public class CustomerResourceConverter {
    private final CustomerLevelService customerLevelService;

    @Autowired
    public CustomerResourceConverter(CustomerLevelService customerLevelService) {
        this.customerLevelService = customerLevelService;
    }

    public Customer convertToService(CustomerResource customerResource) {
        Customer customer = new Customer();
        customer.setFirstName(customerResource.getFirstName());
        customer.setLastName(customerResource.getLastName());
        customer.setPhoneNumber(customerResource.getPhoneNumber());
        final List<CustomerAddress> addresses = customerResource.getAddress();
        try {
            final CustomerAddress customerAddress = addresses.get(0);
            customerAddress.setLatitude(customerAddress.getLatitude().length() > 15 ?
                    customerAddress.getLatitude().substring(0, 15) : customerAddress.getLatitude());
            customerAddress.setLongitude(customerAddress.getLongitude().length() > 15 ?
                    customerAddress.getLongitude().substring(0, 15) : customerAddress.getLongitude());
        } catch (Exception e) {
            e.printStackTrace();
        }
        customer.setAddress(addresses);
        customer.setUserId(customerResource.getUserId());
        customer.setCustomerWallet(new CustomerWallet());
        setCustomerLevel(customer.getCustomerWallet(), customerResource.getLevelId());
        return customer;
    }

    public Customer convertToService(@NotNull Customer customer,
                                     CustomerResource customerResource) {
        setCustomerLevel(customer.getCustomerWallet(), customerResource.getLevelId());
        customer.setFirstName(customerResource.getFirstName());
        customer.setLastName(customerResource.getLastName());
        customer.setPhoneNumber(customerResource.getPhoneNumber());
        customer.setAddress(customerResource.getAddress());
        return customer;
    }

    private void setCustomerLevel(CustomerWallet customerWallet, Integer levelId) {
        if (levelId != null && levelId != 0) {
            CustomerLevel level = customerLevelService.getCustomerLevel(levelId)
                    .orElseThrow(() ->
                            new EntityNotFoundException("Customer level", levelId));
            customerWallet.setLevel(level);
        }
        ;
    }
}
