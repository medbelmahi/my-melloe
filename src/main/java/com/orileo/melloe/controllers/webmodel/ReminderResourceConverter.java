package com.orileo.melloe.controllers.webmodel;

import com.orileo.melloe.model.customer.Customer;
import com.orileo.melloe.model.customer.Reminder;
import com.orileo.melloe.model.exceptions.EntityNotFoundException;
import com.orileo.melloe.servicesinterface.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.Optional;

@Slf4j
@Component
public class ReminderResourceConverter {
    private final CustomerService customerService;

    @Autowired
    public ReminderResourceConverter(CustomerService customerService) {
        this.customerService = customerService;
    }

    public Reminder convertToService(Integer customerID,
                                     @NotNull ReminderResource reminderResource) {
        Optional<Customer> customer = customerService.getCustomer(customerID);
        if (!customer.isPresent()) {
            log.warn("Customer with ID {} not found ", customerID);
            throw new EntityNotFoundException("Customer ", customerID);
        } else {
            Reminder reminder = createReminder(reminderResource);
            reminder.setCustomer(customer.get());
            return reminder;
        }
    }

    public Reminder convertToService(@NotNull Integer id,
                                     @NotNull Reminder persistedReminder,
                                     @NotNull ReminderResource reminderResource) {
        Reminder reminder = createReminder(reminderResource);
        reminder.setCustomer(persistedReminder.getCustomer());
        reminder.setId(id);
        return reminder;
    }

    private Reminder createReminder(ReminderResource reminderResource) {
        Reminder reminder = new Reminder();
        reminder.setReminderDate(reminderResource.getReminderDate());
        reminder.setReminderText(reminderResource.getReminderText());
        return reminder;
    }

}

