package com.orileo.melloe.controllers.webmodel;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

import com.orileo.melloe.model.customer.CustomerAddress;
import com.orileo.melloe.model.order.OrderStatus;

import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@EqualsAndHashCode
@ToString
public class OrderResource {

    @ApiModelProperty(notes = "Created Date")
    @NotNull
    private Date createdDate;

    @ApiModelProperty(notes = "Date of the Last Update")
    private Date lastUpdated;

    @ApiModelProperty(notes = "Date where the Order is Required")
    @NotNull
    private Date requiredDate;

    @ApiModelProperty(notes = "Shipment Date")
    private Date shippedDate;

    @ApiModelProperty(notes = "Status of Order")
    @NotNull
    private OrderStatus orderStatus;

    @ApiModelProperty(notes = "Order Details on Individual Products")
    @NotNull
    private List<OrderDetailsResource> orderDetails;
    
    @ApiModelProperty(notes = "Order Type")
    @NotNull
	private String orderType;

    @ApiModelProperty(notes = "Payment id (null if Order Type is COD)")
    @NotNull
	private String paymentId;

    @ApiModelProperty(notes = "Delivery address of customer for this product")
    @NotNull
    private CustomerAddress customerAddress;

    @ApiModelProperty(notes = "Currency of the order")
    @NotNull
    private String currency;
}
