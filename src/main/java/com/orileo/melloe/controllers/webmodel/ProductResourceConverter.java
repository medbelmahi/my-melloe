package com.orileo.melloe.controllers.webmodel;

import com.orileo.melloe.model.category.Category;
import com.orileo.melloe.model.category.SubCategory;
import com.orileo.melloe.model.exceptions.EntityNotFoundException;
import com.orileo.melloe.model.product.Product;
import com.orileo.melloe.model.vendor.Vendor;
import com.orileo.melloe.servicesinterface.CategoryService;
import com.orileo.melloe.servicesinterface.ProductService;
import com.orileo.melloe.servicesinterface.SubCategoryService;
import com.orileo.melloe.servicesinterface.VendorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Optional.ofNullable;

@Component
public class ProductResourceConverter {
	private final ProductService productService;
    private final VendorService vendorService;
    private final CategoryService categoryService;
    private final SubCategoryService subCategoryService;

	@Autowired
	public ProductResourceConverter(ProductService productService, VendorService vendorService,
			CategoryService categoryService, SubCategoryService subCategoryService) {
		this.productService = productService;
		this.vendorService = vendorService;
		this.categoryService = categoryService;
		this.subCategoryService = subCategoryService;
	}

    public Product convertToService(Integer id, @NotNull ProductResource productResource, Integer vendorID) {
        Product product = getProduct(id);
        product.setName(productResource.getName());
        product.setDescription(productResource.getDescription());
        product.setPicture(productResource.getPicture());
        product.setDiscounted(productResource.isDiscounted());
        product.setVegetarian(productResource.isVegetarian());
        product.setProductCount(productResource.getProductCount());
        product.setProductPrice(productResource.getProductPrice());
        product.setProductStatus(productResource.getProductStatus());
        setVendor(product, vendorID);
        setCategories(product, productResource.getCategoryIDs());
        setSubCategories(product, productResource.getSubcategoryIDs());
        return product;
    }

    private void setSubCategories(Product product, List<Integer> subcategoryIDs) {
        Set<SubCategory> subCategories = ofNullable(subcategoryIDs).orElse(Collections.emptyList())
                .stream()
                .map(catId -> subCategoryService.getSubCategory(catId)
                        .orElseThrow(() ->
                                new EntityNotFoundException("Subcategory", catId)))
            .collect(Collectors.toSet());
        product.setSubcategory(subCategories);
    }

    private void setCategories(Product product, List<Integer> categoryIDs) {
        List<Category> categories = ofNullable(categoryIDs).orElse(Collections.emptyList())
                .stream()
                .map(catId -> categoryService.getCategory(catId)
                        .orElseThrow(() ->
                                new EntityNotFoundException("Category", catId)))
                .collect(Collectors.toList());
        product.setCategory(categories);
    }

    private void setVendor(Product product, Integer vendorID) {
        Vendor vendor = vendorService.getVendor(vendorID).
                orElseThrow(() ->
                        new EntityNotFoundException("Vendor", vendorID));
        product.setVendor(vendor);

    }

	public Product getProduct(Integer id) {
		Product product = null;
		if (id == null) {
			product = new Product();
		} else {
			Optional<Product> oProduct = productService.getProduct(id);
			if (oProduct.isPresent()) {
				product = oProduct.get();
				product.setId(id);
			}
		}
		return product;
	}
}
