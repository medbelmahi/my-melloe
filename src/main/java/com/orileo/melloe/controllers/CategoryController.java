package com.orileo.melloe.controllers;

import com.orileo.melloe.model.category.Category;
import com.orileo.melloe.model.exceptions.EntityNotFoundException;
import com.orileo.melloe.servicesinterface.CategoryService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Slf4j
@Controller
public class CategoryController extends BaseControllerImpl {
    private final CategoryService categoryService;

    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @ApiOperation(value = "List Categories")
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "No categories defined"),
            @ApiResponse(code = 200, message = "Categories are found")})
    @RequestMapping(value = "/open/category", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Category>> list() {
        List<Category> categories = categoryService.listAllCategories();
        if (CollectionUtils.isEmpty(categories)) {
            throw new EntityNotFoundException("Categories");
        }
        return new ResponseEntity<>(categories, HttpStatus.OK);
    }

    @ApiOperation(value = "Find Category by ID", response = Category.class)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Category not found"),
            @ApiResponse(code = 200, message = "Category succesfully returned")})
    @RequestMapping(value = "/open/category/{id}", method = RequestMethod.GET)
    public ResponseEntity<Category> getCategory(@PathVariable int id) {
        Optional<Category> category = categoryService.getCategory(id);
        if (!category.isPresent()) {
            throw new EntityNotFoundException(Category.class.getSimpleName(), id);
        }
        return new ResponseEntity<>(category.get(), HttpStatus.OK);
    }

    @ApiOperation(value = "Create Category")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Category succesfully created")})
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('VENDOR')")
    @RequestMapping(value = "/category", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Category> create(@Valid @RequestBody Category category,
                                           UriComponentsBuilder ucBuilder) {
        categoryService.saveCategory(category);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/category/{id}").buildAndExpand(category.getId()).toUri());
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @ApiOperation(value = "Update Category")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Category succesfully updated"),
            @ApiResponse(code = 404, message = "Category not found")})
    @PreAuthorize("hasPermission('ADMIN')")
    @RequestMapping(value = "/category/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Category> update(
            @PathVariable int id,
            @Valid @RequestBody Category category) {
        Optional<Category> persistedCategory = categoryService.getCategory(id);
        if (!persistedCategory.isPresent()) {
            log.warn("Category with ID {} not found", id);
            throw new EntityNotFoundException(Category.class.getSimpleName(), id);
        }
        category.setId(id);
        categoryService.saveCategory(category);
        return new ResponseEntity<>(category, HttpStatus.OK);
    }

    @ApiOperation(value = "Delete Category")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Category succesfully deleted"),
            @ApiResponse(code = 404, message = "Category not found")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/category/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Category> delete(
            @PathVariable int id) {
        if (!categoryService.isCategoryExists(id)) {
            log.warn("Unable to delete Category. Category with ID : {} not found", id);
            throw new EntityNotFoundException(Category.class.getSimpleName(), id);
        }
        categoryService.deleteCategory(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
