package com.orileo.melloe.controllers;

import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import com.orileo.melloe.servicesinterface.GoogleMapService;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

import com.orileo.melloe.controllers.csv.product.ProductCsvConverter;
import com.orileo.melloe.controllers.webmodel.ProductResource;
import com.orileo.melloe.controllers.webmodel.ProductResourceConverter;
import com.orileo.melloe.model.exceptions.EntityNotFoundException;
import com.orileo.melloe.model.product.Product;
import com.orileo.melloe.model.product.ProductDiscount;
import com.orileo.melloe.model.vendor.Vendor;
import com.orileo.melloe.security.IEnrichedUserDetails;
import com.orileo.melloe.servicesinterface.ImageService;
import com.orileo.melloe.servicesinterface.ProductDiscountService;
import com.orileo.melloe.servicesinterface.ProductService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class ProductController extends BaseControllerImpl {
	public static final String PRODUCT = "product";
	private final ProductService productService;
	private final ProductResourceConverter resourceConverter;
    private final ProductCsvConverter csvConverter;
    private final ProductDiscountService productDiscountService;
	private final ImageService imageService;
	private final GoogleMapService googleMapService;

	@Autowired
    public ProductController(ProductService productService,
							 ProductResourceConverter resourceConverter,
							 ProductCsvConverter csvConverter,
							 ProductDiscountService productDiscountService, ImageService imageService, GoogleMapService googleMapService) {
		this.productService = productService;
		this.resourceConverter = resourceConverter;
        this.csvConverter = csvConverter;
        this.productDiscountService = productDiscountService;
		this.imageService = imageService;
		this.googleMapService = googleMapService;
	}

	@ApiOperation(value = "Find all Products", response = Product.class, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = {
			@ApiResponse(code = 404, message = "No products were found"),
			@ApiResponse(code = 200, message = "Products were found")})
	@RequestMapping(value = "/open/product", method = RequestMethod.GET)
	public ResponseEntity<List<Product>> list(
			@RequestParam(required = false) List<Integer> categoryID,
			@RequestParam(required = false) List<Integer> subCategoryID,
			@RequestParam(required = false) List<Integer> vendorID) {
		List<Product> product = productService.listProducts(categoryID, subCategoryID, vendorID);
		if (CollectionUtils.isEmpty(product)) {
			throw new EntityNotFoundException("Products");
		}
		return new ResponseEntity<>(product, HttpStatus.OK);
    }

    @ApiOperation(value = "Find all Products by vendors and category", response = Product.class, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = {
			@ApiResponse(code = 404, message = "No products were found"),
			@ApiResponse(code = 200, message = "Products were found")})
	@RequestMapping(value = "/open/productByVendorsAndCat", method = RequestMethod.GET)
	public ResponseEntity<List<Product>> productByVendorsAndCat(
			@RequestParam(required = false) Integer categoryID,
			@RequestParam(required = false) List<Integer> vendorID) {
		List<Product> product = productService.listProductsByVendorsAndCat(categoryID, vendorID);
		if (CollectionUtils.isEmpty(product)) {
			throw new EntityNotFoundException("Products");
		}
		return new ResponseEntity<>(product, HttpStatus.OK);
    }

	@ApiOperation(value = "Find product by id", response = Vendor.class,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = {
			@ApiResponse(code = 404, message = "No products found"),
			@ApiResponse(code = 200, message = "Products succesfully returned")})
	@RequestMapping(value = "/open/product/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Product> getById(
            @PathVariable int id) {
		Optional<Product> product = productService.getProduct(id);
		if (!product.isPresent()) {
			log.warn("Failed to find product with ID {}", id);
			throw new EntityNotFoundException(PRODUCT, id);
		}
		Product productToReturn = product.get();
		String link = imageService.generateLink("product-" + productToReturn.getId());
		productToReturn.setPicture(link);
		return new ResponseEntity<>(productToReturn, HttpStatus.OK);
	}

	@ApiOperation(value = "Create product", response = ProductResource.class,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Product succesfully created")})
    @PreAuthorize("hasAuthority('VENDOR')or hasAuthority('ADMIN')")
    @RequestMapping(value = "/product", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> create(@RequestBody @Valid ProductResource productResource,
                                       UriComponentsBuilder ucBuilder) {
        Product product = resourceConverter.convertToService(null, productResource,
                getVendorId(productResource.getVendorID()));
        productService.saveProduct(product);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/product/{id}").buildAndExpand(product.getId()).toUri());
		log.info("Created Product Successfully with Name : " + productResource.getName());
		return new ResponseEntity<>(product.getId(), headers, HttpStatus.CREATED);
	}

    private Integer getVendorId(int vendorID) {
        return isCurrentUserAdmin() ? vendorID : getVendorId();
    }

    @ApiOperation(value = "Upload Products",
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Products successfully uploaded")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/product/import",
			method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
	public void upload(@RequestParam("file") MultipartFile file) throws Exception {
		final Reader reader = new InputStreamReader(file.getInputStream(), "UTF-8");
        final CSVParser parser = new CSVParser(reader, CSVFormat.DEFAULT.withHeader());
        csvConverter.validateFile(parser.getHeaderMap().keySet());
        parser.getRecords().forEach(this::parseAndCreateProduct);
    }

    private void parseAndCreateProduct(CSVRecord record) {
        Product product = csvConverter.convertFromCsv(record);
        List<ProductDiscount> productDiscounts = product.getProductDiscounts();
        productService.saveProduct(product);
        productDiscounts.forEach(disc -> {
            disc.setProduct(product);
            productDiscountService.saveProductDiscount(disc);
        });
    }

	@ApiOperation(value = "Update product", response = Vendor.class,
			consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = {
			@ApiResponse(code = 404, message = "Product not found"),
			@ApiResponse(code = 200, message = "Product succesfully returned")})
    @PreAuthorize("hasAuthority('VENDOR') or hasAuthority('ADMIN')")
    @RequestMapping(value = "/product/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Product> update(
            @PathVariable Integer id, @RequestBody @Valid ProductResource productResource) {
        Integer vendorID = getVendorId(productResource.getVendorID());
        if (!productService.findProduct(id, vendorID).isPresent()) {
            log.warn("Product with ID: {} not found", id);
			throw new EntityNotFoundException(PRODUCT, id);
		}
        Product product = resourceConverter.convertToService(id, productResource, vendorID);
        Product persistedProduct = productService
				.saveProduct(product);
		return new ResponseEntity<>(persistedProduct, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Update Only The Status Of The Product", response = Vendor.class,
			consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = {
			@ApiResponse(code = 404, message = "Product not found"),
			@ApiResponse(code = 200, message = "Product succesfully returned")})
    @PreAuthorize("hasAuthority('VENDOR') or hasAuthority('ADMIN')")
    @RequestMapping(value = "/updateproductstatus/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Product> updateProductStatus(
            @PathVariable Integer id, @RequestBody @Valid ProductResource productResource) {
		Product prePersitedProduct = null;
		if(productResource != null && id > 0)
		{
			Optional<Product> prePersitedProductObject = productService.getProduct(id);
			prePersitedProduct = prePersitedProductObject.get();
			prePersitedProduct.setProductStatus(productResource.getProductStatus());
		}
        Product persistedProduct = productService.saveProduct(prePersitedProduct);
		return new ResponseEntity<>(persistedProduct, HttpStatus.OK);
	}
	

	@ApiOperation(value = "Delete product", response = Vendor.class,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = {
			@ApiResponse(code = 404, message = "Product not found"),
			@ApiResponse(code = 204, message = "Product succesfully deleted")})
    @PreAuthorize("hasAuthority('VENDOR') or hasAuthority('ADMIN')")
    @RequestMapping(value="/product/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Product> delete(@PathVariable int id) {
        if (!productService.findProduct(id, getVendorId()).isPresent()) {
            log.warn("Unable to delete Product. Product with ID : {} not found", id);
			throw new EntityNotFoundException(PRODUCT, id);
		}
		productService.deleteProduct(id);
		imageService.deleteFile("product-" + id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@ApiOperation(value = "Find all Products for a vendor", response = Product.class, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = { @ApiResponse(code = 404, message = "No products were found"),
			@ApiResponse(code = 200, message = "Products were returned") })
	@PreAuthorize("hasAuthority('ADMIN') or hasAuthority('VENDOR')")
	@RequestMapping(value = "/products/{vendorId}", method = RequestMethod.GET)
	public ResponseEntity<List<Product>> getProductsByVendorId(@PathVariable(required=false) Optional<Integer> vendorId) {
		IEnrichedUserDetails userDetails = getCurrentUserDetails();
		int vendor = 0;
		if(userDetails.getAuthorities().contains(new SimpleGrantedAuthority("VENDOR"))) {
			log.debug("VendorId {} request products for vendor id {}", vendorId.get(), userDetails.getVendorId());
			vendor = userDetails.getVendorId();
		} else if(!vendorId.isPresent()) {
			log.info("product list for a vendor requested without passing vendor id");
			new ResponseEntity<>("Please pass the mendatory parameter vendorId", HttpStatus.NOT_FOUND);
		} else {
			vendor = vendorId.get();
		}
		
		List<Product> product = productService.getProductsByVendorId(vendor);
		if (CollectionUtils.isEmpty(product)) {
			throw new EntityNotFoundException("Products");
		}
		return new ResponseEntity<>(product, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Get all Products for one or more vendors", response = Product.class, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = {
			@ApiResponse(code = 404, message = "No products were found for given vendor id"),
			@ApiResponse(code = 200, message = "Products were found"),
			@ApiResponse(code = 403, message = "No access to view the functionality")})
	@RequestMapping(value = "/open/vendorsProduct", method = RequestMethod.GET)
	public @ResponseBody List<Product> getProductOfVendors(@RequestParam(required = false) List<Integer> vendorID)
	{
		List<Product> product = productService.vendorsProduct(vendorID);
		if (CollectionUtils.isEmpty(product)) 
		{
			throw new EntityNotFoundException("Products");
		}
		return product;
    }

	@ApiOperation(value = "Get all Products for one or more vendors Under Radius X", response = Product.class, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = {
			@ApiResponse(code = 404, message = "No products were found under Radius X"),
			@ApiResponse(code = 200, message = "Products were found"),
			@ApiResponse(code = 403, message = "No access to view the functionality")})
	@RequestMapping(value = "/open/vendorsProduct/underRadius", method = RequestMethod.GET)
	public ResponseEntity<List<Product>> getProductOfVendorsUnderRadius(@RequestParam(name = "lat") double lat, @RequestParam(name = "lng") double lng)
	{
		List<Product> product = googleMapService.getProductOfVendorsUnderRadius(lat, lng);
		if (CollectionUtils.isEmpty(product))
		{
			throw new EntityNotFoundException("Products");
		}
		return new ResponseEntity<>(product, HttpStatus.OK);
	}

	@ApiOperation(value = "Get all Products for Category and one or more vendors Under Radius X", response = Product.class, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = {
			@ApiResponse(code = 404, message = "No products were found under Radius X"),
			@ApiResponse(code = 200, message = "Products were found"),
			@ApiResponse(code = 403, message = "No access to view the functionality")})
	@RequestMapping(value = "/open/categoryVendorsProduct/underRadius", method = RequestMethod.GET)
	public ResponseEntity<List<Product>> getProductOfVendorsAndCategoryUnderRadius(@RequestParam(name = "lat") double lat, @RequestParam(name = "lng") double lng, @RequestParam(name = "catId") Integer catId)
	{
		List<Product> product = googleMapService.getProductOfCategoryAndVendorsUnderRadius(lat, lng, catId);
		if (CollectionUtils.isEmpty(product))
		{
			throw new EntityNotFoundException("Products");
		}
		return new ResponseEntity<>(product, HttpStatus.OK);
	}

}
