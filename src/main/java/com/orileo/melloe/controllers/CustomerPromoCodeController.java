package com.orileo.melloe.controllers;

import com.orileo.melloe.controllers.csv.customerpromocode.CustomerPromocodeCsvConverter;
import com.orileo.melloe.model.customer.Customer;
import com.orileo.melloe.model.customer.CustomerPromoCode;
import com.orileo.melloe.model.exceptions.EntityNotFoundException;
import com.orileo.melloe.model.promocode.PromoCode;
import com.orileo.melloe.servicesinterface.CustomerPromoCodeService;
import com.orileo.melloe.servicesinterface.CustomerService;
import com.orileo.melloe.servicesinterface.PromocodeService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriComponentsBuilder;
import javax.validation.Valid;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;
import java.util.Optional;

@Slf4j
@Controller
public class CustomerPromoCodeController extends BaseControllerImpl {
    public static final String CUSTOMER_PROMO_CODE = "Customer promo code ";
    private final CustomerPromoCodeService customerPromoCodeService;
    private final CustomerService customerService;
    private final PromocodeService promocodeService;
    private final CustomerPromocodeCsvConverter resourceConverter;

    @Autowired
    public CustomerPromoCodeController(CustomerPromoCodeService customerPromoCodeService,
                                       CustomerService customerService,
                                       PromocodeService promocodeService,
                                       CustomerPromocodeCsvConverter resourceConverter) {
        this.customerPromoCodeService = customerPromoCodeService;
        this.customerService = customerService;
        this.promocodeService = promocodeService;
        this.resourceConverter = resourceConverter;
    }

    @ApiOperation(value = "Find all Customer Promocodes", response = PromoCode.class,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "No Customer Promocodes found"),
            @ApiResponse(code = 200, message = "Customer Promocodes succesfully returned")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/customerpromocode", method = RequestMethod.GET)
    public ResponseEntity<List<CustomerPromoCode>> listAllPromoCodes(
            @RequestParam(required = false) List<Integer> customerID,
            @RequestParam(required = false) List<String> promocode) {
        List<CustomerPromoCode> customerPromoCodes = customerPromoCodeService
                .listCustomerPromocodes(customerID, promocode);
        if (CollectionUtils.isEmpty(customerPromoCodes)) {
            log.warn("There are no Customer Promocodes found");
            throw new EntityNotFoundException("Customer Promocodes");
        }
        return new ResponseEntity<>(customerPromoCodes, HttpStatus.OK);
    }

  @ApiOperation(value = "Find Customer Promocode by ID", response = PromoCode.class,
      produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiResponses(value = {
      @ApiResponse(code = 404, message = "No Customer Promocodes found"),
      @ApiResponse(code = 200, message = "Customer Promocodes succesfully returned")})
  @PreAuthorize("hasAuthority('ADMIN')")
  @RequestMapping(value = "/customerpromocode/{id}", method = RequestMethod.GET)
  public ResponseEntity<CustomerPromoCode> listPromocodeByID(
      @PathVariable Integer id) {
    Optional<CustomerPromoCode> customerPromoCodes = customerPromoCodeService.getCustomerPromocode(id);
    if (!customerPromoCodes.isPresent()) {
      log.warn("There are no Customer Promocodes found with id {}", id);
      throw new EntityNotFoundException("Customer Promocodes");
    }
    return new ResponseEntity<>(customerPromoCodes.get(), HttpStatus.OK);
  }
    @ApiOperation(value = "Upload Customer Promocodes",
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Customer or Promocode not found"),
            @ApiResponse(code = 200, message = "Customer Promocodes successfully uploaded")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/customerpromocode/import",
            method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public void upload(@RequestParam("file") MultipartFile file,
                       RedirectAttributes redirectAttributes) throws Exception {
        final Reader reader = new InputStreamReader(file.getInputStream(), "UTF-8");
        final CSVParser parser = new CSVParser(reader, CSVFormat.DEFAULT.withHeader());
        resourceConverter.validateFile(parser.getHeaderMap().keySet());
        parser.getRecords().stream().forEach(this::parseAndCreateCustomerPromocode);
    }

    private void parseAndCreateCustomerPromocode(CSVRecord record) {
        CustomerPromocodeCsvConverter.ParsedCustomerPromocode parsedPromocode =
                resourceConverter.convertFromCsv(record);
        createCustomerPromocode(parsedPromocode.getCustomerID(),
                parsedPromocode.getPcode(),
                parsedPromocode.getCustomerPromoCode());
    }


    @ApiOperation(value = "Create Customer Promocode", response = CustomerPromoCode.class, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Customer or Promocode not found"),
            @ApiResponse(code = 200, message = "Customer Promocode successfully created")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/customer/{customerID}/promocode/{pcode}/customerpromocode",
            method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Void> create(
            @PathVariable Integer customerID,
            @PathVariable String pcode,
            @Valid @RequestBody CustomerPromoCode customerPromoCode,
            UriComponentsBuilder ucBuilder) {
        CustomerPromoCode persisted = createCustomerPromocode(customerID, pcode, customerPromoCode);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/customerpromocode/{id}")
                .buildAndExpand(persisted.getId()).toUri());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    private CustomerPromoCode createCustomerPromocode(Integer customerID, String pcode,
                                                      CustomerPromoCode customerPromoCode) {
        Optional<Customer> customer = customerService.getCustomer(customerID);
        if (!customer.isPresent()) {
            log.warn("Unable to find Customer with ID {}", customerID);
            throw new EntityNotFoundException(Customer.class.getSimpleName(), customerID);
        }
        Optional<PromoCode> promoCode = promocodeService.getPromoCode(pcode);
        if (!promoCode.isPresent()) {
            log.warn("Unable to find Promocode with ID {}", promoCode);
            throw new EntityNotFoundException(PromoCode.class.getSimpleName(), pcode);
        }
        customerPromoCode.setCustomer(customer.get());
        customerPromoCode.setPromoCode(promoCode.get());
        return customerPromoCodeService.saveCustomerPromocode(customerPromoCode);
    }

    @ApiOperation(value = "Update Customer Promocode", response = CustomerPromoCode.class,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Customer Promocode not found"),
            @ApiResponse(code = 200, message = "Customer Promocode successfully updated")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/customerpromocode/{id}", method = RequestMethod.PUT)
    public ResponseEntity<CustomerPromoCode> update(
            @PathVariable int id,
            @Valid @RequestBody CustomerPromoCode customerPromoCode) {
        if (!customerPromoCodeService.isCustomerPromocodeExists(id)) {
            log.warn("Unable to find customer promocode ID {}", id);
            throw new EntityNotFoundException(CUSTOMER_PROMO_CODE, id);
        } else {
            customerPromoCode.setId(id);
            customerPromoCodeService.saveCustomerPromocode(customerPromoCode);
        }
        return new ResponseEntity<>(customerPromoCode, HttpStatus.OK);
    }

    @ApiOperation(value = "Delete Customer Promocode",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Customer Promocode not found"),
            @ApiResponse(code = 200, message = "Customer Promocode successfully deleted")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/customerpromocode/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<CustomerPromoCode> delete(
            @PathVariable int id) {
        if (!customerPromoCodeService.isCustomerPromocodeExists(id)) {
            log.warn("Unable to find customer promocode ID {}", id);
            throw new EntityNotFoundException(CUSTOMER_PROMO_CODE, id);
        } else {
            customerPromoCodeService.deleteCustomerPromocode(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
    
    @ApiOperation(value = "check promo code status", response = CustomerPromoCode.class, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "promocode not used"),
            @ApiResponse(code = 200, message = "promocode used")})
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('CUSTOMER')")
    @RequestMapping(value = "/checkPromoCodeStatus", method = RequestMethod.GET)
    public ResponseEntity<List<CustomerPromoCode>> checkPromoCodeStatus(
            @RequestParam(required = true) Integer customerID,
            @RequestParam(required = false) String appId,
            @RequestParam(required = false) String ipAddress,
            @RequestParam(required = true) String promoCode)
    {
        List<CustomerPromoCode> promoCodeStatus = customerPromoCodeService.getCustomerPromoCodeStatus(customerID, promoCode, ipAddress, appId);
        
        if (CollectionUtils.isEmpty(promoCodeStatus)) {
            log.warn("promo code not used by the customer");
            return new ResponseEntity<>(promoCodeStatus, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(promoCodeStatus, HttpStatus.OK);
    }
}