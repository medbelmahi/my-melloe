package com.orileo.melloe.controllers;

import com.google.maps.model.LatLng;
import com.orileo.melloe.controllers.dto.AddToCartInput;
import com.orileo.melloe.controllers.dto.SelectedAddress;
import com.orileo.melloe.controllers.webmodel.CartResource;
import com.orileo.melloe.controllers.webmodel.CartResourceConverter;
import com.orileo.melloe.facade.populator.Populator;
import com.orileo.melloe.model.cart.Cart;
import com.orileo.melloe.model.customer.CustomerAddress;
import com.orileo.melloe.model.exceptions.EntityNotFoundException;
import com.orileo.melloe.model.exceptions.IsNotUnderRadius;
import com.orileo.melloe.model.exceptions.ProductIsNotFromSameVendorException;
import com.orileo.melloe.model.promocode.PromoCode;
import com.orileo.melloe.model.vendor.Address;
import com.orileo.melloe.model.vendor.Vendor;
import com.orileo.melloe.servicesinterface.CartService;
import com.orileo.melloe.servicesinterface.GoogleMapService;
import com.orileo.melloe.servicesinterface.PromocodeService;
import com.orileo.melloe.servicesinterface.SessionService;
import com.orileo.melloe.servicesinterface.VendorService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Slf4j
@Controller
public class CartController extends BaseControllerImpl {
    private final CartService cartService;
    private final PromocodeService promocodeService;
    private final CartResourceConverter resourceConverter;

    private final SessionService sessionService;
    private final GoogleMapService googleMapService;
    private final VendorService vendorService;
    @Resource(name = "cartAddressPopulator")
    private Populator<CustomerAddress, SelectedAddress> cartAddressPopulator;

    @Autowired
    public CartController(CartService cartService,PromocodeService promocodeService,
                          CartResourceConverter resourceConverter, SessionService sessionService, GoogleMapService googleMapService, VendorService vendorService) {
        this.cartService = cartService;
        this.resourceConverter = resourceConverter;
        this.sessionService = sessionService;
        this.googleMapService = googleMapService;
        this.vendorService = vendorService;
        this.promocodeService = promocodeService;
    }

    @ApiOperation(value = "Find cart by ID", response = Cart.class,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "No cart found"),
            @ApiResponse(code = 200, message = "Cart succesfully returned")})
    @RequestMapping(value = "/cart/{id}", method = RequestMethod.GET)
    public ResponseEntity<Cart> getCart(@PathVariable int id) {
        Optional<Cart> cart = cartService.getCart(id, getCustomerId());
        if (!cart.isPresent()) {
            log.warn("Cart with ID :{} not found ", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(cart.get(), HttpStatus.OK);
        }
    }

    @ApiOperation(value = "Create cart", response = CartResource.class,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Cart succesfully created")})
    @PreAuthorize("hasAuthority('CUSTOMER') or hasAuthority('ADMIN')")
    @RequestMapping(value = "/cart", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Void> create(@Valid @RequestBody CartResource cartResource,
                                       UriComponentsBuilder ucBuilder, HttpSession session) {
        Cart cart = sessionService.getSessionCart(session);
        resourceConverter.convertToCart(cart, cartResource);
        cartService.calculateCart(cart);
        cartService.saveCart(cart);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/cart/{id}").buildAndExpand(cart.getId()).toUri());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @ApiOperation(value = "Update Cart", response = CartResource.class,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Cart not found"),
            @ApiResponse(code = 200, message = "Cart succesfully updated")})
    @PreAuthorize("hasAuthority('CUSTOMER')  or hasAuthority('ADMIN')")
    @RequestMapping(value = "/cart/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Cart> update(@PathVariable int id,
                                        @Valid @RequestBody CartResource cartResource, HttpSession session) {
        if (!cartService.isCartExist(id)) {
            log.warn("Failed to find cart with ID {}", id);
            throw new EntityNotFoundException("Cart", id);
        } else {
            Cart cart = sessionService.getSessionCart(session);
            resourceConverter.convertToCart(cart, cartResource);
            cartService.calculateCart(cart);
            cartService.saveCart(cart);
            return new ResponseEntity<>(cart, HttpStatus.OK);
        }
    }

    @ApiOperation(value = "Delete Cart", response = Cart.class,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Cart not found"),
            @ApiResponse(code = 204, message = "Cart succesfully deleted")})
    @PreAuthorize("hasAuthority('CUSTOMER') or hasAuthority('ADMIN')")
    @RequestMapping(value = "/cart/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Cart> delete(@PathVariable int id) {
        if (!cartService.getCart(id, getCustomerId()).isPresent()) {
            log.warn("Failed to find cart with ID {}", id);
            throw new EntityNotFoundException("Cart", id);
        } else {
            cartService.deleteCart(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }
    }


    @ApiOperation(value = "Get cart by session", response = Cart.class,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Cart succesfully created")})
    @PreAuthorize("hasAuthority('IS_AUTHENTICATED_ANONYMOUSLY') or hasAuthority('CUSTOMER') or hasAuthority('ADMIN')")
    @RequestMapping(value = "/sessionCart", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody Cart getOrCreateSessionCart(HttpSession session) {
        Cart cart = sessionService.getSessionCart(session);
        return cart;
    }

    @ApiOperation(value = "Add product to cart", response = Cart.class, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses( value = {@ApiResponse(code = 202, message = "Product has been added to cart"),
                            @ApiResponse(code = 400, message = "Bad Request")})
    @PreAuthorize("hasAuthority('IS_AUTHENTICATED_ANONYMOUSLY') or hasAuthority('CUSTOMER') or hasAuthority('ADMIN')")
    @RequestMapping(value = "/cart/addToCart", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public @ResponseBody Cart addToCart(@RequestParam(value = "productId") Integer productId,
                                        @RequestParam(value = "vendorId") Integer vendorId, HttpSession session) {
        Cart cart = sessionService.getSessionCart(session);
        try {
            cartService.addToCart(cart, productId, vendorId);
        } catch (ProductIsNotFromSameVendorException e) {
            log.info("Cart id = " + cart.getId() + " info : " + e.getMessage());
            throw e;
        }
        return cart;
    }

    @ApiOperation(value = "Add Multi product to cart", response = Cart.class, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses( value = {@ApiResponse(code = 202, message = "Multiple Products has been added to cart")})
    @PreAuthorize("hasAuthority('IS_AUTHENTICATED_ANONYMOUSLY') or hasAuthority('CUSTOMER') or hasAuthority('ADMIN')")
    @RequestMapping(value = "/cart/addMultiToCart", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public @ResponseBody Cart addMultiProductToCart(@RequestBody List<AddToCartInput> addMultiInput, HttpSession session) {
        Cart cart = sessionService.getSessionCart(session);
        if (!CollectionUtils.isEmpty(addMultiInput)) {
            addMultiInput.stream().forEach(productInput -> cartService.addToCart(cart, productInput));
        }
        return cart;
    }


    @ApiOperation(value = "Remove product from cart", response = Cart.class, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses( value = {@ApiResponse(code = 202, message = "Product has been removed from cart")})
    @PreAuthorize("hasAuthority('IS_AUTHENTICATED_ANONYMOUSLY') or hasAuthority('CUSTOMER') or hasAuthority('ADMIN')")
    @RequestMapping(value = "/cart/removeItem", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public @ResponseBody Cart removeFromCart(@RequestParam(value = "productId") Integer productId, HttpSession session) {
        Cart cart = sessionService.getSessionCart(session);
        cartService.remove(cart, productId);
        return cart;
    }

    @ApiOperation(value = "Update Item cart Quantity", response = Cart.class, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses( value = {@ApiResponse(code = 202, message = "Item cart quantity has been updated")})
    @PreAuthorize("hasAuthority('IS_AUTHENTICATED_ANONYMOUSLY') or hasAuthority('CUSTOMER') or hasAuthority('ADMIN')")
    @RequestMapping(value = "/cart/updateItemQty", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public @ResponseBody Cart updateItemQty(@RequestParam(value = "productId") Integer productId,
                                            @RequestParam(value = "qty") Integer qty, HttpSession session) {
        Cart cart = sessionService.getSessionCart(session);
        cartService.updateQty(cart, productId, qty);
        return cart;
    }

    @ApiOperation(value = "Update cart address", response = Cart.class, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses( value = {@ApiResponse(code = 202, message = "Cart Address has been updated")})
    @PreAuthorize("hasAuthority('IS_AUTHENTICATED_ANONYMOUSLY') or hasAuthority('CUSTOMER') or hasAuthority('ADMIN')")
    @RequestMapping(value = "/cart/updateAddress", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public @ResponseBody Cart updateAddress(@RequestBody @Valid SelectedAddress selectedAddress, HttpSession session) {
    	
        Cart cart = sessionService.getSessionCart(session);
        final CustomerAddress customerAddress = cartAddressPopulator.convert(selectedAddress);
        cart.setDeliveryAddress(customerAddress);
        cartService.saveCart(cart);
        return cart;
    }

    @ApiOperation(value = "Check cart address", response = Cart.class, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses( value = {@ApiResponse(code = 202, message = "Cart Address Is Under Radius")})
    @PreAuthorize("hasAuthority('IS_AUTHENTICATED_ANONYMOUSLY') or hasAuthority('CUSTOMER') or hasAuthority('ADMIN')")
    @RequestMapping(value = "/cart/checkAddress", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public @ResponseBody Cart checkAddress(HttpSession session) {
        Cart cart = sessionService.getSessionCart(session);
        final CustomerAddress deliveryAddress = cart.getDeliveryAddress();
        final Optional<Vendor> vendor = vendorService.getVendor(cart.getVendorId());
        if (vendor.isPresent()) {
            final Address address = vendor.get().getAddress();
            LatLng vendorLatLng = new LatLng(Double.valueOf(address.getLatitude()), Double.valueOf(address.getLongitude()));

            final LatLng cartLatLong = new LatLng(
                    Double.valueOf(deliveryAddress.getLatitude()),
                    Double.valueOf(deliveryAddress.getLongitude()));
            if (!googleMapService.isUnderRadius(cartLatLong, vendorLatLng)) {
                throw new IsNotUnderRadius("Address is out of range");
            }
        }
        return cart;
    }
    
    /**
 	* <h1>Promo Code Apply</h1>
    * <p>Method Simply apply the promocode to the cart save the cart and reflect the changes in DB as well as session object</p>
    * <p> Last Modified on : 9/Oct/2017 </p>
    * @author  H Kapil Kumar
    * @version 0.2
    * @since   8/Oct/2017 
    */
    @ApiOperation(value = "Apply Promocode for customer", response = Cart.class, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses( value = {@ApiResponse(code = 202, message = "promocode applied")})
    @PreAuthorize("hasAuthority('IS_AUTHENTICATED_ANONYMOUSLY') or hasAuthority('CUSTOMER') or hasAuthority('ADMIN')")
    @RequestMapping(value = "/cart/applyPromocode", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ResponseEntity<Cart> applyPromocode(@RequestParam(value = "promcode") String promoCodeInput,  HttpSession sessionObj) {
    	Optional<PromoCode> promoCodeObj = promocodeService.getPromoCode(promoCodeInput);
    	PromoCode promoCode = promoCodeObj.get();
    	//Cart cart = sessionService.getSessionCart(sessionObj);
    	Cart sessionCart =(Cart) sessionObj.getAttribute("currentCart");
    	Optional<Cart> persistedCartObj = cartService.getCartBySession(sessionCart.getSessionID());
    	Cart persistedCart = persistedCartObj.get();
    	
    	persistedCart.setPromoCodeAmount(promoCode.getDiscountDetails().getDisAmount());
    	persistedCart.setPromoCode(promoCodeInput);
    	
    	cartService.applyPromocode(persistedCart);
    	sessionService.setSessionCart(sessionObj, persistedCart);
    	return new ResponseEntity<>(persistedCart, HttpStatus.OK);
    }
    
    /**
 	* <h1>Promo Code Apply</h1>
    * <p>Method Simply apply the promocode to the cart save the cart and reflect the changes in DB as well as session object</p>
    * <p> Last Modified on : 9/Oct/2017 </p>
    * @author  H Kapil Kumar
    * @version 0.2
    * @since   8/Oct/2017 
    */
    @ApiOperation(value = "Apply Promocode for customer using mobile app", response = Cart.class, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses( value = {@ApiResponse(code = 202, message = "promocode applied")})
    @PreAuthorize("hasAuthority('IS_AUTHENTICATED_ANONYMOUSLY') or hasAuthority('CUSTOMER') or hasAuthority('ADMIN')")
    @RequestMapping(value = "/cart/applyPromocodeByApp", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ResponseEntity<Cart> applyPromocodeByApp(@RequestParam(value = "promcode") String promoCodeInput,  HttpSession sessionObj) {
    	Optional<PromoCode> promoCodeObj = promocodeService.getPromoCode(promoCodeInput);
    	PromoCode promoCode = promoCodeObj.get();
    	Cart sessionCart =(Cart) sessionObj.getAttribute("currentCart");
    	sessionCart.setPromoCodeAmount(promoCode.getDiscountDetails().getDisAmount());
    	sessionCart.setPromoCode(promoCodeInput);
    	cartService.applyPromocode(sessionCart);
    	sessionService.setSessionCart(sessionObj, sessionCart);
    	return new ResponseEntity<>(sessionCart, HttpStatus.OK);
    }
}