package com.orileo.melloe.controllers;

import com.orileo.melloe.model.exceptions.DuplicateException;
import com.orileo.melloe.model.exceptions.EntityNotFoundException;
import com.orileo.melloe.security.IEnrichedUserDetails;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.xml.bind.ValidationException;

@RequestMapping("/api")
public class BaseControllerImpl {
    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<String> entityNotFound(Exception e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ValidationException.class)
    public ResponseEntity<String> validationFailed(Exception e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<String> assertFailed(Exception e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(DuplicateException.class)
    public ResponseEntity<String> duplicateException(Exception e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<String> dbConstraintException(Exception e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
    }

    protected static Integer getCustomerId() {
        return getCurrentUserDetails().getCustomerId();
    }

    protected static Integer getVendorId() {
        return (getCurrentUserDetails()).getVendorId();
    }


    protected static boolean isCurrentUserAdmin() {
        return getCurrentUserDetails().getAuthorities()
                .contains(new SimpleGrantedAuthority("ADMIN"));
    }

    protected Integer substituteVendorId(int id) {
        return isCurrentUserAdmin() ? id : getVendorId();
    }

    protected Integer substituteCustomerId(int id) {
        return isCurrentUserAdmin() ? id : getCustomerId();
    }

  protected static IEnrichedUserDetails getCurrentUserDetails() {
        return (IEnrichedUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}
