package com.orileo.melloe.controllers;

import com.orileo.melloe.controllers.webmodel.ReminderResource;
import com.orileo.melloe.controllers.webmodel.ReminderResourceConverter;
import com.orileo.melloe.model.customer.Reminder;
import com.orileo.melloe.model.exceptions.EntityNotFoundException;
import com.orileo.melloe.servicesinterface.CustomerService;
import com.orileo.melloe.servicesinterface.ReminderService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Slf4j
@Controller
public class ReminderController extends BaseControllerImpl {

	public static final String REMINDER = "Reminder";
	private final ReminderService reminderService;
	private final CustomerService customerService;
	private final ReminderResourceConverter resourceConverter;

	@Autowired
	public ReminderController(ReminderService reminderService, CustomerService customerService,
							  ReminderResourceConverter reminderResourceConverter) {
		this.reminderService = reminderService;
		this.customerService = customerService;
		this.resourceConverter = reminderResourceConverter;
	}

	@ApiOperation(value = "Find all reminders", response = Reminder.class, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = {
			@ApiResponse(code = 404, message = "No reminders found"),
			@ApiResponse(code = 200, message = "Reminders succesfully returned")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/reminder", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Reminder>> list() {
		List<Reminder> reminders = reminderService.listAllReminders();
		if (CollectionUtils.isEmpty(reminders)) {
			log.warn("There are no Reminders found");
			throw new EntityNotFoundException("Reminders");
		}
		return new ResponseEntity<>(reminders, HttpStatus.OK);
	}

	@ApiOperation(value = "Get reminder by ID", response = Reminder.class, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = {
			@ApiResponse(code = 404, message = "No reminder found"),
			@ApiResponse(code = 200, message = "Reminder succesfully returned")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/reminder/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Reminder> getReminder(
			@PathVariable int id) {
		Optional<Reminder> reminder = reminderService.getReminder(id);
		if (!reminder.isPresent()) {
			log.warn("Reminder with ID :{} not found ", id);
			throw new EntityNotFoundException(REMINDER, id);
		}
		return new ResponseEntity<>(reminder.get(), HttpStatus.OK);
	}

	@ApiOperation(value = "Create reminder", response = ReminderResource.class, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Reminder succesfully created")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/customer/{customerID}/reminder", method = RequestMethod.POST,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Void> create(
			@PathVariable Integer customerID,
			@Valid @RequestBody ReminderResource reminderResource,
			UriComponentsBuilder ucBuilder) {
		Reminder reminder = resourceConverter.convertToService(customerID, reminderResource);
		reminderService.saveReminder(reminder);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/reminder/{id}").buildAndExpand(reminder.getId()).toUri());
		return new ResponseEntity<>(headers, HttpStatus.CREATED);
	}

	@ApiOperation(value = "Update reminder", response = ReminderResource.class,
			consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = {
			@ApiResponse(code = 404, message = "No reminder found"),
			@ApiResponse(code = 200, message = "Reminder succesfully updated")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/reminder/{id}", method = RequestMethod.PUT,
			consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Reminder> update(@PathVariable int id,
										   @Valid @RequestBody ReminderResource reminderResource) {
		Optional<Reminder> persistedReminder = reminderService.getReminder(id);
		if (!persistedReminder.isPresent()) {
			log.warn("Reminder with ID: {} not found", id);
			throw new EntityNotFoundException(REMINDER, id);
		}
		Reminder reminder = resourceConverter.convertToService(id, persistedReminder.get(), reminderResource);
		reminderService.saveReminder(reminder);
		return new ResponseEntity<>(reminder, HttpStatus.OK);
	}

	@ApiOperation(value = "Delete reminder", response = Reminder.class)
	@ApiResponses(value = {
			@ApiResponse(code = 404, message = "No reminder found"),
			@ApiResponse(code = 204, message = "Reminder succesfully deleted")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value="/reminder/{id}",method=RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public ResponseEntity<Reminder> delete(
			@PathVariable int id) {
		if (!reminderService.isReminderExists(id)) {
			log.warn("Unable to delete Reminder. Reminder with ID : {} not found", id);
			throw new EntityNotFoundException(REMINDER, id);
		}
		reminderService.deleteReminder(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
}
