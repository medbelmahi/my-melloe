package com.orileo.melloe.controllers;

import com.orileo.melloe.controllers.csv.vendor.VendorCsvConverter;
import com.orileo.melloe.email.EmailConfig;
import com.orileo.melloe.email.Mail;
import com.orileo.melloe.email.MailService;
import com.orileo.melloe.model.exceptions.EntityNotFoundException;
import com.orileo.melloe.model.login.MasterLogin;
import com.orileo.melloe.model.vendor.UserType;
import com.orileo.melloe.model.vendor.Vendor;
import com.orileo.melloe.ordertracking.OrderTrackingClient;
import com.orileo.melloe.servicesinterface.LoginService;
import com.orileo.melloe.servicesinterface.VendorService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;
import javax.validation.Valid;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Slf4j
@Controller
public class VendorController extends BaseControllerImpl {

    public static final String VENDOR = "Vendor";
    private final VendorService vendorService;
    private final LoginService loginService;
    private final VendorCsvConverter csvConverter;
    private final MailService mailService;
    private final OrderTrackingClient orderTrackingClient;

    @Autowired
    public VendorController(VendorService vendorService, LoginService loginService,
                            VendorCsvConverter csvConverter,
                            MailService mailService,
                            OrderTrackingClient orderTrackingClient) {
        this.vendorService = vendorService;
        this.loginService = loginService;
        this.csvConverter = csvConverter;
        this.mailService = mailService;
        this.orderTrackingClient= orderTrackingClient;
    }

    @ApiOperation(value = "Find all vendors", response = Vendor.class, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "No vendors found"),
            @ApiResponse(code = 200, message = "Vendors succesfully returned")})
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('VENDOR')")
    @RequestMapping(value = "/vendor", method = RequestMethod.GET)
    public ResponseEntity<List<Vendor>> list() {
        List<Vendor> vendors = vendorService.listAllVendors();
        if (CollectionUtils.isEmpty(vendors)) {
            log.warn("There are no Vendors found");
            throw new EntityNotFoundException("Vendors");
        }
        return new ResponseEntity<>(vendors, HttpStatus.OK);
    }

    @ApiOperation(value = "Find vendor by ID", response = Vendor.class, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Vendor not found"),
            @ApiResponse(code = 200, message = "Vendor succesfully returned")})
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('VENDOR')")
    @RequestMapping(value = "/vendor/{id}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Vendor> getVendor(
            @PathVariable("id") int id) {
        Optional<Vendor> vendor = vendorService.getVendor(id);
        if (!vendor.isPresent()) {
            log.warn("Vendor with ID :{} not found ", id);
            throw new EntityNotFoundException(VENDOR, id);
        }
        return new ResponseEntity<>(vendor.get(), HttpStatus.OK);
    }

    @ApiOperation(value = "Upload Vendors",
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Promocodes succesfully uploaded")})
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('VENDOR')")
    @RequestMapping(value = "/vendor/import",
            method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public void upload(@RequestParam("file") MultipartFile file) throws Exception {
        final Reader reader = new InputStreamReader(file.getInputStream(), "UTF-8");
        final CSVParser parser = new CSVParser(reader, CSVFormat.DEFAULT.withHeader());
        csvConverter.validateFile(parser.getHeaderMap().keySet());
        parser.getRecords().stream().forEach(this::parseAndCreateVendor);
        parser.close();
    }

    private void parseAndCreateVendor(CSVRecord record) {
        VendorCsvConverter.ParsedVendor parsedVendor = csvConverter.convertFromCsv(record);
        createVendor(parsedVendor.getVendor(), parsedVendor.getLogin());
    }

    @ApiOperation(value = "Create new Vendor", response = Vendor.class,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 409, message = "Vendor with duplicate email specified"),
            @ApiResponse(code = 201, message = "Vendor succesfully created")})
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('VENDOR')")
    @RequestMapping(value = "/vendor", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Boolean> createVendor(@Valid @RequestBody Vendor vendor,
                                             UriComponentsBuilder ucBuilder) {
        createVendor(vendor, vendor.getUserId());
        HttpHeaders headers = new HttpHeaders();
   		headers.setLocation(ucBuilder.path("/vendor/{id}").buildAndExpand(vendor.getId()).toUri());
   		Map < String, Object > model = new HashMap < String, Object > ();
        model.put("username", vendor.getName());
        model.put("password", vendor.getUserId().get(0).getPassword());
        EmailConfig emailConfig = new EmailConfig();
        Mail mail = emailConfig.setMailCredentials(vendor.getUserId().get(0).getEmailId(), "Welcome To Melloe", model);
        mailService.sendEmail(mail, "VendorRegistration.txt");
        
        //Creating a merchant in the system of Grab.
        boolean status = orderTrackingClient.createMerchant(vendor.getId());
        return new ResponseEntity<>(status, headers, HttpStatus.CREATED);
    }

    private Vendor createVendor(Vendor vendor, List<MasterLogin> masterLogins) {
        validateMasterLogins(masterLogins);
        enrichMasterLoginsWithType(masterLogins);
        masterLogins.forEach(loginService::saveLogin);
        vendor.setUserId(masterLogins);
        return vendorService.saveVendor(vendor);
    }

    private void validateMasterLogins(List<MasterLogin> masterLogins) {
        Assert.notEmpty(masterLogins, "Vendor should have at least one user");
        Assert.isTrue(masterLogins.size() <= 4, "Vendor cannot have more than 4 users");
        masterLogins.forEach(loginService::isMasterLoginExists);
    }

    @ApiOperation(value = "Update Vendor", response = Vendor.class,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Vendor not found"),
            @ApiResponse(code = 409, message = "Vendor with duplicate email specified"),
            @ApiResponse(code = 200, message = "Vendor succesfully updated")})
    @PreAuthorize("hasAuthority('VENDOR') or hasAuthority('ADMIN')")
    @RequestMapping(value = "/vendor/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Vendor> updateVendor(@PathVariable int id, @RequestBody @Valid Vendor vendor)
    {
        Optional<Vendor> persistedVendor = vendorService.getVendor(id);
        if (!persistedVendor.isPresent())
        {
            log.warn("Vendor with ID: {} not found", id);
            throw new EntityNotFoundException(VENDOR, id);
        }
                
		vendor.setId(substituteVendorId(id));
		vendor.setUserId(persistedVendor.get().getUserId());		    
		List<MasterLogin> masterLogins = vendor.getUserId();
		enrichMasterLoginsWithType(masterLogins);
		masterLogins.forEach(loginService::saveLogin);
		vendorService.saveVendor(vendor);
		
        log.info("Vendor updated successfully with ID : {} ", vendor.getId());
        return new ResponseEntity<>(vendor, HttpStatus.OK);
    }


    @ApiOperation(value = "Delete Vendor")
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Vendor not found"),
            @ApiResponse(code = 204, message = "Vendor succesfully deleted")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/vendor/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Vendor> deleteVendor(
            @PathVariable("id") int id) {
        Optional<Vendor> vendorOpt = vendorService.getVendor(id);
        if (!vendorOpt.isPresent()) {
            log.warn("Unable to delete Vendor. Vendor with ID : {} not found", id);
            throw new EntityNotFoundException(VENDOR, id);
        }
        Vendor vendor = vendorOpt.get();
        loginService.deleteLogins(vendor.getUserId());
        vendorService.deleteVendor(id);
        log.info("Vendor Deleted Successfully with ID : {}", vendor.getId());
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    private void enrichMasterLoginsWithType(List<MasterLogin> userIds) {
        userIds.forEach(userId -> userId.setUserType(UserType.VENDOR));
    }
}