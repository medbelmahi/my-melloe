package com.orileo.melloe.controllers;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PasswordDto 
{
		private int id;
		
		private String oldPassword;

	    @ValidPassword
	    private String newPassword;
	    
	    private String token;
}