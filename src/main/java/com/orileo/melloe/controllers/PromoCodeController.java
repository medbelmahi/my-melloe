package com.orileo.melloe.controllers;

import com.orileo.melloe.controllers.csv.promocode.PromocodeCsvConverter;
import com.orileo.melloe.model.exceptions.EntityNotFoundException;
import com.orileo.melloe.model.order.DeliveryBoy;
import com.orileo.melloe.model.promocode.PromoCode;
import com.orileo.melloe.servicesinterface.PromocodeService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;
import java.util.Optional;

@Slf4j
@Controller
public class PromoCodeController extends BaseControllerImpl {

	public static final String PROMOCODE = "Promocode";
	private final PromocodeService promocodeService;
    private final PromocodeCsvConverter csvConverter;

	@Autowired
    public PromoCodeController(PromocodeService promocodeService,
                               PromocodeCsvConverter csvConverter) {
        this.promocodeService = promocodeService;
        this.csvConverter = csvConverter;
    }

    @ApiOperation(value = "Find all promocodes", response = PromoCode.class, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
			@ApiResponse(code = 404, message = "No vendors found"),
			@ApiResponse(code = 200, message = "Vendors succesfully returned")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/promocode", method = RequestMethod.GET)
	public ResponseEntity<List<PromoCode>> list() {
		List<PromoCode> promoCodes = promocodeService.listAllPromoCodes();
		if (CollectionUtils.isEmpty(promoCodes)) {
			log.warn("There are no Vendors found");
			throw new EntityNotFoundException("Promocodes");
		}
		log.info("Fetched Vendors Successfully");
		return new ResponseEntity<>(promoCodes, HttpStatus.OK);
	}

	@ApiOperation(value = "Find promocode by ID", response = PromoCode.class, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = {
			@ApiResponse(code = 404, message = "Promocode not found"),
			@ApiResponse(code = 200, message = "Promocode succesfully returned")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/promocode/{pcode}", method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PromoCode> getPromocode(
			@PathVariable String pcode) {
		Optional<PromoCode> promocode = promocodeService.getPromoCode(pcode);
		if (!promocode.isPresent()) {
			log.warn("Promocode with ID :{} not found ", pcode);
			throw new EntityNotFoundException(PROMOCODE, pcode);
		}
		log.info("Fetched Vendor Successfully with ID : {}", promocode.get().getPromoCode());
		return new ResponseEntity<>(promocode.get(), HttpStatus.OK);
	}


    @ApiOperation(value = "Upload Promocodes",
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Promocodes succesfully uploaded")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/promocode/import",
			method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public void upload(@RequestParam("file") MultipartFile file,
                       RedirectAttributes redirectAttributes) throws Exception {
        final Reader reader = new InputStreamReader(file.getInputStream(), "UTF-8");
        final CSVParser parser = new CSVParser(reader, CSVFormat.DEFAULT.withHeader());
        csvConverter.validateFile(parser.getHeaderMap().keySet());
        parser.getRecords().stream().forEach(this::parseAndCreatePromocode);
    }

    private void parseAndCreatePromocode(CSVRecord csvRecord) {
        PromoCode promoCode = csvConverter.convertFromCsv(csvRecord);
        promocodeService.savePromoCode(promoCode);
    }

    @ApiOperation(value = "Create new Promocode", response = PromoCode.class,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Promocode succesfully created")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/promocode", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Void> create(@Valid @RequestBody PromoCode promoCode,
                                       UriComponentsBuilder ucBuilder) {
        promocodeService.savePromoCode(promoCode);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/promocode/{id}")
                .buildAndExpand(promoCode.getPromoCode()).toUri());
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @ApiOperation(value = "Update Promocode", response = PromoCode.class,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = {
			@ApiResponse(code = 404, message = "Promocode not found"),
			@ApiResponse(code = 409, message = "Promocode with duplicate email specified"),
			@ApiResponse(code = 200, message = "Promocode succesfully updated")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/promocode/{pcode}", method = RequestMethod.PUT,
			consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PromoCode> update(
			@PathVariable String pcode,
											@Valid @RequestBody PromoCode promoCode) {
		if (!promocodeService.isPromocodeExists(pcode)) {
			log.warn("Promocode with ID: {} not found", pcode);
			throw new EntityNotFoundException(PROMOCODE, pcode);
		} else {
			promoCode.setPromoCode(pcode);
			promocodeService.savePromoCode(promoCode);
		}
		return new ResponseEntity<>(promoCode, HttpStatus.OK);
	}

	@ApiOperation(value = "Delete Promocode", response = DeliveryBoy.class,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = {
			@ApiResponse(code = 404, message = "Promocode not found"),
			@ApiResponse(code = 204, message = "Promocode succesfully deleted")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/promocode/{pcode}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public ResponseEntity<PromoCode> delete(
			@PathVariable String pcode) {
		if (!promocodeService.isPromocodeExists(pcode)) {
			log.warn("Unable to delete Promocode. Promocode with ID : {} not found", pcode);
			throw new EntityNotFoundException(PROMOCODE, pcode);
		}
		promocodeService.deletePromoCode(pcode);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
