package com.orileo.melloe.controllers;

import com.orileo.melloe.model.category.Category;
import com.orileo.melloe.model.category.SubCategory;
import com.orileo.melloe.model.exceptions.EntityNotFoundException;
import com.orileo.melloe.servicesinterface.CategoryService;
import com.orileo.melloe.servicesinterface.SubCategoryService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Slf4j
@Controller
public class SubCategoryController extends BaseControllerImpl {

    public static final String SUBCATEGORY = "Subcategory";
    private final SubCategoryService subCategoryService;

    private final CategoryService categoryService;

    @Autowired
    public SubCategoryController(SubCategoryService subCategoryService,
                                 CategoryService categoryService) {
        this.subCategoryService = subCategoryService;
        this.categoryService = categoryService;
    }

    @ApiOperation(value = "List All Subcategories")
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "No Subcategories found"),
            @ApiResponse(code = 200, message = "Subcategories are retured")})
    @RequestMapping(value = "/open/subCategory", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<SubCategory>> listAllSubcategoriesForCategory(
            @RequestParam(required = false) List<Integer> categoryIDs) {
        List<SubCategory> subCategories = subCategoryService.listAllSubCategories(categoryIDs);
        if (CollectionUtils.isEmpty(subCategories)) {
            log.info("No subcategories found");
            throw new EntityNotFoundException("Subcategory");
        }
        return new ResponseEntity<>(subCategories, HttpStatus.OK);
    }

    @ApiOperation(value = "Get Subcategory by ID")
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Subcategory not found"),
            @ApiResponse(code = 200, message = "Subcategory is found")})
    @RequestMapping(value = "/open/subCategory/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SubCategory> getSubcategory(
            @PathVariable Integer id) {
        Optional<SubCategory> subCategory = subCategoryService.getSubCategory(id);
        if (!subCategory.isPresent()) {
            log.warn("Unable to find Subcategory with ID {}", id);
            throw new EntityNotFoundException(SUBCATEGORY, id);
        }
        return new ResponseEntity<>(subCategory.get(), HttpStatus.OK);
    }

    @ApiOperation(value = "Create Subcategory")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Subcategory created")})
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('VENDOR')")
    @RequestMapping(value = "category/{categoryID}/subCategory", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Void> create(
            @PathVariable Integer categoryID,
            @Valid @RequestBody SubCategory subCategory,
            UriComponentsBuilder ucBuilder) {

        Optional<Category> category = categoryService.getCategory(categoryID);
        if (!category.isPresent()) {
            log.error("Unable to find category with ID {}", categoryID);
            throw new EntityNotFoundException("Category", categoryID);
        } else {
            subCategory.setCategory(category.get());
            subCategoryService.saveSubCategory(subCategory);
            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(ucBuilder.path("/subCategory/{id}").buildAndExpand(subCategory.getId()).toUri());
            log.info("Created Subcategory Successfully with Name :{} for category {} ",
                    subCategory.getName(), category.get().getName());
            return new ResponseEntity<>(headers, HttpStatus.CREATED);
        }
    }

    @ApiOperation(value = "Update Subcategory")
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Subcategory not found"),
            @ApiResponse(code = 200, message = "Subcategory is found")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/subCategory/{id}", method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SubCategory> update(
            @PathVariable Integer id,
            @Valid @RequestBody SubCategory subCategory) {
        Optional<SubCategory> persistedSubCategory = subCategoryService.getSubCategory(id);
        if (!persistedSubCategory.isPresent()) {
            log.warn("Subcategory with ID: {} not found", id);
            throw new EntityNotFoundException(SUBCATEGORY, id);
        } else {
            subCategory.setId(id);
            subCategory.setCategory(persistedSubCategory.get().getCategory());
            subCategoryService.saveSubCategory(subCategory);
            return new ResponseEntity<>(subCategory, HttpStatus.OK);
        }
    }

    @ApiOperation(value = "Delete Subcategory")
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Subcategory not found"),
            @ApiResponse(code = 204, message = "Subcategory is deleted")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/subCategory/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<SubCategory> delete(
            @PathVariable Integer id) {
        if (!subCategoryService.isSubcategoryExists(id)) {
            log.warn("Unable to delete Subcategory.Subcategory with ID : {} not found", id);
            throw new EntityNotFoundException(SUBCATEGORY, id);
        }
        subCategoryService.deleteSubCategory(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
