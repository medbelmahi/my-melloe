package com.orileo.melloe.controllers;

import com.orileo.melloe.model.customer.Customer;
import com.orileo.melloe.model.exceptions.EntityNotFoundException;
import com.orileo.melloe.model.notifications.Notification;
import com.orileo.melloe.model.notifications.NotificationType;
import com.orileo.melloe.servicesinterface.CustomerService;
import com.orileo.melloe.servicesinterface.NotificationService;
import com.orileo.melloe.servicesinterface.NotificationTypeService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Slf4j
@Controller
public class NotificationController extends BaseControllerImpl {

	private final NotificationService notificationService;
	private final CustomerService customerService;
    private final NotificationTypeService notificationTypeService;

	@Autowired
    public NotificationController(NotificationService notificationService,
                                  CustomerService customerService,
                                  NotificationTypeService notificationTypeService) {
        this.notificationService = notificationService;
		this.customerService = customerService;
        this.notificationTypeService = notificationTypeService;
    }

	@ApiOperation(value = "Find all notifications", response = Notification.class,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = {
			@ApiResponse(code = 404, message = "No notifications found"),
			@ApiResponse(code = 200, message = "Notifications succesfully returned")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/notification", method = RequestMethod.GET)
	public ResponseEntity<List<Notification>> list() {
		List<Notification> notifications = notificationService.listAllNotifications();
		if (CollectionUtils.isEmpty(notifications)) {
			log.warn("There are no Notifications found");
			throw new EntityNotFoundException("Notification");
		}
		return new ResponseEntity<>(notifications, HttpStatus.OK);
	}

	@ApiOperation(value = "Get Notification by ID", response = Notification.class,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = {
			@ApiResponse(code = 404, message = "No notifications found"),
			@ApiResponse(code = 200, message = "Notifications succesfully returned")})
    @RequestMapping(value = "/notification/{id}", method = RequestMethod.GET)
    public ResponseEntity<Notification> getNotification(
			@PathVariable int id) {
		Optional<Notification> notification = notificationService.getNotification(id);
		if (!notification.isPresent()) {
			log.warn("Notification with ID :{} not found ", id);
			throw new EntityNotFoundException(Notification.class.getSimpleName(), id);
		}
		return new ResponseEntity<>(notification.get(), HttpStatus.OK);
	}

	@ApiOperation(value = "Save Notification by ID",
			consumes = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = {
			@ApiResponse(code = 404, message = "Customer not found"),
			@ApiResponse(code = 200, message = "Notification succesfully created")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/customer/{customerID}/notificationtype/{notificationTypeID}/notification", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Void> create(
            @PathVariable Integer customerID,
            @PathVariable Integer notificationTypeID,
            @Valid @RequestBody Notification notification,
            UriComponentsBuilder ucBuilder) {
		Optional<Customer> customer = customerService.getCustomer(customerID);
		if (!customer.isPresent()) {
			log.warn("Customer with ID {} not found", customerID);
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} else {
			notification.setCustomer(customer.get());
            notification.setNotificationType(getNotificationType(notificationTypeID));
            notificationService.saveNotification(notification);
			HttpHeaders headers = new HttpHeaders();
			headers.setLocation(ucBuilder.path("/notification/{id}")
					.buildAndExpand(notification.getId()).toUri());
			return new ResponseEntity<>(headers, HttpStatus.CREATED);
		}
	}

    private NotificationType getNotificationType(Integer notificationTypeID) {
        return notificationTypeService.getNotificationType(notificationTypeID)
                .orElseThrow(() -> new EntityNotFoundException("Notification Type", notificationTypeID));
    }

	@ApiOperation(value = "Update Notification", response = Notification.class,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = {
			@ApiResponse(code = 404, message = "No notifications found"),
			@ApiResponse(code = 200, message = "Notifications succesfully updated")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/notification/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Notification> update(
			@PathVariable int id,
			@Valid @RequestBody Notification notification) {
		if (!notificationService.isNotificationExist(id)) {
			log.warn("Notification with ID {} not found", id);
			throw new EntityNotFoundException(Notification.class.getSimpleName(), id);
		} else {
			notification.setId(id);
			notificationService.saveNotification(notification);
			return new ResponseEntity<>(HttpStatus.OK);
		}
	}

	@ApiOperation(value = "Update Notification", response = Notification.class,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = {
			@ApiResponse(code = 404, message = "No notifications found"),
			@ApiResponse(code = 200, message = "Notifications succesfully updated")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/notification/{id}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public ResponseEntity<Notification> delete(
			@PathVariable int id) {
		if (!notificationService.isNotificationExist(id)) {
			log.warn("Notification with ID {} not found", id);
			throw new EntityNotFoundException(Notification.class.getSimpleName(), id);
		} else {
			notificationService.deleteNotification(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
}
