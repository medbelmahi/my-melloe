package com.orileo.melloe.controllers;

import java.util.Optional;

import com.orileo.melloe.controllers.dto.PaymentGatewayData;
import com.orileo.melloe.servicesinterface.PaymentService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.orileo.melloe.controllers.dto.PaymentDetails;
import com.orileo.melloe.model.order.Order;
import com.orileo.melloe.servicesinterface.OrdersService;
import com.razorpay.Payment;
import com.razorpay.RazorpayClient;
import com.razorpay.RazorpayException;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

/**
 * @author mahendrapratap
 *
 */
@Slf4j
@RestController
public class PaymentController extends BaseControllerImpl {
	
	private final OrdersService ordersService;
	private final String apiKey;
	private final String secretKey;
	private RazorpayClient razorpayClient;
	private PaymentService paymentService;
	
	@Autowired
	public PaymentController(OrdersService ordersService, @Value("${melloe.razorpay.apiKey}") String apiKey,
							 @Value("${melloe.razorpay.secretKey}") String secretKey, PaymentService paymentService) throws RazorpayException {
		this.ordersService = ordersService;
		this.apiKey = apiKey;
		this.secretKey = secretKey;
		this.paymentService = paymentService;
		this.razorpayClient = new RazorpayClient(apiKey, secretKey, true);
	}
	
	@ApiOperation(value = "get payment details", response = Order.class,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Payment succesfully initiated")})
    @RequestMapping(value = "/payment/{orderId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PaymentDetails> payment(@PathVariable int orderId) {
		
		Optional<Order> order = ordersService.getOrder(orderId, getCustomerId(), getVendorId());
		if (!order.isPresent()) {
            log.warn("Order with ID :{} not found ", orderId);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
			try {
				razorpayClient = new RazorpayClient(apiKey, secretKey);

				JSONObject options = new JSONObject();
				options.put("amount", Math.round(order.get().getTotalPrice() * 100)); // Note: The amount in paise.
				options.put("currency", order.get().getCurrency());
				options.put("receipt", "receipt_" + order.get().getId());
				options.put("payment_capture", 1);
				com.razorpay.Order razorOrder = razorpayClient.Orders.create(options);
				
				PaymentDetails paymentDetails = new PaymentDetails();
				paymentDetails.setRazorpayOrderId(razorOrder.get("id"));
//				paymentDetails.setRazorpayOrderId("rzr_order_qweqweqewqe");
System.out.println("Payment Gateway Initiated Promocode Amount "+ order.get().getPromoCodeAmount());
				paymentDetails.setAmountInPaise(Math.round(order.get().getTotalPrice() * 100));
				paymentDetails.setCustomerName(order.get().getCustomer().getFirstName());
				paymentDetails.setCustomerEmail(order.get().getCustomer().getUserId().getEmailId());
				paymentDetails.setCustomerPhone(order.get().getCustomer().getPhoneNumber());
				paymentDetails.setMelloeOrderId(order.get().getId());
				
				return new ResponseEntity<>(paymentDetails, HttpStatus.OK);
			} catch (RazorpayException ex) {
				log.error("Issue with payment gateway", ex);
	            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
			}
        }
    }
	
	@ApiOperation(value = "capture payment", response = Order.class,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Payment succesfully captured")})
    @RequestMapping(value = "/capture/{paymentId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> list(@PathVariable String paymentId, @RequestParam String amount) {
		
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("amount", amount);
		
		try {
			Payment payment = razorpayClient.Payments.capture(paymentId, jsonObject);
			log.debug(payment.toString());
		} catch (RazorpayException e) {
			log.error("Error while capturing the payment.", e);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Error Occurred");
		}
		
		return new ResponseEntity<>(HttpStatus.OK);
    }


	@ApiOperation(value = "Initialize payment", response = PaymentGatewayData.class,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Payment Initialized")})
	@PreAuthorize("hasAuthority('ADMIN') or hasAuthority('CUSTOMER')")
	@RequestMapping(value = "/payment/init", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PaymentGatewayData> initPaymentObject() {
		PaymentGatewayData paymentGateway = paymentService.initPaymentObject();
		return new ResponseEntity<>(paymentGateway, HttpStatus.OK);
	}

}
