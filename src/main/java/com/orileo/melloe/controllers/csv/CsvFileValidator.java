package com.orileo.melloe.controllers.csv;

import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.Set;

@Component
public class CsvFileValidator {
    public void validateFile(Set<String> columns, Set<String> expectedColumns) {
        Assert.isTrue(columns.containsAll(expectedColumns),
                String.format("File contains invalid column names: %s, valid column names are:%s",
                        columns.toString(), expectedColumns.toString()));
    }
}
