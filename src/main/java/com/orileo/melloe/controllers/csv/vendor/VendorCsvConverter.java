package com.orileo.melloe.controllers.csv.vendor;

import com.orileo.melloe.controllers.csv.CsvFileValidator;
import com.orileo.melloe.controllers.csv.CsvRecordHelper;
import com.orileo.melloe.model.login.MasterLogin;
import com.orileo.melloe.model.vendor.Address;
import com.orileo.melloe.model.vendor.CellNumber;
import com.orileo.melloe.model.vendor.Vendor;
import lombok.Value;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.orileo.melloe.controllers.csv.vendor.VendorColumns.*;


@Component
public class VendorCsvConverter {
    public static final String CONTACT_PERSON = "CONTACT_PERSON_";
    public static final String EMPTY_CONTACT_MESSAGE = "Column %s is blank, it should be filled in for this row ";
    private final CsvFileValidator csvFileValidator;

    @Autowired
    public VendorCsvConverter(CsvFileValidator csvFileValidator) {
        this.csvFileValidator = csvFileValidator;
    }


    public ParsedVendor convertFromCsv(CSVRecord record) {
        CsvRecordHelper recordHelper = new CsvRecordHelper(record);
        Vendor vendor = new Vendor();
        vendor.setName(recordHelper.getString(NAME));
        vendor.setCellNumbers(parseCellNumbers(recordHelper));
        vendor.setAddress(parseAddress(recordHelper));
        List<MasterLogin> masterLogins = parseMasterLogins(recordHelper);
        return new ParsedVendor(masterLogins, vendor);
    }

    private List<MasterLogin> parseMasterLogins(CsvRecordHelper recordHelper) {
        MasterLogin masterLogin = new MasterLogin();
        masterLogin.setEmailId(recordHelper.getString(EMAIL));
        masterLogin.setPassword("123456");
        return Collections.singletonList(masterLogin);
    }

    private Address parseAddress(CsvRecordHelper recordHelper) {
        Address address = new Address();
        address.setLine1(recordHelper.getString(LINE_1));
        address.setLine2(recordHelper.getString(LINE_2));
        address.setLine3(recordHelper.getString(LINE_3));
        address.setCity(recordHelper.getString(CITY));
        address.setState(recordHelper.getString(STATE));
        address.setCountry(recordHelper.getString(COUNTRY));
        address.setZipcode(recordHelper.getString(ZIPCODE));
        address.setLandMark(recordHelper.getString(LANDMARK));
        address.setLandLine(recordHelper.getString(LANDLINE));
        return address;
    }

    private List<CellNumber> parseCellNumbers(CsvRecordHelper recordHelper) {
        List<String> contactPersons = recordHelper.getRecordColumns()
                .stream()
                .filter(r -> r.startsWith(CONTACT_PERSON))
                .collect(Collectors.toList());
        List<CellNumber> cellNumbers = contactPersons.stream().map(cp -> parseCellNumber(cp, recordHelper))
                .collect(Collectors.toList());
        Assert.isTrue(!CollectionUtils.isEmpty(cellNumbers),
                "At least one cell number should be provided");
        return cellNumbers;
    }

    private CellNumber parseCellNumber(String contactPersonColumnName,
                                       CsvRecordHelper recordHelper) {
        String contactPersonName = recordHelper.getString(contactPersonColumnName);
        String phoneNumberColumnName = "CELL_PHONE_"
                + StringUtils.substringAfter(contactPersonColumnName, CONTACT_PERSON);
        String phoneNumber = recordHelper.getString(phoneNumberColumnName);
        Assert.isTrue(StringUtils.isNotBlank(contactPersonName),
                String.format(EMPTY_CONTACT_MESSAGE,
                        contactPersonColumnName));
        Assert.isTrue(StringUtils.isNotBlank(phoneNumber),
                String.format(EMPTY_CONTACT_MESSAGE,
                        contactPersonColumnName));
        return new CellNumber(contactPersonName, phoneNumber);
    }

    public void validateFile(Set<String> columns) {
        csvFileValidator.validateFile(columns, COLUMN_NAMES);
    }

    @Value
    public static class ParsedVendor {
        List<MasterLogin> login;
        Vendor vendor;
    }
}
