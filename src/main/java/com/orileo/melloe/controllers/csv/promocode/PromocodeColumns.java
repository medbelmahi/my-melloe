package com.orileo.melloe.controllers.csv.promocode;

import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Arrays.stream;

public enum PromocodeColumns {
    PROMOCODE,
    DISCOUNT_AMOUNT,
    DISCOUNT_TYPE,
    START_DATE,
    END_DATE,
    OFFER_TYPE,
    IS_ACTIVE,
    IS_FIXED,
    DESCRIPTION;

    public static Set<String> COLUMN_NAMES = stream(values())
            .map(Enum::name).collect(Collectors.toSet());
}
