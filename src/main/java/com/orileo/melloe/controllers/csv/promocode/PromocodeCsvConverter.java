package com.orileo.melloe.controllers.csv.promocode;

import com.orileo.melloe.controllers.csv.CsvFileValidator;
import com.orileo.melloe.controllers.csv.CsvRecordHelper;
import com.orileo.melloe.model.customer.DiscountDetails;
import com.orileo.melloe.model.customer.DiscountType;
import com.orileo.melloe.model.promocode.PromoCode;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

import static com.orileo.melloe.controllers.csv.CsvUtils.DATE_FORMAT;
import static com.orileo.melloe.controllers.csv.promocode.PromocodeColumns.*;

@Component
public class PromocodeCsvConverter {

    private final CsvFileValidator csvFileValidator;

    @Autowired
    public PromocodeCsvConverter(CsvFileValidator csvFileValidator) {
        this.csvFileValidator = csvFileValidator;
    }

    public PromoCode convertFromCsv(CSVRecord csvRecord) {
        CsvRecordHelper recordHelper = new CsvRecordHelper(csvRecord, DATE_FORMAT);
        PromoCode promoCode = new PromoCode();
        promoCode.setPromoCode(recordHelper.getString(PROMOCODE));
        DiscountDetails discountDetails = new DiscountDetails();
        discountDetails.setDisAmount(recordHelper.getDouble(DISCOUNT_AMOUNT));
        discountDetails.setDiscountType((DiscountType) recordHelper.getEnum(
                DISCOUNT_TYPE,
                DiscountType::valueOf));
        promoCode.setDiscountDetails(discountDetails);
        promoCode.setStartDate(recordHelper.getDate(START_DATE));
        promoCode.setEndDate(recordHelper.getDate(END_DATE));
        promoCode.setOfferType(recordHelper.getString(OFFER_TYPE));
        promoCode.setActive(recordHelper.getBoolean(IS_ACTIVE));
        promoCode.setFixed(recordHelper.getBoolean(IS_FIXED));
        promoCode.setDescription(recordHelper.getString(DESCRIPTION));
        return promoCode;
    }

    public void validateFile(Set<String> columns) {
        csvFileValidator.validateFile(columns, COLUMN_NAMES);
    }


}
