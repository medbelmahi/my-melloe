package com.orileo.melloe.controllers.csv.product;

import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Arrays.stream;

public enum ProductColumns {
    NAME,
    DESCRIPTION,
    CATEGORIES,
    SUBCATEGORIES,
    IS_VEGETARIAN,
    PRODUCT_STATUS,
    PRODUCT_COUNT,
    IS_DISCOUNTED,
    PROMOCODE,
    FROM_DATE,
    TO_DATE,
    DISCOUNT_AMOUNT,
    MIN_AMOUNT,
    MAX_AMOUNT,
    IS_FIXED,
    PICTURE,
    UNIT_MEASUREMENT,
    PRICE,
    CURRENCY,
    IS_DEFAULT,
    VENDOR_ID;

    public static Set<String> COLUMN_NAMES = stream(values())
            .map(Enum::name).collect(Collectors.toSet());
}
