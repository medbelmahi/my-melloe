package com.orileo.melloe.controllers.csv.customerpromocode;

import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Arrays.stream;

public enum CustomerPromocodeColumns {
    CUSTOMER_ID, PROMOCODE, FROM_DATE, TO_DATE, IS_USED;

    public static Set<String> COLUMN_NAMES = stream(values())
            .map(Enum::name).collect(Collectors.toSet());
}
