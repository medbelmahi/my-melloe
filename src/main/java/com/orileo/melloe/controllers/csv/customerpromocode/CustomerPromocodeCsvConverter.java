package com.orileo.melloe.controllers.csv.customerpromocode;

import com.orileo.melloe.controllers.csv.CsvFileValidator;
import com.orileo.melloe.controllers.csv.CsvRecordHelper;
import com.orileo.melloe.model.customer.CustomerPromoCode;
import lombok.Value;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

import static com.orileo.melloe.controllers.csv.CsvUtils.DATE_FORMAT;
import static com.orileo.melloe.controllers.csv.customerpromocode.CustomerPromocodeColumns.*;

@Component
public class CustomerPromocodeCsvConverter {
    private final CsvFileValidator csvFileValidator;

    @Autowired
    public CustomerPromocodeCsvConverter(CsvFileValidator csvFileValidator) {
        this.csvFileValidator = csvFileValidator;
    }


    public ParsedCustomerPromocode convertFromCsv(CSVRecord csvRecord) {
        CsvRecordHelper recordHelper = new CsvRecordHelper(csvRecord, DATE_FORMAT);
        Integer customerID = recordHelper.getInteger(CUSTOMER_ID);
        String pcode = recordHelper.getString(PROMOCODE);
        CustomerPromoCode customerPromoCode = new CustomerPromoCode();
        customerPromoCode.setToDate(recordHelper.getDate(TO_DATE));
        customerPromoCode.setFromDate(recordHelper.getDate(FROM_DATE));
        customerPromoCode.setUsed(recordHelper.getBoolean(IS_USED));
        return new ParsedCustomerPromocode(customerID, pcode, customerPromoCode);
    }

    public void validateFile(Set<String> columns) {
        csvFileValidator.validateFile(columns, COLUMN_NAMES);
    }

    @Value
    public static final class ParsedCustomerPromocode {
        Integer customerID;
        String pcode;
        CustomerPromoCode customerPromoCode;
    }
}
