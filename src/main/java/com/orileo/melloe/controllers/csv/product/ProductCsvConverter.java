package com.orileo.melloe.controllers.csv.product;

import com.orileo.melloe.controllers.csv.CsvFileValidator;
import com.orileo.melloe.controllers.csv.CsvRecordHelper;
import com.orileo.melloe.model.category.Category;
import com.orileo.melloe.model.category.SubCategory;
import com.orileo.melloe.model.exceptions.EntityNotFoundException;
import com.orileo.melloe.model.product.Product;
import com.orileo.melloe.model.product.ProductDiscount;
import com.orileo.melloe.model.product.ProductPrice;
import com.orileo.melloe.model.product.ProductStatus;
import com.orileo.melloe.model.vendor.Vendor;
import com.orileo.melloe.servicesinterface.CategoryService;
import com.orileo.melloe.servicesinterface.SubCategoryService;
import com.orileo.melloe.servicesinterface.VendorService;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.orileo.melloe.controllers.csv.product.ProductColumns.*;


@Component
public class ProductCsvConverter {
    private final CsvFileValidator csvFileValidator;
    private final CategoryService categoryService;
    private final SubCategoryService subCategoryService;
    private final VendorService vendorService;


    @Autowired
    public ProductCsvConverter(CsvFileValidator csvFileValidator,
                               CategoryService categoryService,
                               SubCategoryService subCategoryService, VendorService vendorService) {
        this.csvFileValidator = csvFileValidator;
        this.categoryService = categoryService;
        this.subCategoryService = subCategoryService;
        this.vendorService = vendorService;
    }

    public Product convertFromCsv(CSVRecord record) {
        CsvRecordHelper recordHelper = new CsvRecordHelper(record);
        Product product = new Product();
        product.setName(recordHelper.getString(NAME));
        product.setDescription(recordHelper.getString(DESCRIPTION));
        product.setPicture(recordHelper.getString(PICTURE));
        product.setDiscounted(recordHelper.getBoolean(IS_DISCOUNTED));
        product.setVegetarian(recordHelper.getBoolean(IS_VEGETARIAN));
        product.setProductCount(recordHelper.getInteger(PRODUCT_COUNT));
        product.setProductStatus((ProductStatus) recordHelper.getEnum(
                PRODUCT_STATUS, ProductStatus::valueOf));
        setCategories(product, recordHelper.getList(CATEGORIES));
        setSubCategories(product, recordHelper.getList(SUBCATEGORIES));
        product.setProductPrice(parseProductPrice(recordHelper));
        product.setProductDiscounts(parseProductDiscounts(recordHelper));
        product.setVendor(getVendor(recordHelper));
        return product;
    }

    private Vendor getVendor(CsvRecordHelper recordHelper) {
        Integer vendorID = recordHelper.getInteger(VENDOR_ID);
        return vendorService.getVendor(vendorID)
                .orElseThrow(() -> new EntityNotFoundException("Vendor", vendorID));
    }

    private List<ProductDiscount> parseProductDiscounts(CsvRecordHelper recordHelper) {
        String promoCode = recordHelper.getString(PROMOCODE);
        if (StringUtils.isNotBlank(promoCode)) {
            ProductDiscount productDiscount = new ProductDiscount();
            productDiscount.setPromoCode(promoCode);
            productDiscount.setFromDate(recordHelper.getDate(FROM_DATE));
            productDiscount.setToDate(recordHelper.getDate(TO_DATE));
            productDiscount.setDisAmount(recordHelper.getDouble(DISCOUNT_AMOUNT));
            productDiscount.setMinAmount(recordHelper.getDouble(MIN_AMOUNT));
            productDiscount.setMaxAmount(recordHelper.getDouble(MAX_AMOUNT));
            productDiscount.setIsFixed(recordHelper.getBoolean(IS_FIXED));
            return Collections.singletonList(productDiscount);
        } else {
            return Collections.emptyList();
        }
    }

    private ProductPrice parseProductPrice(CsvRecordHelper recordHelper) {
        ProductPrice productPrice = new ProductPrice();
        productPrice.setUnitMeasurment(recordHelper.getString(UNIT_MEASUREMENT));
        productPrice.setPrice(recordHelper.getDouble(PRICE));
        productPrice.setCurrency(recordHelper.getString(CURRENCY));
        productPrice.setDefault(recordHelper.getBoolean(IS_DEFAULT));
        return productPrice;
    }

    private void setCategories(Product product, List<String> categoryNames) {
        List<Category> categories = categoryNames.stream()
                .map(c -> categoryService.findByName(c)
                        .orElseThrow(() -> new EntityNotFoundException("Category", c)))
                .collect(Collectors.toList());
        product.setCategory(categories);
    }

    private void setSubCategories(Product product, List<String> subCategoryNames) {
      Set<SubCategory> subCategories = subCategoryNames.stream()
                .map(c -> subCategoryService.findByName(c)
                        .orElseThrow(() -> new EntityNotFoundException("Subcategory", c)))
          .collect(Collectors.toSet());
        product.setSubcategory(subCategories);
    }

    public void validateFile(Set<String> columns) {
        csvFileValidator.validateFile(columns, COLUMN_NAMES);
    }
}
