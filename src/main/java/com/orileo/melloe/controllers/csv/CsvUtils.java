package com.orileo.melloe.controllers.csv;

import lombok.experimental.UtilityClass;

@UtilityClass
public class CsvUtils {
    public static String DATE_FORMAT = "DD/MM/YYYY";
}
