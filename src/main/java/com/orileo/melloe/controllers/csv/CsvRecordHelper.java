package com.orileo.melloe.controllers.csv;

import com.google.common.collect.Lists;
import com.orileo.melloe.model.exceptions.MelloeException;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.util.Assert;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

import static java.util.Optional.ofNullable;


public class CsvRecordHelper {
    private final CSVRecord record;
    private final SimpleDateFormat formatter;

    public CsvRecordHelper(CSVRecord record, String format) {
        this.record = record;
        this.formatter = new SimpleDateFormat(format);
    }

    public CsvRecordHelper(CSVRecord record) {
        this.record = record;
        this.formatter = new SimpleDateFormat(CsvUtils.DATE_FORMAT);
    }
    public Integer getInteger(Enum name) {
        String nonParsed = record.get(name);
        Assert.isTrue(NumberUtils.isDigits(nonParsed),
                "Fail to parse " + name + " for value " + nonParsed);
        return Integer.parseInt(nonParsed);
    }

    public String getString(Enum name) {
        return record.get(name);
    }

    public String getString(String name) {
        return record.get(name);
    }
    public List<String> getList(Enum name) {
        String nonParsed = ofNullable(record.get(name)).orElse(StringUtils.EMPTY);
        return Lists.newArrayList(StringUtils.split(nonParsed, "\\|"));
    }

    public Enum getEnum(Enum name, Function<String, Enum> enumConverter) {
        String nonParsed = record.get(name);
        if (StringUtils.isEmpty(nonParsed)) {
            return null;
        } else {
            return enumConverter.apply(nonParsed);
        }
    }

    public Date getDate(Enum name) {
        String nonParsed = record.get(name);
        try {
            return formatter.parse(nonParsed);
        } catch (ParseException e) {
            throw new MelloeException("Unable to parse date for " + name + " from value " + nonParsed);
        }
    }

    public boolean getBoolean(Enum name) {
        return Boolean.parseBoolean(record.get(name));
    }

    public Double getDouble(Enum name) {
        String nonParsed = record.get(name);
        Assert.isTrue(NumberUtils.isNumber(nonParsed),
                "Fail to parse " + name + " for value " + nonParsed);
        return Double.parseDouble(nonParsed);
    }

    public Set<String> getRecordColumns() {
        return record.toMap().keySet();
    }
}
