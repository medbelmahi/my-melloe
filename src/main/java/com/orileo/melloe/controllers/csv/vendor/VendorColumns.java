package com.orileo.melloe.controllers.csv.vendor;

import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Arrays.stream;

public enum VendorColumns {
    EMAIL,
    NAME,
    LANDLINE,
    LINE_1,
    LINE_2,
    LINE_3,
    CITY,
    COUNTRY,
    LANDMARK,
    STATE,
    ZIPCODE;
    public static Set<String> COLUMN_NAMES = stream(values())
            .map(Enum::name).collect(Collectors.toSet());
}
