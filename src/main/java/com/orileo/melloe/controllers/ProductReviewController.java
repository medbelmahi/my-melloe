package com.orileo.melloe.controllers;

import com.orileo.melloe.model.customer.Customer;
import com.orileo.melloe.model.exceptions.EntityNotFoundException;
import com.orileo.melloe.model.product.Product;
import com.orileo.melloe.model.product.ProductReview;
import com.orileo.melloe.servicesinterface.CustomerService;
import com.orileo.melloe.servicesinterface.ProductReviewService;
import com.orileo.melloe.servicesinterface.ProductService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Slf4j
@Controller
public class ProductReviewController extends BaseControllerImpl {
    public static final String PRODUCT_REVIEW = "Product Review";
    private final ProductReviewService productReviewService;
    private final CustomerService customerService;
    private final ProductService productService;

    @Autowired
    public ProductReviewController(ProductReviewService productReviewService,
                                   CustomerService customerService,
                                   ProductService productService) {
        this.productReviewService = productReviewService;
        this.customerService = customerService;
        this.productService = productService;
    }

    @ApiOperation(value = "List Product Reviews", response = ProductReview.class,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Product review succesfully created")})
    @RequestMapping(value = "/productreview", method = RequestMethod.GET)
    public ResponseEntity<List<ProductReview>> listAllProductReviews(
            @RequestParam(required = false) List<Integer> customerID,
            @RequestParam(required = false) List<Integer> productID) {
        List<ProductReview> productReviews = productReviewService.listAllProductReviews(customerID, productID);
        if (CollectionUtils.isEmpty(productReviews)) {
            log.warn("There is no ProductReviews found");
            throw new EntityNotFoundException("Product Reviews");
        }
        log.info("Fecthed ProductReviews Successfully");
        return new ResponseEntity<>(productReviews, HttpStatus.OK);
    }

    @ApiOperation(value = "Find product review by ID", response = ProductReview.class,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Product review succesfully created")})
    @RequestMapping(value = "/productreview/{id}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProductReview> getProductReview(
            @PathVariable("id") Integer id) {
        log.info("Fecthing ProductReview with Id:-" + id);
        Optional<ProductReview> productReviews = productReviewService.getProductReview(id);
        if (!productReviews.isPresent()) {
            log.warn("ProductReview with ID {} not found", id);
            throw new EntityNotFoundException(PRODUCT_REVIEW, id);
        }
        return new ResponseEntity<>(productReviews.get(), HttpStatus.OK);
    }

    @ApiOperation(value = "Create Product Review", response = ProductReview.class,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Product Review not found"),
            @ApiResponse(code = 200, message = "Product Review succesfully returned")})
    @PreAuthorize("hasAuthority('CUSTOMER') or hasAuthority('ADMIN')")
    @RequestMapping(value = "customer/product/{productID}/productreview", method = RequestMethod.POST)
    public ResponseEntity<Void> createProductReview(
            @PathVariable Integer productID,
            @Valid @RequestBody ProductReview productReview,
            UriComponentsBuilder ucBuilder) {
        Integer customerID = getCustomerId();
        Optional<Customer> customer = customerService.getCustomer(customerID);
        if (!customer.isPresent()) {
            log.warn("Failed to find Customer with ID {}", customerID);
            throw new EntityNotFoundException("Customer", customerID);
        }
        Optional<Product> product = productService.getProduct(productID);
        if (!product.isPresent()) {
            log.warn("Failed to find Product with ID {}", productID);
            throw new EntityNotFoundException("Product", productID);
        }
        productReview.setCustomer(customer.get());
        productReview.setProduct(product.get());
        productReviewService.saveProductReview(productReview);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/productReviews/{id}").buildAndExpand(productReview.getId()).toUri());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @ApiOperation(value = "Update product review", response = ProductReview.class,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Product review not found"),
            @ApiResponse(code = 200, message = "Product review succesfully updated")})
    @PreAuthorize("hasAuthority('CUSTOMER') or hasAuthority('ADMIN')")
    @RequestMapping(value = "/productreview/{id}", method = RequestMethod.PUT)
    public ResponseEntity<ProductReview> updateProductReview(
            @PathVariable int id,
            @Valid @RequestBody ProductReview productReview) {
        Optional<ProductReview> persisted = productReviewService.getProductReview(id, getCustomerId());
        if (!persisted.isPresent()) {
            log.warn("Failed to find ProductReview with ID {}", id);
            throw new EntityNotFoundException(PRODUCT_REVIEW, id);
        } else {
            productReview.setCustomer(persisted.get().getCustomer());
            productReview.setProduct(persisted.get().getProduct());
            productReviewService.saveProductReview(productReview);
            return new ResponseEntity<>(productReview, HttpStatus.OK);
        }
    }

    @ApiOperation(value = "Delete Product Review", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Product review not found"),
            @ApiResponse(code = 204, message = "Product review successfully deleted")})
    @PreAuthorize("hasAuthority('CUSTOMER') or hasAuthority('ADMIN')")
    @RequestMapping(value = "/productreview/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<ProductReview> deleteProductReview(
            @PathVariable("id") int id) {
        Optional<ProductReview> productReview = productReviewService.getProductReview(id, getCustomerId());
        if (!productReview.isPresent()) {
            log.warn("Unable to delete. ProductReview with ID {} not found", id);
            throw new EntityNotFoundException(PRODUCT_REVIEW, id);
        } else {
            productReviewService.deleteProductReview(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
}