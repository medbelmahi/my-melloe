package com.orileo.melloe.controllers;

import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;
import com.orileo.melloe.model.exceptions.EntityNotFoundException;
import com.orileo.melloe.model.order.Order;
import com.orileo.melloe.model.order.OrderStatus;
import com.orileo.melloe.ordertracking.LiveOrderTracking;
import com.orileo.melloe.ordertracking.OrderDetailsResponse;
import com.orileo.melloe.ordertracking.OrderStatusResponse;
import com.orileo.melloe.ordertracking.OrderTrackingClient;
import com.orileo.melloe.servicesinterface.OrdersService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class OrderTrackingController extends BaseControllerImpl {

	private final OrderTrackingClient orderTrackingClient;
	private final OrdersService ordersService;

	@Autowired
	public OrderTrackingController(OrderTrackingClient orderTrackingClient, OrdersService ordersService) {
		this.orderTrackingClient = orderTrackingClient;
		this.ordersService = ordersService;
	}

	@PreAuthorize("hasAuthority('ADMIN')")
	@ApiOperation(value = "Create Merchant at", response = Boolean.class, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Merchant registered successfully") })
	@RequestMapping(value = "/createMerchant/{vendorId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Boolean> createMerchant(@PathVariable int vendorId) {
		boolean status = orderTrackingClient.createMerchant(vendorId);

		return new ResponseEntity<>(status, HttpStatus.OK);
	}

	@ApiOperation(value = "Create order delivery", response = OrderDetailsResponse.class, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "order delivery created.") })
	@RequestMapping(value = "/createOrderDelivery/{orderId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<OrderDetailsResponse> createOrderDelivery(@PathVariable int orderId) {
		OrderDetailsResponse status = orderTrackingClient.createOrder(getCustomerId(), orderId);

		return new ResponseEntity<>(status, HttpStatus.OK);
	}

	@ApiOperation(value = "Push order delivery", response = Integer.class, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "push delivery created.") })
	@RequestMapping(value = "/pushOrderDelivery/{orderId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> pushOrderDelivery(@PathVariable int orderId)
	{
		Integer expectedDelivery = orderTrackingClient.pushOrder(getCustomerId(), orderId);
		if(expectedDelivery != null)
		{
			// Update the order status once the order is pushed 
			Optional<Order> optionalOrder = ordersService.getOrder(orderId, getCustomerId(), getVendorId());
			if (optionalOrder.isPresent())
			{
				Order order = optionalOrder.get();
				order.setOrderStatus(OrderStatus.ORDER_IN_PROGRESS);
				ordersService.saveOrder(order);
			} else 
			{
				log.warn("Order with ID: {} not found", orderId);
				throw new EntityNotFoundException("Order", orderId);
			}
		}
		
		return new ResponseEntity<>(expectedDelivery, HttpStatus.OK);
	}

	@ApiOperation(value = "Track order delivery", response = OrderStatusResponse.class, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Current order status returned.") })
	@RequestMapping(value = "/trackOrderDelivery/{orderId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<OrderStatusResponse> trackOrderDelivery(@PathVariable int orderId) {
		OrderStatusResponse status = orderTrackingClient.orderStatus(orderId);

		return new ResponseEntity<>(status, HttpStatus.OK);
	}

	@ApiOperation(value = "Cancel Order Delivery", response = Boolean.class, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Order Delivery cancelled.") })
	@RequestMapping(value = "/cancelOrderDelivery/{orderId}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Boolean> cancelOrderDelivery(@PathVariable int orderId, @RequestBody String cancelComment) {
		Optional<Order> optionalOrder = ordersService.getOrder(orderId, getCustomerId(), getVendorId());
		boolean status = false;
		if (optionalOrder.isPresent()) {
			Order order = optionalOrder.get();
			OrderStatus orderStatus = order.getOrderStatus();
			if (OrderStatus.ORDER_ACCEPTED.equals(orderStatus) || OrderStatus.ORDER_IN_PROGRESS.equals(orderStatus)
					|| OrderStatus.READY_FOR_DELIVERY.equals(orderStatus)) {
				status = orderTrackingClient.cancelOrder(getCustomerId(), orderId, cancelComment);
			}
			order.setOrderStatus(OrderStatus.ORDER_VENDOR_CANCELLED);
			ordersService.saveOrder(order);
		} else {
			log.warn("Order with ID: {} not found", orderId);
			throw new EntityNotFoundException("Order", orderId);
		}
		return new ResponseEntity<>(status, HttpStatus.OK);
	}
	
	/**
	* <b>Note:</b> API call method to update order status as per grap API status
	* @author  Kapil Halewale
	* @version 0.2 Beta
	* @since   2017-08-10
	*/
	
	@ApiOperation(value = "Is order delivery", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = { @ApiResponse(code = 201, message = "order status succesfully received")})
	@RequestMapping(value = "/test/isOrderDelivered",method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LiveOrderTracking>  isOrderDelivered(@Valid @RequestBody LiveOrderTracking liveOrderTracking, UriComponentsBuilder ucBuilder)
	{
		if(liveOrderTracking != null)
		{
			Optional<Order> optionalOrder = ordersService.getOrder(liveOrderTracking.getClientOrderId(), null, null);
			if (optionalOrder.isPresent())
			{
				Order order = optionalOrder.get();
				
				if(liveOrderTracking.getOrderStatus() == 3)
				{
					order.setOrderStatus(OrderStatus.ASSIGNED_DELIVERY_BOY);
				}
				
				if(liveOrderTracking.getOrderStatus() == 5)
				{
					order.setOrderStatus(OrderStatus.OUT_FOR_DELIVERY);
				}
				
				if(liveOrderTracking.getOrderStatus() == 6)
				{
					order.setOrderStatus(OrderStatus.ORDER_DELIVERED);
				}
				ordersService.saveOrder(order);
				log.info(liveOrderTracking.getClientOrderId() +" No Order status updated to "+order.getOrderStatus());
			}
			else
			{
				log.warn("Order with ID: {} not found to update", liveOrderTracking.getClientOrderId());			
			}
		}
			
        return new ResponseEntity<>(HttpStatus.CREATED);
	}
}