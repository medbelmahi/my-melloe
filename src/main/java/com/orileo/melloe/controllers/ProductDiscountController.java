package com.orileo.melloe.controllers;

import com.orileo.melloe.model.exceptions.EntityNotFoundException;
import com.orileo.melloe.model.product.Product;
import com.orileo.melloe.model.product.ProductDiscount;
import com.orileo.melloe.servicesinterface.ProductDiscountService;
import com.orileo.melloe.servicesinterface.ProductService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Slf4j
@Controller
public class ProductDiscountController extends BaseControllerImpl {
	public static final String PRODUCT_DISCOUNT = "Product Discount";
	public static final String PRODUCT = "product";
	private final ProductDiscountService productDiscountService;
	private final ProductService productService;

	@Autowired
	public ProductDiscountController(ProductDiscountService productDiscountService,
									 ProductService productService) {
		this.productDiscountService = productDiscountService;
		this.productService = productService;
	}


	@ApiOperation(value = "Find all product discounts",
			response = ProductDiscount.class, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = {
			@ApiResponse(code = 404, message = "No product discounts found"),
			@ApiResponse(code = 200, message = "products discounts succesfully returned")})
	@RequestMapping(value = "/productdiscount", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ProductDiscount>> list() {
		List<ProductDiscount> productDiscount = productDiscountService.listAllProductDiscounts();
		if (CollectionUtils.isEmpty(productDiscount)) {
			log.warn("There are no Product Discounts found");
			throw new EntityNotFoundException("Product Discounts");
		}
		return new ResponseEntity<>(productDiscount, HttpStatus.OK);
	}

	@ApiOperation(value = "Find Product Discount by ID", response = ProductDiscount.class,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = {
			@ApiResponse(code = 404, message = "No product discounts found"),
			@ApiResponse(code = 200, message = "products discounts succesfully returned")})
	@RequestMapping(value = "/productdiscount/{id}", method = RequestMethod.GET)
	public ResponseEntity<ProductDiscount> getListById(
			@PathVariable int id) {
		Optional<ProductDiscount> productDiscount = productDiscountService.getProductDiscount(id);
		if (!productDiscount.isPresent()) {
			log.warn("Product Discount with ID {} not found", id);
			throw new EntityNotFoundException(PRODUCT_DISCOUNT, id);
		}
		return new ResponseEntity<>(productDiscount.get(), HttpStatus.OK);
	}

	@ApiOperation(value = "Create Product Discount")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "products discount succesfully created")})
    @PreAuthorize("hasAuthority('VENDOR') or hasAuthority('ADMIN')")
    @RequestMapping(value = "product/{productID}/productdiscount",
			method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> create(
            @PathVariable Integer productID,
            @RequestBody @Valid ProductDiscount productDiscount,
            UriComponentsBuilder ucBuilder) {
        Optional<Product> product = productService.findProduct(productID, getVendorId());
        if (!product.isPresent()) {
			log.error("Unable to find Product with ID {}", productID);
			throw new EntityNotFoundException(PRODUCT, productID);
		} else {
			productDiscount.setProduct(product.get());
			productDiscountService.saveProductDiscount(productDiscount);
			HttpHeaders headers = new HttpHeaders();
			headers.setLocation(ucBuilder.path("/productdiscount/{id}").buildAndExpand(productDiscount.getId()).toUri());
			return new ResponseEntity<>(headers, HttpStatus.CREATED);
		}
	}

	@ApiOperation(value = "Update Product Discount")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "products discount succesfully updated")})
    @PreAuthorize("hasAuthority('VENDOR') or hasAuthority('ADMIN')")
    @RequestMapping(value = "/productdiscount/{id}", method = RequestMethod.PUT,
			consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ProductDiscount> update(
            @PathVariable int id, @Valid @RequestBody ProductDiscount productDiscount) {
        Optional<ProductDiscount> persisted = productDiscountService.findProductDiscount(id, getVendorId());
        if (!persisted.isPresent()) {
            log.warn("Product Discount with ID {} not found", id);
			throw new EntityNotFoundException(PRODUCT_DISCOUNT, id);
		}
        Product product = persisted.get().getProduct();
        productDiscount.setProduct(product);
        productDiscount.setId(id);
		productDiscountService.saveProductDiscount(productDiscount);
		return new ResponseEntity<>(productDiscount, HttpStatus.OK);
	}

	@ApiOperation(value = "Delete Product Discount")
	@ApiResponses(value = {
			@ApiResponse(code = 404, message = "No product discount found"),
			@ApiResponse(code = 200, message = "products discount succesfully deleted")})
    @PreAuthorize("hasAuthority('VENDOR') or hasAuthority('ADMIN')")
    @RequestMapping(value="/productdiscount/{id}",method=RequestMethod.DELETE)
    public ResponseEntity<ProductDiscount> delete(@PathVariable int id) {
        Optional<ProductDiscount> persisted = productDiscountService.getProductDiscount(id);
        if (!productDiscountService.findProductDiscount(id, getVendorId()).isPresent()) {
            log.warn("Product Discount with ID {} not found", id);
			throw new EntityNotFoundException(PRODUCT_DISCOUNT, id);
		}
		productDiscountService.deleteProductDiscount(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
