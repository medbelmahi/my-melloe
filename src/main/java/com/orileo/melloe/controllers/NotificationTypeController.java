package com.orileo.melloe.controllers;

import com.orileo.melloe.model.exceptions.EntityNotFoundException;
import com.orileo.melloe.model.notifications.NotificationType;
import com.orileo.melloe.servicesinterface.NotificationTypeService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Slf4j
@Controller
public class NotificationTypeController extends BaseControllerImpl {

    public static final String NOTIFICATION_TYPE = "Notification type";
    private final NotificationTypeService notificationTypeService;

    @Autowired
    public NotificationTypeController(NotificationTypeService notificationTypeService) {
        this.notificationTypeService = notificationTypeService;
    }

    @ApiOperation(value = "Find all notification types", response = NotificationType.class,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "No notification types found"),
            @ApiResponse(code = 200, message = "Notification types succesfully returned")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/notificationtype", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<NotificationType>> listNotificationTypes() {
        List<NotificationType> notificationTypes = notificationTypeService.listAllNotificationTypes();
        if (CollectionUtils.isEmpty(notificationTypes)) {
            log.warn("There are no Notification types found");
            throw new EntityNotFoundException("Notification types");
        }
        return new ResponseEntity<>(notificationTypes, HttpStatus.OK);
    }

    @ApiOperation(value = "Get Notification type by ID", response = NotificationType.class,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Notification type not found"),
            @ApiResponse(code = 200, message = "Notification type succesfully returned")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/notificationtype/{id}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<NotificationType> getListById(
            @PathVariable int id) {
        Optional<NotificationType> notificationType = notificationTypeService.getNotificationType(id);
        if (!notificationType.isPresent()) {
            log.warn("Notification Type with ID :{} not found ", id);
            throw new EntityNotFoundException(NOTIFICATION_TYPE, id);
        }
        return new ResponseEntity<>(notificationType.get(), HttpStatus.OK);
    }

    @ApiOperation(value = "Create Notification Type",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Notification type succesfully created")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/notificationtype", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Void> create(@Valid @RequestBody NotificationType notificationType,
                                       UriComponentsBuilder ucBuilder) {
        notificationTypeService.saveNotificationType(notificationType);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/notificationtype/{id}")
                .buildAndExpand(notificationType.getId()).toUri());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }


    @ApiOperation(value = "Update Notification Type", response = NotificationType.class,
            produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Notification type not found"),
            @ApiResponse(code = 201, message = "Notification type succesfully created")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/notificationtype/{id}", method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<NotificationType> update(
            @PathVariable int id,
            @Valid @RequestBody NotificationType notificationType) {
        if (!notificationTypeService.isNotificationTypeExist(id)) {
            log.warn("Notification type with ID {} not found", id);
            throw new EntityNotFoundException(NOTIFICATION_TYPE, id);
        } else {
            notificationType.setId(id);
            notificationTypeService.saveNotificationType(notificationType);
            return new ResponseEntity<>(HttpStatus.OK);
        }
    }

    @ApiOperation(value = "Delete Notification Type", response = NotificationType.class,
            produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Notification type not found"),
            @ApiResponse(code = 204, message = "Notification type succesfully deleted")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/notificationtype/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<NotificationType> delete(
            @PathVariable int id) {
        if (!notificationTypeService.isNotificationTypeExist(id)) {
            log.warn("Notification type with ID {} not found", id);
            throw new EntityNotFoundException(NOTIFICATION_TYPE, id);
        } else {
            notificationTypeService.deleteNotificationType(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
}
