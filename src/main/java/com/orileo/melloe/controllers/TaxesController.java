package com.orileo.melloe.controllers;

import com.orileo.melloe.model.exceptions.EntityNotFoundException;
import com.orileo.melloe.model.payment.Tax;
import com.orileo.melloe.servicesinterface.TaxesService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;


@Slf4j
@Controller
public class TaxesController extends BaseControllerImpl {

    private final TaxesService taxesService;

	@Autowired
    public TaxesController(TaxesService taxesService) {
        this.taxesService = taxesService;
    }

    @ApiOperation(value = "List Taxes")
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "No taxes defined"),
            @ApiResponse(code = 200, message = "Taxes are found")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/taxes", method = RequestMethod.GET)
    public ResponseEntity<List<Tax>> list() {
        List<Tax> taxDetails = taxesService.listAllTaxes();
        if (CollectionUtils.isEmpty(taxDetails)) {
            throw new EntityNotFoundException("Taxes");
        }
        return new ResponseEntity<>(taxDetails, HttpStatus.OK);
    }

    @ApiOperation(value = "Get Tax by ID")
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "No taxes defined"),
            @ApiResponse(code = 200, message = "Tax is found")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/taxes/{id}", method = RequestMethod.GET)
    public ResponseEntity<Tax> getListById(@PathVariable int id) {
        Optional<Tax> taxDetails = taxesService.getTax(id);
        if (!taxDetails.isPresent()) {
            log.warn("Tax with ID {} not found", id);
            throw new EntityNotFoundException("Tax", id);
        }
        return new ResponseEntity<>(taxDetails.get(), HttpStatus.OK);
    }

    @ApiOperation(value = "Create Tax")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Tax succesfully created")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/taxes", method = RequestMethod.POST)
    public ResponseEntity<Void> create(@Valid @RequestBody Tax tax,
                                       UriComponentsBuilder ucBuilder) {
        taxesService.save(tax);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/taxes/{id}").buildAndExpand(tax.getId()).toUri());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @ApiOperation(value = "Update Tax")
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "No taxes found"),
            @ApiResponse(code = 200, message = "Tax succesfully updated")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/taxes/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Tax> update(@PathVariable int id,
                                      @Valid @RequestBody Tax tax) {
        if (!taxesService.isTaxExist(id)) {
            log.warn("Tax with ID {} not found", id);
            throw new EntityNotFoundException("Tax", id);
        } else {
            tax.setId(id);
            taxesService.save(tax);
            return new ResponseEntity<>(HttpStatus.OK);
        }
    }

    @ApiOperation(value = "Delete Tax")
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "No taxes found"),
            @ApiResponse(code = 204, message = "Tax succesfully deleted")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/taxes/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Tax> delete(@PathVariable int id) {
        if (!taxesService.isTaxExist(id)) {
            log.warn("Tax with ID {} not found", id);
            throw new EntityNotFoundException("Tax", id);
        } else {
            taxesService.deleteTax(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
}
