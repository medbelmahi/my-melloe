package com.orileo.melloe.controllers;

import com.orileo.melloe.model.exceptions.EntityNotFoundException;
import com.orileo.melloe.model.order.DeliveryBoy;
import com.orileo.melloe.servicesinterface.DeliveryBoyService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Slf4j
@Controller
public class DeliveryBoyController extends BaseControllerImpl {

    public static final String DELIVERY_BOY = "Delivery boy";
    private final DeliveryBoyService deliveryBoyService;

    @Autowired
    public DeliveryBoyController(DeliveryBoyService deliveryBoyService) {
        this.deliveryBoyService = deliveryBoyService;
    }

    @ApiOperation(value = "Find all Delivery Boys", response = DeliveryBoy.class, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "No Delivery Boys found"),
            @ApiResponse(code = 200, message = "Delivery boys succesfully returned")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/deliveryboy", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<DeliveryBoy>> list() {
        List<DeliveryBoy> deliveryBoy = deliveryBoyService.listAllDeliveryBoys();
        if (CollectionUtils.isEmpty(deliveryBoy)) {
            log.warn("There are no Delivery Boys found");
            throw new EntityNotFoundException("Delivery boys");
        }
        return new ResponseEntity<>(deliveryBoy, HttpStatus.OK);
    }

    @ApiOperation(value = "Find Delivery Boy by ID", response = DeliveryBoy.class,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Vendor not found"),
            @ApiResponse(code = 200, message = "Vendor succesfully returned")})
    @RequestMapping(value = "/deliveryboy/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DeliveryBoy> getListById(
            @PathVariable int id) {
        Optional<DeliveryBoy> deliveryBoy = deliveryBoyService.getDeliveryBoy(id);
        if (!deliveryBoy.isPresent()) {
            log.warn("Delivery Boy with ID :{} not found ", id);
            throw new EntityNotFoundException(DELIVERY_BOY, id);
        }
        return new ResponseEntity<>(deliveryBoy.get(), HttpStatus.OK);
    }

    @ApiOperation(value = "Create new Delivery Boy", response = DeliveryBoy.class,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Delivery Boy succesfully created")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/deliveryboy", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Void> create(@Valid @RequestBody DeliveryBoy deliveryBoy,
                                       UriComponentsBuilder ucBuilder) {
        deliveryBoyService.saveDeliveryBoy(deliveryBoy);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/deliveryboy/{id}").buildAndExpand(deliveryBoy.getId()).toUri());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @ApiOperation(value = "Update Delivery Boy", response = DeliveryBoy.class,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Delivery Boy not found"),
            @ApiResponse(code = 200, message = "Delivery Boy succesfully updated")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/deliveryboy/{id}", method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DeliveryBoy> update(
            @PathVariable int id,
            @Valid @RequestBody DeliveryBoy deliveryBoy) {
        if (!deliveryBoyService.isDeliveryBoyExists(id)) {
            log.warn("Delivery Boy with ID: {} not found", id);
            throw new EntityNotFoundException(DELIVERY_BOY, id);
        }
        deliveryBoy.setId(id);
        deliveryBoyService.saveDeliveryBoy(deliveryBoy);
        return new ResponseEntity<>(deliveryBoy, HttpStatus.OK);
    }

    @ApiOperation(value = "Delete Delivery Boy", response = DeliveryBoy.class,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Delivery Boy not found"),
            @ApiResponse(code = 204, message = "Delivery Boy succesfully deleted")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/deliveryboy/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<DeliveryBoy> delete(
            @PathVariable int id) {
        if (!deliveryBoyService.isDeliveryBoyExists(id)) {
            log.warn("Unable to delete Delivery Boy. Delivery Boy with ID : {} not found", id);
            throw new EntityNotFoundException(DELIVERY_BOY, id);
        }
        deliveryBoyService.deleteDeliveryBoy(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
