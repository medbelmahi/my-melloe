package com.orileo.melloe.controllers.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

/**
 * Created by Mohamed BELMAHI on 31/08/2017.
 */
@Setter
@Getter
public class LocateMeForm {
    @NotNull
    @NotEmpty
    private String address;
    @NotNull
    @NotEmpty
    private String house;
    @NotNull
    @NotEmpty
    private String landmark;
    @NotNull
    @NotEmpty
    private String addressName;
    @NotNull
    @NotEmpty
    private String lat;
    @NotNull
    @NotEmpty
    private String lng;
}
