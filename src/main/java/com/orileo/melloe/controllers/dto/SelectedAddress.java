package com.orileo.melloe.controllers.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

/**
 * Created by Mohamed BELMAHI on 08/09/2017.
 */
@Setter
@Getter
public class SelectedAddress {
    @NotNull
    @NotEmpty
    private String addressLine;
    @NotNull
    @NotEmpty
    private String addressLine_2;
    @NotNull
    @NotEmpty
    private String addressName;
    @NotNull
    @NotEmpty
    private String nearestLandmark;
    private String latitude;
    private String longitude;
    private String city;
    private String state;
    private String pincode;
}
