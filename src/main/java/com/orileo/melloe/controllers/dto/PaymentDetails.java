package com.orileo.melloe.controllers.dto;

public class PaymentDetails {

	public PaymentDetails() {
	}

	private long amountInPaise;
	private String razorpayOrderId;
	private String customerName;
	private String customerEmail;
	private String customerPhone;
	private int melloeOrderId;

	public long getAmountInPaise() {
		return amountInPaise;
	}

	public void setAmountInPaise(long amountInPaise) {
		this.amountInPaise = amountInPaise;
	}

	public String getRazorpayOrderId() {
		return razorpayOrderId;
	}

	public void setRazorpayOrderId(String razorpayOrderId) {
		this.razorpayOrderId = razorpayOrderId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public String getCustomerPhone() {
		return customerPhone;
	}

	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}

	public int getMelloeOrderId() {
		return melloeOrderId;
	}

	public void setMelloeOrderId(int melloeOrderId) {
		this.melloeOrderId = melloeOrderId;
	}

}