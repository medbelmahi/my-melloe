package com.orileo.melloe.controllers.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Mohamed BELMAHI on 20/08/2017.
 */
@Getter
@Setter
public class AddToCartInput {
    private Integer productId;
    private Integer vendorId;
    private Integer qty;
}