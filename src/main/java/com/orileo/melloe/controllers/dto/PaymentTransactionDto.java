package com.orileo.melloe.controllers.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Mohamed BELMAHI on 21/08/2017.
 */
@Getter
@Setter
public class PaymentTransactionDto {
    private String id;
    private String entity;
    private Integer amount;
    private String currency;
    private String status;
    private String order_id;
    private String invoice_id;
    private boolean international;
    private String method;
    private Integer amount_refunded;
    private String refund_status;
    private boolean captured;
    private String description;
    private String card_id;
    private String bank;
    private String wallet;
    private String vpa;
    private String email;
    private String contact;
    private Notes notes;
    private Integer fee;
    private Integer tax;
    private String error_code;
    private String error_description;
    private long created_at;

    @Getter
    @Setter
    private class Notes {
        private String merchant_order_id;
    }
}
