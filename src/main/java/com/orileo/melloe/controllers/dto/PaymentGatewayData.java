package com.orileo.melloe.controllers.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by Mohamed BELMAHI on 19/08/2017.
 */
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class PaymentGatewayData {
    private String key;
    private Double amount;
    private String name;
    private String description;
    private String image;
    private Profile prefill;
    private Notes notes;
    private Theme theme;

    public PaymentGatewayData withKey(String key) {
        this.key = key;
        return this;
    }

    public PaymentGatewayData withAmount(Double amount) {
        this.amount = amount;
        return this;
    }

    public PaymentGatewayData withName(String name) {
        this.name = name;
        return this;
    }

    public PaymentGatewayData withDescription(String description) {
        this.description = description;
        return this;
    }

    public PaymentGatewayData withImage(String image) {
        this.image = image;
        return this;
    }

    public PaymentGatewayData withProfile(String name, String email) {
        this.prefill = new Profile(name, email);
        return this;
    }

    public PaymentGatewayData withNotes(String address) {
        this.notes = new Notes(address);
        return this;
    }

    public PaymentGatewayData withThem(String color) {
        this.theme = new Theme(color);
        return this;
    }

    @Getter
    @Setter
    @EqualsAndHashCode
    @ToString
    private class Profile {
        private String name;
        private String email;

        private Profile(String name, String email) {
            this.name = name;
            this.email = email;
        }
    }

    @Getter
    @Setter
    @EqualsAndHashCode
    @ToString
    private class Notes {
        private String address;

        private Notes(String address) {
            this.address = address;
        }
    }

    @Getter
    @Setter
    @EqualsAndHashCode
    @ToString
    private class Theme {
        private String color;

        private Theme(String color) {
            this.color = color;
        }
    }
}


