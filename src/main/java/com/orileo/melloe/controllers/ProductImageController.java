package com.orileo.melloe.controllers;

import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.orileo.melloe.model.product.Product;
import com.orileo.melloe.servicesinterface.ImageService;
import com.orileo.melloe.servicesinterface.ProductService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class ProductImageController extends BaseControllerImpl {

  private final ImageService imageService;
  private final ProductService productService;

  public ProductImageController(ImageService imageService, ProductService productService) {
    this.imageService = imageService;
    this.productService = productService;
  }

  @PreAuthorize("hasAuthority('VENDOR') or hasAuthority('ADMIN')")
  @RequestMapping(value = "/product/{id}/image", method = RequestMethod.POST,
      consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
  public ResponseEntity<Void> create(@RequestParam("file") MultipartFile imageFile,
                                     @PathVariable Integer id) {

    boolean isUploaded = imageService.uploadFile(imageFile, "product-" + id);
    
    if(isUploaded) {
    	Optional<Product> product = productService.getProduct(id);
			product.ifPresent(p -> {
				p.setPicture(imageService.generateLink("product-" + id));
				productService.saveProduct(p);
			});
    }
    log.info("Image for product id: {} is uploaded successfully.", id);
    return new ResponseEntity<>(HttpStatus.OK);
  }

}
