
package com.orileo.melloe.controllers;

import com.google.maps.model.LatLng;
import com.orileo.melloe.controllers.dto.LocateMeForm;
import com.orileo.melloe.controllers.webmodel.CustomerResource;
import com.orileo.melloe.controllers.webmodel.CustomerResourceConverter;
import com.orileo.melloe.email.EmailConfig;
import com.orileo.melloe.email.Mail;
import com.orileo.melloe.email.MailService;
import com.orileo.melloe.facade.populator.Populator;
import com.orileo.melloe.model.cart.Cart;
import com.orileo.melloe.model.customer.Customer;
import com.orileo.melloe.model.customer.CustomerAddress;
import com.orileo.melloe.model.customer.CustomerContactForm;
import com.orileo.melloe.model.exceptions.EntityNotFoundException;
import com.orileo.melloe.model.exceptions.IsNotUnderRadius;
import com.orileo.melloe.security.IEnrichedUserDetails;
import com.orileo.melloe.servicesinterface.*;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.Resource;
import javax.annotation.security.PermitAll;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import java.util.*;


@Slf4j
@Controller
public class CustomerController extends BaseControllerImpl {
    private final CustomerService customerService;
    private final LoginService loginService;
    private final CustomerResourceConverter customerResourceConverter;
    private final MailService mailService;
    private final SessionService sessionService;
    private final CartService cartService;
    private final GoogleMapService googleMapService;

    @Resource(name = "customerAddressPopulator")
    private Populator<CustomerAddress, LocateMeForm> customerAddressPopulator;

    @Autowired
    public CustomerController(CustomerService customerService,
                              LoginService loginService,
                              CustomerResourceConverter customerResourceConverter,
                              MailService mailService, SessionService sessionService, CartService cartService, GoogleMapService googleMapService) {
        this.customerService = customerService;
        this.loginService = loginService;
        this.customerResourceConverter = customerResourceConverter;
        this.mailService = mailService;
        this.sessionService = sessionService;
        this.cartService = cartService;
        this.googleMapService = googleMapService;
    }

    @ApiOperation(value = "Find all customers", response = Customer.class, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "No customers found"),
            @ApiResponse(code = 200, message = "Customers succesfully returned")})
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('VENDOR')")
    @RequestMapping(value = "/customer", method = RequestMethod.GET)
    public ResponseEntity<List<Customer>> listCustomers() {
        List<Customer> customers = customerService.listAllCustomers();
        if (CollectionUtils.isEmpty(customers)) {
            log.warn("There are no Customers found");
            throw new EntityNotFoundException("Customers");
        }
        return new ResponseEntity<>(customers, HttpStatus.OK);
    }

    @ApiOperation(value = "Customer's Info", response = Customer.class, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "No customers found"),
            @ApiResponse(code = 200, message = "Customers succesfully returned")})
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('CUSTOMER')")
    @RequestMapping(value = "/customerinfo", method = RequestMethod.GET)
    public ResponseEntity<Customer> getCustomerInfo(HttpSession session) {
    	IEnrichedUserDetails userDetails = getCurrentUserDetails();
    	if (userDetails.getCustomerId() ==null) { return new ResponseEntity<>( HttpStatus.BAD_REQUEST);}
    	int id = userDetails.getCustomerId();
    	Optional<Customer> customer = customerService.getCustomer(id);
    	if (!customer.isPresent()) {
            log.warn("Customer with ID :{} not found ", id);
            throw new EntityNotFoundException(Customer.class.getSimpleName(), id);
        }
        final Cart cart = (Cart) session.getAttribute(SessionService.CURRENT_CART);
        if (cart == null) {
            final Cart currentCart = customer.get().getCart();
            if (currentCart != null) {
                session.setAttribute(SessionService.CURRENT_CART, currentCart);
            }
        } else {
            final Cart currentCart = customer.get().getCart();
            if (currentCart != null) {
                if (currentCart.getId() != cart.getId()) {
                    session.setAttribute(SessionService.CURRENT_CART, currentCart);
                }
            }
        }
        return new ResponseEntity<>(customer.get(), HttpStatus.OK);
    }

    @ApiOperation(value = "Find customer by ID", response = Customer.class, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Customer not found"),
            @ApiResponse(code = 200, message = "Customer succesfully returned")})
    @RequestMapping(value = "/customer/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('CUSTOMER')")
    public ResponseEntity<Customer> getCustomer(
            @PathVariable("id") int id) {
        Optional<Customer> customer = customerService.getCustomer(id);
        if (!customer.isPresent()) {
            log.warn("Customer with ID :{} not found ", id);
            throw new EntityNotFoundException(Customer.class.getSimpleName(), id);
        }
        return new ResponseEntity<>(customer.get(), HttpStatus.OK);
    }

    @ApiOperation(value = "Create new Customer", response = CustomerResource.class,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 409, message = "Customer with duplicate email specified"),
            @ApiResponse(code = 201, message = "Customer succesfully created")})
    @PermitAll
    @RequestMapping(value = "/open/customer", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Customer> create(@Valid @RequestBody CustomerResource customerResource,
                                       UriComponentsBuilder ucBuilder, HttpSession session) {
        Customer customer = customerResourceConverter.convertToService(customerResource);
        loginService.isMasterLoginExists(customer.getUserId());
        loginService.saveLogin(customer.getUserId());
        customerService.saveCustomer(customer);
        System.out.println("customer " +customer);
        //setSessionCartToNewCustomer(session, customer);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/customer/{id}").buildAndExpand(customer.getId()).toUri());
        log.info("Created customer successfully with name : " + customer.getFirstName() + " " + customer.getLastName());
        Map < String, Object > model = new HashMap < String, Object > ();
        model.put("username", customer.getFirstName());
        EmailConfig emailConfig = new EmailConfig();
        Mail mail = emailConfig.setMailCredentials(customer.getUserId().getEmailId(), "Welcome To Melloe", model);
        mailService.sendEmail(mail, "CustomerRegistration.txt");
        return new ResponseEntity<>(customer, headers, HttpStatus.CREATED);
    }


    private void setSessionCartToNewCustomer(HttpSession session, Customer customer) {
        final Cart sessionCart = sessionService.getSessionCart(session);
        customer.setCart(sessionCart);
        //sessionCart.setCustomer(customer);
        //cartService.saveCart(sessionCart);
        customerService.saveCustomer(customer);
    }


    @ApiOperation(value = "Update Customer", response = CustomerResource.class,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Customer not found"),
            @ApiResponse(code = 409, message = "Customer with duplicate email specified"),
            @ApiResponse(code = 200, message = "Customer succesfully updated")})
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('CUSTOMER')")
    @RequestMapping(value = "/customer/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Customer> updateCustomer(
            @PathVariable Integer id,
            @Valid @RequestBody CustomerResource customerResource) {
        Optional<Customer> persisted = customerService.getCustomer(substituteCustomerId(id));
        if (!persisted.isPresent()) {
            log.warn("Customer with ID: {} not found", id);
            throw new EntityNotFoundException(Customer.class.getSimpleName(), id);
        }
        Customer customer = customerResourceConverter.convertToService(persisted.get(), customerResource);
        customer.setCustomerWallet(persisted.get().getCustomerWallet());
        customerService.saveCustomer(customer);
        return new ResponseEntity<>(customer, HttpStatus.OK);
    }


    @ApiOperation(value = "Delete Customer")
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Customer not found"),
            @ApiResponse(code = 204, message = "Customer succesfully deleted")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/customer/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Customer> deleteCustomer(@PathVariable("id") int id) {
        if (!customerService.isCustomerExists(id)) {
            log.warn("Unable to delete Customer. Customer with ID : {} not found", id);
            throw new EntityNotFoundException(Customer.class.getSimpleName(), id);
        }
        customerService.deleteCustomer(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    
	/**
	* <b>Note:</b> API call method to save the contact details from website 
	* @author  Kapil Halewale
	* @version 0.2 Beta
	* @since   2017-08-14
	*/
    
    @ApiOperation(value = "new contact form data", response = CustomerResource.class, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Contated Customer/people succesfully saved")})
    @PermitAll
    @RequestMapping(value = "/open/customer_contacted", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<CustomerContactForm> customerContacted(@Valid @RequestBody CustomerContactForm customerContactForm, UriComponentsBuilder ucBuilder)
    {
    	CustomerContactForm customerContactFormDb = customerService.saveCustomerContactForm(customerContactForm);
    	if(customerContactFormDb != null && customerContactFormDb.getId() > 0)
    	{
    		Map < String, Object > model = new HashMap < String, Object > ();
    		if(customerContactForm.getRefName().equalsIgnoreCase("CONTACTED"))
    		{
    			model.put("message", "Contacting "+customerContactForm.getName()+" our team will get back you soon");
    		}
    		else if(customerContactForm.getRefName().equalsIgnoreCase("SUBSCRIBER"))
    		{
    			model.put("message", "subscribing");
    		}
    		EmailConfig emailConfig = new EmailConfig();
    		Mail mail = emailConfig.setMailCredentials(customerContactForm.getEmailId(), "Melloe Support Team", model);
    		mailService.sendEmail(mail, "contacted.txt");
    	}
    	
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/open/customer_contacted/{id}").buildAndExpand(customerContactForm.getId()).toUri());
        return new ResponseEntity<>(customerContactForm, headers, HttpStatus.CREATED);
    }

    
    @ApiOperation(value = "Find all contacted customers/peoples", response = CustomerContactForm.class, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "No contacted customers found"),
            @ApiResponse(code = 200, message = "contacted customers succesfully returned")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/contactedcustomers", method = RequestMethod.GET)
    public ResponseEntity<List<CustomerContactForm>> getContactedCustomersInfo()
    {
        List<CustomerContactForm> contactedCustomers = customerService.listAllCustomerContactForm();
        if (CollectionUtils.isEmpty(contactedCustomers)) {
            log.warn("There are no contacted customers found");
            throw new EntityNotFoundException("CoontactedCustomers");
        }
        return new ResponseEntity<>(contactedCustomers, HttpStatus.OK);
    }

    @ApiOperation(value = "Update Session Address", response = CustomerAddress.class, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "No contacted customers found"),
            @ApiResponse(code = 200, message = "contacted customers succesfully returned"),
            @ApiResponse(code = 400, message = "Bad Request")})
    @PreAuthorize("hasAuthority('IS_AUTHENTICATED_ANONYMOUSLY') or hasAuthority('ADMIN') or hasAuthority('CUSTOMER')")
    @RequestMapping(value = "/update/cartAddress", method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody CustomerAddress updateCustomerAddress(@RequestBody @Valid LocateMeForm locateMeForm, HttpSession session) throws IsNotUnderRadius {
        //final LatLng latLngOrigin = new LatLng(Double.valueOf(locateMeForm.getLat()), Double.valueOf(locateMeForm.getLng()));
            final Cart sessionCart = sessionService.getSessionCart(session);
            CustomerAddress customerAddress = customerAddressPopulator.convert(locateMeForm);
            customerAddress.setAddressLine_2(locateMeForm.getAddress());
            sessionCart.setDeliveryAddress(customerAddress);
            System.out.println(customerAddress);
            System.out.println(sessionCart);
            cartService.saveCart(sessionCart);
            
            IEnrichedUserDetails userDetails = getCurrentUserDetails();
            if (userDetails.getCustomerId() != null) {
                int id = userDetails.getCustomerId();
                Optional<Customer> customer = customerService.getCustomer(id);
                if (customer.isPresent()) {
                    List<CustomerAddress> customerAddresses = customer.get().getAddress();
                    if (CollectionUtils.isEmpty(customerAddresses)) {
                        customerAddresses = new ArrayList<CustomerAddress>();
                    }
                    customerAddresses.add(customerAddress);
                    customerService.saveCustomer(customer.get());
                }
            }

            return customerAddress;
    }
}