package com.orileo.melloe.controllers;

import com.orileo.melloe.model.customer.CustomerLevel;
import com.orileo.melloe.model.exceptions.EntityNotFoundException;
import com.orileo.melloe.servicesinterface.CustomerLevelService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Slf4j
@Controller
public class CustomerLevelController extends BaseControllerImpl {

    private final CustomerLevelService customerLevelService;

    @Autowired
    public CustomerLevelController(CustomerLevelService customerLevelService) {
        this.customerLevelService = customerLevelService;
    }

    @ApiOperation(value = "Find all customer levels", response = CustomerLevel.class,
            responseContainer = "list", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "No customer levels found"),
            @ApiResponse(code = 200, message = "Customer levels succesfully returned")})
    @RequestMapping(value = "/level", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<List<CustomerLevel>> list() {
        List<CustomerLevel> levels = customerLevelService.listAllCustomerLevels();
        if (CollectionUtils.isEmpty(levels)) {
            throw new EntityNotFoundException("Customer Levels");
        }
        return new ResponseEntity<>(levels, HttpStatus.OK);
    }

    @ApiOperation(value = "Find all customer levels", response = CustomerLevel.class,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Customer Level not found"),
            @ApiResponse(code = 200, message = "Customer Level succesfully returned")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/level/{id}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CustomerLevel> getCustomerLevel(
            @PathVariable int id) {
        Optional<CustomerLevel> level = customerLevelService.getCustomerLevel(id);
        if (!level.isPresent()) {
            log.warn("Customer Level with ID :{} not found ", id);
            throw new EntityNotFoundException(CustomerLevel.class.getSimpleName(), id);
        }
        return new ResponseEntity<>(level.get(), HttpStatus.OK);
    }

    @ApiOperation(value = "Create Customer Level",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Customer Level succesfully created")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value="/level",method = RequestMethod.POST)
    public ResponseEntity<Void> create(@Valid @RequestBody CustomerLevel customerLevel,
                                       UriComponentsBuilder ucBuilder) {
        customerLevelService.saveCustomerLevel(customerLevel);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/level/{id}").buildAndExpand(customerLevel.getId()).toUri());
        log.info("Created Customer Level Successfully with ID :{} ", customerLevel.getName());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @ApiOperation(value = "Update Customer Level", response = CustomerLevel.class,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Customer Level not found"),
            @ApiResponse(code = 200, message = "Customer Level succesfully created")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/level/{id}", method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CustomerLevel> update(
            @PathVariable int id,
            @Valid @RequestBody CustomerLevel customerLevel) {
        if (!customerLevelService.isCustomerLevelExist(id)) {
            log.warn("Customer Level with ID: {} not found", id);
            throw new EntityNotFoundException(CustomerLevel.class.getSimpleName(), id);
        }
        customerLevel.setId(id);
        customerLevelService.saveCustomerLevel(customerLevel);
        return new ResponseEntity<>(customerLevel, HttpStatus.OK);
    }

    @ApiOperation(value = "Delete Customer Level", response = CustomerLevel.class,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Customer Level not found"),
            @ApiResponse(code = 204, message = "Customer Level succesfully deleted")})
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value="/level/{id}",method=RequestMethod.DELETE)
    public ResponseEntity<CustomerLevel> delete(
            @PathVariable int id) {
        if (!customerLevelService.isCustomerLevelExist(id)) {
            log.warn("Unable to delete Customer Level. Customer Level with ID : {} not found", id);
            throw new EntityNotFoundException(CustomerLevel.class.getSimpleName(), id);
        }
        customerLevelService.deleteCustomerLevel(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
