package com.orileo.melloe.ordertracking;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode
@ToString
public class OrderDetails {

	private Integer clientId;
	private Integer merchantId;
	private Integer orderId;
	@JsonInclude(Include.NON_EMPTY)
	private Integer clientOrderId;
	private String dttm;
	private String customerPhone;
	private String customerName;
	@JsonInclude(Include.NON_EMPTY)
	private String deliveryAreaLat;
	@JsonInclude(Include.NON_EMPTY)
	private String deliveryAreaLong;
	private String customerAddressLine1;
	private String customerAddressLine2;
	private String customerAddressLandMark;
	private String orderType;
	private String comments;
	@JsonInclude(Include.NON_EMPTY)
	private Integer prepTime;
	@JsonInclude(Include.NON_EMPTY)
	private Double billAmount;
	@JsonInclude(Include.NON_EMPTY)
	private Integer billNo;
	@JsonInclude(Include.NON_EMPTY)
	private Double amountPaidByClient;
	@JsonInclude(Include.NON_EMPTY)
	private Double amountCollectedInCash;
	
}