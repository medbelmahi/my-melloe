package com.orileo.melloe.ordertracking;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode
@ToString
public class OrderStatusDetails {
	private int clientId;
	private int clientOrderId;

}