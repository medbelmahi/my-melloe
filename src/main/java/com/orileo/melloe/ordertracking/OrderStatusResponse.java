package com.orileo.melloe.ordertracking;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode
@ToString
public class OrderStatusResponse {
	private String merchantId;
	private String billNo;
	private String orderAmount;
	private String orderStatus;
	private String deliveryBoyName;
	private String deliveryBoyContact;
	private String deliveryBoyLat;
	private String deliveryBoyLong;
	private int requestTimestamp;
	private boolean success;
	private String errorCode;
	private String errorDesciption;
}