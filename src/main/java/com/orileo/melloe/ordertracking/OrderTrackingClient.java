package com.orileo.melloe.ordertracking;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import com.orileo.melloe.commonmethods.CommonMethods;
import com.orileo.melloe.model.exceptions.EntityNotFoundException;
import com.orileo.melloe.model.exceptions.MelloeException;
import com.orileo.melloe.model.order.Order;
import com.orileo.melloe.model.vendor.Vendor;
import com.orileo.melloe.servicesinterface.OrdersService;
import com.orileo.melloe.servicesinterface.VendorService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class OrderTrackingClient {

	private final RestTemplate restTemplate;
	private final VendorService vendorService;
	private final OrdersService ordersService;
	private final String grabPublicKey;
	private final String grabPrivateKey;
	private final String baseURL;
	private final int grabClientId;

	@Autowired
	public OrderTrackingClient(VendorService vendorService, OrdersService ordersService,
			@Value("${melloe.grab.client.id}") int clientId, @Value("${melloe.grab.baseurl}") String baseurl,
			@Value("${melloe.grab.public.key}") String grabPublicKey,
			@Value("${melloe.grab.private.key}") String grabPrivateKey) {
		this.vendorService = vendorService;
		this.ordersService = ordersService;
		this.grabClientId = clientId;
		this.baseURL = baseurl;
		this.grabPublicKey = grabPublicKey;
		this.grabPrivateKey = grabPrivateKey;
		this.restTemplate = new RestTemplate();
	}

	/**
	 * Create Merchant on Grab.
	 * @param vendorId
	 * @return
	 */
	public boolean createMerchant(int vendorId) {
		String URL = baseURL + "/createmerchant/";
		boolean isCreated = false;
		MerchantDetails mDetails = new MerchantDetails();

		Optional<Vendor> vendor = vendorService.getVendor(vendorId);
		if (vendor.isPresent()) {
			// Grab Client ID of MELLOE
			mDetails.setClientId(grabClientId);
			// VendorId
			mDetails.setMerchantId(vendorId);
			mDetails.setMerchantAddress(vendor.get().getAddress().geTrackingAddress());
			mDetails.setMerchantContact(vendor.get().getCellNumbers().get(0).getPhoneNumber());
			mDetails.setMerchantLat(vendor.get().getAddress().getLatitude());
			mDetails.setMerchantLong(vendor.get().getAddress().getLongitude());
			mDetails.setMerchantName(vendor.get().getName());

			//Create Hash and Set headers for API call
			String requestJson = new JSONObject(mDetails).toString();
			HttpHeaders headers = getHeaders(requestJson);
			HttpEntity<String> entity = new HttpEntity<String>(requestJson, headers);
			// API call
			ResponseEntity<String> response = restTemplate.postForEntity(URL, entity, String.class);
			JSONObject obj = new JSONObject(response.getBody());
			if (!obj.isNull("success") && (obj.get("success").toString().equals("1"))) {
				isCreated = true;
				log.debug("Merchant Id created! Grab MerchantId is: " + obj.getString("id"));
			} else {
				String message;
				if (obj.isNull("errorCode"))
					message = obj.getString("message");
				else
					message = obj.getString("errorCode");
				log.debug("Unable to create merchant with id: {} for reason: {}", vendorId, message);
			}
		} else {
			throw new EntityNotFoundException(Vendor.class.getName(), vendorId);
		}
		return isCreated;
	}

	/**
	 * Private method to set headers for Grab API call.
	 * @param data
	 * @return
	 */
	private HttpHeaders getHeaders(String data) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("X-Public", grabPublicKey);
		headers.set("X-Hash", CommonMethods.generateHmacKey(grabPrivateKey, data));
		return headers;
	}
	
	public OrderDetailsResponse createOrder(Integer customerId, int orderId) {
		String URL = baseURL + "/createorder/";
		OrderDetails oDetails = new OrderDetails();

		Optional<Order> order = ordersService.getOrderByIdAndCustomerId(orderId, customerId);
//		Optional<Vendor> vendor = vendorService.getVendor(vendorId);
		if (order.isPresent()) {
			// Grab Client ID of melloe
			oDetails.setClientId(grabClientId);
			// VendorId
			oDetails.setMerchantId(order.get().getOrderDetails().get(0).getProduct().getVendor().getId());
			oDetails.setOrderId(0);
			oDetails.setCustomerName(order.get().getCustomer().getFirstName());
			oDetails.setCustomerAddressLine1(order.get().getDeliveryAddress().getAddressLine());
			oDetails.setCustomerAddressLine2("NA");
			oDetails.setCustomerAddressLandMark(order.get().getDeliveryAddress().getNearestLandmark());
			oDetails.setCustomerPhone(order.get().getCustomer().getPhoneNumber());
			// Lat and Long of delivery address
			oDetails.setDeliveryAreaLat(order.get().getDeliveryAddress().getLatitude());
			oDetails.setDeliveryAreaLong(order.get().getDeliveryAddress().getLongitude());

			oDetails.setOrderType(order.get().getOrderType());
			oDetails.setDttm(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
			oDetails.setComments(order.get().getComment());

			// Create Hash and Set headers for API call
			String requestJson = new JSONObject(oDetails).toString();
			HttpHeaders headers = getHeaders(requestJson);
			HttpEntity<String> entity = new HttpEntity<String>(requestJson, headers);

			ResponseEntity<String> response = restTemplate.postForEntity(URL, entity, String.class);

			JSONObject obj = new JSONObject(response.getBody());
			if (!obj.isNull("success") && obj.getBoolean("success")) {
				if (obj.getInt("status") == 101) {
					OrderDetailsResponse orderResponse = new OrderDetailsResponse();
					orderResponse.setOrderId(obj.getInt("orderId"));
					orderResponse.setExpectedDeliveryTime(obj.getInt("expectedDeliveryTime"));
					orderResponse.setExpectedRiderTime(obj.getInt("expectedRiderTime"));
					orderResponse.setTimestamp(obj.getString("timestamp"));
					order.get().setGrabOrderId(orderResponse.getOrderId());
					ordersService.saveOrder(order.get());
					log.info("Grab Order Id updated: {} for order Id: {}", orderResponse.getOrderId(), orderId);
					return orderResponse;
				} else {
					log.info("Order creation failed, with status: {}", obj.getInt("status"));
					throw new MelloeException("Order creation failed, with status: " + obj.getString("status"));
				}
			} else {
				log.info("Order creation failed, with status: {}", obj.getInt("errorCode"));
				throw new MelloeException("Order creation failed, with status: " + obj.getString("errorCode"));
			}
		} else {
			throw new EntityNotFoundException(Vendor.class.getName() + ", " + Order.class.getName(),
					", OrderId: " + orderId);
		}
	}

	/**
	 * Push Order to grab.in for delivery.
	 * @param customerId
	 * @param vendorId
	 * @param orderId
	 */
	public int pushOrder(Integer customerId, int orderId) {
System.out.println("*****************Inside Push Order Client API");
		String URL = baseURL + "/pushorder/";
		OrderDetails oDetails = new OrderDetails();
		int expectedDeliveryTime = 0;

		Optional<Order> order = ordersService.getOrderByIdAndCustomerId(orderId, customerId);
		if (order.isPresent()) 
		{
			// Grab Client ID of melloe
			oDetails.setClientId(grabClientId);
			//oDetails.setOrderId(grabOrderId); optional
			oDetails.setClientOrderId(orderId);
			oDetails.setPrepTime(20);
			// VendorId
			oDetails.setMerchantId(order.get().getOrderDetails().get(0).getProduct().getVendor().getId());
			oDetails.setDttm(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
			oDetails.setCustomerPhone(order.get().getCustomer().getPhoneNumber());
			oDetails.setCustomerName(order.get().getCustomer().getFirstName());
			oDetails.setCustomerAddressLine1(order.get().getDeliveryAddress().getAddressLine());
			oDetails.setCustomerAddressLine2(order.get().getDeliveryAddress().getAddressLine_2());
			oDetails.setCustomerAddressLandMark(order.get().getDeliveryAddress().getNearestLandmark());	
			
			if(order.get().getPromoCodeAmount() != null && order.get().getPromoCodeAmount() > 0)
			{
				System.out.println("*******************Inside Bill Amount 1" +order.get().getTotalOrderPriceWithPromocode());
				oDetails.setBillAmount(order.get().getTotalOrderPriceWithPromocode());
				oDetails.setAmountPaidByClient(order.get().getTotalOrderPriceWithPromocode());
			}
			else
			{
				System.out.println("*******************Inside Bill Amount 2" +order.get().getTotalPrice());
				oDetails.setBillAmount(order.get().getTotalPrice());
				oDetails.setAmountPaidByClient(order.get().getTotalPrice());
			}
			oDetails.setBillNo(orderId);
			oDetails.setOrderType(order.get().getOrderType());
			oDetails.setAmountCollectedInCash(0.0);
			oDetails.setComments(order.get().getComment());
			
			//Latitude and Longitude of delivery address
			//oDetails.setDeliveryAreaLat(order.get().getDeliveryAddress().getLatitude());
			//oDetails.setDeliveryAreaLong(order.get().getDeliveryAddress().getLongitude());
			
			String requestJson = new JSONObject(oDetails).toString();
			HttpHeaders headers = getHeaders(requestJson);

			HttpEntity<String> entity = new HttpEntity<String>(requestJson, headers);
			ResponseEntity<String> response = restTemplate.postForEntity(URL, entity, String.class);
			JSONObject obj = new JSONObject(response.getBody());

			if("true".equals(obj.getString("success"))) {
				expectedDeliveryTime = obj.getInt("expectedDeliveryTime");
			}
		} else {
			throw new EntityNotFoundException(Vendor.class.getName() + ", " + Order.class.getName(),
					", OrderId: " + orderId);
		}
		return expectedDeliveryTime;
	}

	/**
	 * Method to track order status details
	 * @param orderId
	 */
	public OrderStatusResponse orderStatus(int grabOrderId) {
		String URL = baseURL + "/getorderstatus/";

		JSONObject oStatusDetails = new JSONObject();
		OrderStatusResponse orderStatusResponse =new OrderStatusResponse();
		// Grab Client ID of melloe
		oStatusDetails.put("clientId", grabClientId);
		oStatusDetails.put("clientOrderId", grabOrderId);

		
		// String requestJson = new JSONObject(oDetails).toString();
		HttpHeaders headers = getHeaders(oStatusDetails.toString());
		HttpEntity<String> entity = new HttpEntity<>(oStatusDetails.toString(), headers);

		System.out.println(entity.getBody());
		try {
		ResponseEntity<String> response = restTemplate.postForEntity(URL, entity,
				String.class);
		System.out.println(response.getBody());
		JSONObject obj = new JSONObject(response.getBody());
		orderStatusResponse.setBillNo(obj.getString("billNo"));
		if (!obj.isNull("deliveryBoyContact")) { orderStatusResponse.setDeliveryBoyContact(obj.getString("deliveryBoyContact")); }
		orderStatusResponse.setDeliveryBoyLat(obj.getString("deliveryBoyLat"));
		orderStatusResponse.setDeliveryBoyLong(obj.getString("deliveryBoyLong"));
		orderStatusResponse.setDeliveryBoyName(obj.getString("deliveryBoyName"));
		orderStatusResponse.setMerchantId(obj.getString("MerchantId"));
		orderStatusResponse.setOrderAmount(obj.getString("orderAmount"));
		orderStatusResponse.setOrderStatus(obj.getString("orderStatus"));
		orderStatusResponse.setRequestTimestamp(obj.getInt("requestTimestamp"));
		orderStatusResponse.setSuccess(true);
		}
		
		catch (HttpServerErrorException e) { System.out.println(e); orderStatusResponse.setSuccess(false); }
		return orderStatusResponse;
	}

	/**
	 * Cancel Order from Delivery.
	 * @param customerId
	 * @param vendorId
	 * @param orderId
	 * @param cancelComment
	 */
	public boolean cancelOrder(Integer customerId, int orderId, String cancelComment) {
		String URL = baseURL + "/cancelorder/";
		JSONObject oCancelDetails = new JSONObject();
		Optional<Order> order = ordersService.getOrderByIdAndCustomerId(orderId, customerId);

		if (order.isPresent()) {
			// Grab Client ID of melloe
			oCancelDetails.put("clientId", grabClientId);
			//oCancelDetails.put("orderId", orderId);
			oCancelDetails.put("clientOrderId", orderId);
			oCancelDetails.put("comment", cancelComment);
			oCancelDetails.put("dttm", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
			
			HttpHeaders headers = getHeaders(oCancelDetails.toString());
			HttpEntity<String> entity = new HttpEntity<>(oCancelDetails.toString(), headers);
			
			ResponseEntity<String> response = restTemplate.postForEntity(URL, entity, String.class);
			JSONObject obj = new JSONObject(response.getBody());
			if ("true".equals(obj.getString("success"))) {
				return true;
			} else {
				log.debug("Cancel order failed with reason: {} ", obj.getString("errorCode"));
			}
		}
		return false;
	}
}
