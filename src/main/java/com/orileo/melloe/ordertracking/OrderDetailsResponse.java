package com.orileo.melloe.ordertracking;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode
@ToString
public class OrderDetailsResponse {
	private int orderId;
	private int expectedRiderTime;
	private int expectedDeliveryTime;
	private String timestamp;
}