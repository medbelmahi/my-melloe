package com.orileo.melloe.ordertracking;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode
@ToString
public class MerchantDetails {
	private int clientId;
	private int merchantId;
	private String merchantName;
	private String merchantAddress;
	private String merchantLat;
	private String merchantLong;
	private String merchantContact;
	private char merchantType = 'R';
	private int merchantVerified = 0;

}