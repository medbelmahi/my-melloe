package com.orileo.melloe.ordertracking;

import java.io.Serializable;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode
@ToString
public class LiveOrderTracking implements Serializable
{
	private static final long serialVersionUID = -8430151119617556968L;
	private int orderStatus;
	private int grabOrderId;
	private int clientOrderId;
	private String merchantBillNo;
	private String merchantId;
	private String riderName;
	private String riderPhone;
	private String riderLatitude;
	private String riderLongitude;
	private String expectedDeliveryTime;
	private String dttm;
}